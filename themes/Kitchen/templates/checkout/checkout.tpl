{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!doctype html>
<html lang="{$language.iso_code}">

  <head>
    {block name='head'}
      {include file='_partials/head.tpl'}
    {/block}
  </head>

  <body id="{$page.page_name}" class="{$page.body_classes|classnames}">
  <main id="page">
    {block name='hook_after_body_opening_tag'}
      {hook h='displayAfterBodyOpeningTag'}
    {/block}

    <header id="header">
      {block name='header'}
        {include file='_partials/header.tpl'}
      {/block}
    </header>

    {block name='notifications'}
      {include file='_partials/notifications.tpl'}
    {/block}

    <section id="wrapper">
{hook h="displayWrapperTop"}
     <div class="page-container container">

		<div id="columns_inner">
		  	{block name="left_column"}
				<div id="left-column" class="col-xs-12" style="width:22.65%">
					{if $page.page_name == 'product'}
						{hook h='displayLeftColumnProduct'}
					{else}
						{hook h="displayLeftColumn"}
					{/if}
				</div>
			{/block}
		  
		  {block name='content'}
			<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9" style="width:77.35%">
				<section id="main">
					<div class="row">
						<div class="col-md-8">
							{render file='checkout/checkout-process.tpl' ui=$checkout_process}
						</div>
						<div class="col-md-4">
							{include file='checkout/_partials/cart-summary.tpl' cart = $cart}
							{hook h='displayReassurance'}
						</div>
					</div>
				</section>
			</div>
		  {/block}
			
		  {block name="right_column"}
			<div id="right-column" class="col-xs-12"  style="width:22.65%">
				{if $page.page_name == 'product'}
					{hook h='displayRightColumnProduct'}
				{else}
					{hook h="displayRightColumn"}
				{/if}
			</div>
		{/block}
		</div>
		  
	  
	  
      </div>
 {hook h="displayWrapperBottom"}
    </section>

    <footer id="footer">
      {block name='footer'}
        {include file='_partials/footer.tpl'}
      {/block}
    </footer>

    {block name='javascript_bottom'}
      {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}

    {block name='hook_before_body_closing_tag'}
      {hook h='displayBeforeBodyClosingTag'}
    {/block}
  </main>
  </body>

</html>
