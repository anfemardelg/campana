<?php $__env->startSection('tittle','Creacion de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Creacion de Antecedentes'); ?>
<?php $__env->startSection('content'); ?>



<?php echo Form::open(['action' => 'AntecedentesController@store']); ?>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
  <div class="form-group">
  	<?php echo Form::label('antecedente_oficio','Oficio:'); ?>

  	<?php echo Form::text('antecedente_oficio',null,['class'=>'form-control','placeHolder'=>'Oficio','required']); ?>

  </div>
  </div>
   <div class="col-xs-12 col-sm-12 col-md-12">
  <div class="form-group">
  	<?php echo Form::label('antecedente_descripcion','Anotaciones:'); ?>

  	<?php echo Form::textarea('antecedente_descripcion',null,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

  </div>
   </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
  	<?php echo Form::submit('Registrar',['class'=>'btn btn-primary']); ?>

  </div>  
</div>   


<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('coactivo.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>