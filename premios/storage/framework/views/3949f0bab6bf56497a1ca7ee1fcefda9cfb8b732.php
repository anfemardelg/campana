<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('title','Default'); ?></title>

    <!-- Styles <?php echo e(asset('css/sticky-footer.css')); ?>-->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('css/stickyfooter.css')); ?>">
    <!--<link rel="stylesheet" href="<?php echo e(asset('librerias/bootstrap-3.3.7-dist/css/bootstrap.css')); ?>">      -->
    <link rel='stylesheet' href="<?php echo e(asset('librerias/bootstrap-3.3.7-dist/css/bootstrap.min.css')); ?>" >
    <link rel='stylesheet' href="<?php echo e(asset('librerias/DataTables-1.10.15/media/css/jquery.dataTables.min.css')); ?>" >
    <link rel='stylesheet' href="<?php echo e(asset('librerias/DataTables-1.10.15/media/css/buttons.dataTables.min.css')); ?>" >
    <!--Bootstrap-->
      <link rel='stylesheet' href="<?php echo e(asset('librerias/DataTables-1.10.15/media/css/buttons.bootstrap.min.css')); ?>" >
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
  
</head>

  <body>
      <div id="app">
          <?php echo $__env->make('admin.template.partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-md-10 col-md-offset-1">
                         <div class="panel panel-default">
                             <div class="panel-heading" style="font-size:18px"><strong><?php echo $__env->yieldContent('title_panel','Titulo del panel'); ?></strong></div>
                             <div class="panel-body">
                              <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                               <?php echo $__env->yieldContent('content'); ?>
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
      </div> 
        <footer class="footer">
            <div class="container">
                <div>
                    <img src="<?php echo e(asset('imagen/logAviaMarketing.png')); ?>"  width="200" style="margin-top: -2%">
                    <span class="text-muted">© Copyright <?php echo e(date('Y')); ?></span>
                </div>  
            </div>
        </footer>
       <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
      
    <script type="application/javascript" src="<?php echo e(asset('librerias/jquery/js/jquery-1.12.4.js')); ?>"></script>    
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/jquery.dataTables.min.js')); ?>"></script>
    <!--Bootstrap-->  
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/buttons.bootstrap.min.js')); ?>"></script>  
      
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/dataTables.buttons.min.js')); ?>"></script> 
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/buttons.html5.min.js')); ?>"></script>  
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/jszip.min.js')); ?>"></script> 
      
    <script type="application/javascript" src="<?php echo e(asset('librerias/DataTables-1.10.15/media/js/buttons.colVis.min.js')); ?>"></script>  
      
  </body>
   
</html>
