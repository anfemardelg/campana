<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $__env->yieldContent('tittle','default'); ?>| Cobro Coactivo</title>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/bootstrap/css/bootstrap.css')); ?>">
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style type="text/css" title="currentStyle">
        @import  "<?php echo e(asset('plugins/DataTables-1.10.12/media/css/jquery.dataTables.min.css')); ?>";
        </style>-->  	 
        <!--<script src="<?php echo e(asset('plugins/jquery/js/jquery-3.1.1.js')); ?>"></script>-->
        <script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.js')); ?>"></script>
        <!--<script src="<?php echo e(asset('plugins/jquery/js/jquery-ui/jquery-1.12.4.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/jquery/js/jquery-ui/jquery-ui.js')); ?>"></script>
        <script type="text/javascript" charset="utf-8" src="<?php echo e(asset('plugins/DataTables-1.10.12/media/js/jquery.dataTables.min.js')); ?>"></script>-->
    </head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <br><br>
                <div class="panel panel-default">
                   <div class="panel-heading">
                        <!--<strong>Listar Documentos</strong>-->
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">    
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Documento</th>
                                        <th>Opción</th>
                                    </tr>
                                    <tbody>
                                        <?PHP
                                        $ver = 0;
                                            foreach($ListaDocumentos as $row){
                                                $ver = 1;
                                            }
                                        $i = 0;
                                        ?>
                                        <?php if($ver==1): ?>
                                        <?php $__currentLoopData = $ListaDocumentos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doc): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <?PHP 
                                            $urlname = explode('_',$doc->url);    
                                            $extencionDoc = explode('.',$urlname[1]);   
                                            $num = count($urlname);
                                           
                                            ?>
                                            <tr>
                                                <td><?php echo e($i+1); ?></td>
                                                <td><?php echo e($urlname[1]); ?></td>
                                                <td>
                                                    <?php if($doc->extension=='pdf' || $doc->extension=='doc' || $doc->extension=='docx'): ?>
                                                        <?php if($doc->extension=='doc' || $doc->extension=='docx'): ?>
                                                              <a href="<?php echo e(route('documentos.downloadfile', $doc->id)); ?>"  >                                                                  
                                                              <img src="<?php echo e(asset('imagen/1477094023_word.png')); ?>"  width="30" style="cursor:pointer" class="img-responsive" alt="responsive image">
                                                                  </a>
                                                    
                                                        <?php endif; ?>
                                                        <?php if($doc->extension=='pdf'): ?>
                                                    <a  onclick="window.open('documentos/downloadfile/<?php echo e($doc->id); ?>')"  >     
                                                             <img src="<?php echo e(asset('imagen/1477094440_pdf.png')); ?>"  href="<?php echo e(route('documentos.downloadfile', $doc->id)); ?>"  width="30" style="cursor:pointer" class="img-responsive" alt="responsive image">
                                                    </a>    <!-- <?php echo e(route('documentos.downloadfile', $doc->id)); ?> -->
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>   
                                        <?PHP $i++; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        <?php endif; ?>
                                        <?php if($ver!=1): ?>
                                            <tr>
                                                <td colspan="3">
                                                    <div style="text-align:center">
                                                        <?php echo Form::label('texto','No hay Documentos Adjuntos o Asociados'); ?>

                                                    </div>    
                                                </td>
                                            </tr>
                                      <?php endif; ?>
                                    </tbody>
                                </thead>        
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>     
</html>



