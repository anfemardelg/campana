<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Acto Administrativo'); ?>
<?php $__env->startSection('title_panel','Lista de Acto Administrativo'); ?>
<?php $__env->startSection('content'); ?> 

<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
 <script type="text/javascript">
    $(document).ready(function(){
        $('#DataAntecedente').DataTable({
              "language": {
                "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"    
                }
              }
            });
    });
</script>
  <a href="<?php echo e(route('antecedentes.create')); ?>" id="Button" class="Button">Registrar Acto Administrativo</a>          
<br/>
<br/>
<div id="dialog-form" title="Listado de Documentos"></div>
<input type="hidden" id="userid" name="userid" value="<?php echo e(Auth::user()->id); ?>" />
    <div class="table-responsive">
  <table id="DataAntecedente" class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Origen</th>
        <th>Fecha de Recepción</th>  
        <th>Resolución</th>  
        <th>Fecha de Resolución</th>  
        <th>Fecha de Ejecución</th>  
        <th>Oficio</th>
        <th>Nombre ó Razón Social</th>          
        <th>Abogado</th>  
        <th>Estado</th>  
        <th>Opción</th>      
      </tr>
    </thead>
    <tbody>
    <?PHP $i=0; ?>    
    <?php $__currentLoopData = $antecedentes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $antecedente): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
     <tr>
        <td><?php echo e($i+1); ?></td> 
        <td style="width: 20%" ><?php echo e($antecedente->parametro_codigo); ?></td>
         <td><?php echo e($antecedente->fecha_recepcion); ?></td>
         <td><?php echo e($antecedente->antecedente_resolucion); ?></td>
         <td><?php echo e($antecedente->antecedente_fecharesol); ?></td>
         <td><?php echo e($antecedente->fecha_ejecucion); ?></td>
         <td><?php echo e($antecedente->antecedente_oficio); ?></td>
        <td  style="width: 20%"><?php echo e($antecedente->antecedente_nombre); ?>&nbsp;<?php echo e($antecedente->antecedente_apellido); ?></td>
       <td ><?php echo e($antecedente->usr_name); ?>&nbsp;<?php echo e($antecedente->usr_lname); ?></td>
       <td><?php echo e($antecedente->parametro_descripcion); ?></td>
        <td>
        
        <?php if($antecedente->antecedente_estado!=20): ?>
          <div  style="margin:0 auto; width: 100%; ">
           <?php if (\Entrust::can('antecedentes-edit')) : ?>
            <a href="<?php echo e(route('antecedentes.edit', $antecedente->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
           <?php endif; // Entrust::can ?>      
              <?php $__currentLoopData = $ViewDoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                     <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($view->valor_id == $antecedente->id): ?>
                                <a onclick="ListarDocumentos(<?php echo e($antecedente->id); ?>,'antecedenteid')" class="btn btn-primary btn-xs" title="Visualizar Doumentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>  
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>      
            <?php if (\Entrust::can('antecedentes-delete')) : ?>
            <a href="<?php echo e(route('antecedentes.destroy', $antecedente->id)); ?>" onclick="return confirm('Desea Eliminar el Acto Administrativo...!!')"  class="btn btn-danger btn-xs" title="Eliminar Acto Administrativo"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
           <?php endif; // Entrust::can ?>    
          </div>
        <?php endif; ?>  
        </td>
      </tr>
        <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
</div>
 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>