<h2>Etapas ó Actidades por Vencer</h2>
<?php $__currentLoopData = $name->Datos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Datos): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<?php $__currentLoopData = $Datos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
		<p>
			<strong>Acto Administrativo</strong><br/>
			<strong>Resolución:</strong>  <?php echo e($row->antecedente_resolucion); ?><br/>
			<strong>Razón Social ó Nombre:</strong>  <?php echo e($row->RazonSocial); ?><br/>
			<strong>Identificado:</strong> <?php echo e($row->parametro_codigo); ?> <?php echo e($row->antecedente_nro_documento); ?><br/>
			<strong>N° Radicado:</strong>  <?php echo e($row->expediente_rad); ?><br/>
			<strong>Etapa:</strong>  <?php echo e($row->proceso_nombre); ?><br/>
			<strong>Fecha Finalización de Etapa:</strong>  <?php echo e($row->fecha_fin_etapa); ?> <br/>
			<strong>Actividad:</strong>  <?php echo e($row->estado_nombre); ?><br/>
			<strong>Fecha Finalización de Actividad:</strong>  <?php echo e($row->fecha_fin_Actividad); ?> <br/>
		</p>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

