<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Tabla de tasas intereses moratorios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Tasas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#tablatasas').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>


	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
            <?php if (\Entrust::can('tasas-create')) : ?>
	            <a class="btn btn-success" href="<?php echo e(route('tasas.create')); ?>"> Crear Tasa</a>
            <?php endif; // Entrust::can ?>
	        </div>
	    </div>
	</div>
<br/>
	<?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
	<table id="tablatasas" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Porcentaje anual</th>
			<th>Fecha inicio</th>
			<th>Fecha fin</th>
			<th>Accion</th>
		</tr>
		</thead>
	<tbody>
	<?php $__currentLoopData = $tasas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $tasa): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($tasa->porcentaje_anual); ?></td>
		<td><?php echo e($tasa->fecha_inicio); ?></td>
		<td><?php echo e($tasa->fecha_fin); ?></td>
		<td>		
		    <?php if (\Entrust::can('tasas-edit')) : ?>
			<a class="btn btn-primary" href="<?php echo e(route('tasas.edit',$tasa->id)); ?>">Editar</a>
			<?php endif; // Entrust::can ?>

			<?php if (\Entrust::can('tasas-delete')) : ?>

			<?php echo Form::open(['method' => 'DELETE','route' => ['tasas.destroy', $tasa->id],'style'=>'display:inline']); ?>

            <?php echo Form::submit('Eliminar', ['class' => 'btn btn-danger','id'=>'eliminar','onClick'=>'return confirm("Seguro desea eliminar el valor de la tasas...?")']); ?>

        	<?php echo Form::close(); ?>

       		<?php endif; // Entrust::can ?>

		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	</tbody>
	</table>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>