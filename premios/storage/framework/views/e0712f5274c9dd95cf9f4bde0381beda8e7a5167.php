<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('title','Default'); ?>|Administracion</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

     <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

  </head>
  <body>

   <?php echo $__env->make('admin.template.partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <div class="container">
         <div class="row">
             <div class="col-md-8 col-md-offset-2">
                 <div class="panel panel-default">
                     <div class="panel-heading" style="font-size:18px"><strong><?php echo $__env->yieldContent('title_panel','Titulo del panel'); ?></strong></div>
                     <div class="panel-body">

                       <?php echo $__env->yieldContent('content'); ?>

                     </div>
                 </div>
             </div>
         </div>
    </div>

      <script src="/jquery/jquery-3.1.1"></script>
      <script src="/js/app.js"></script>

  </body>
</html>
