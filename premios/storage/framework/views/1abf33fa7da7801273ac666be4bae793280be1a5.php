<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte de pagos'); ?>
<?php $__env->startSection('title_panel','Reporte'); ?>
<?php $__env->startSection('content'); ?>

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte de pagos
		</legend>
		<table id="Payu" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Orden</th>
				<th>Usuario</th>
                <th>Payu</th>
                <th>Valor</th>
                <th>Moneda</th>
                <th>Medio de pago</th>
                <th>Entidad</th>
                <th>Estado</th>
                <th>Fecha</th>
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $payu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pago): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($pago->orden); ?></td>
					<td><?php echo e($pago->usuario); ?></td>
                    <td><?php echo e($pago->payu); ?></td>
                    <td><?php echo e($pago->valor); ?></td>
                    <td><?php echo e($pago->moneda); ?></td>
                    <td><?php echo e($pago->medio_de_pago); ?></td>
                    <td><?php echo e($pago->entidad); ?></td>
                    <td><?php echo e($pago->estado); ?></td>
                    <td><?php echo e($pago->fecha); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>