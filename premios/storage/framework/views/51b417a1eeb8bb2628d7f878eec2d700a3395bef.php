<?php $__env->startSection('tittle','Lista de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Lista de Antecedentes'); ?>
<?php $__env->startSection('content'); ?>
  <a href="<?php echo e(route('detalles.create',$idOficio)); ?>" class="btn btn-info">Registrar Antecedente</a> 
  <a href="#" class="glyphicon-wrench">Oficio de Llegada <?php echo e($oficio); ?></a>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TIPO DOC</th>
        <th>NUMERO DOCUMENTO</th>
        <th>RESOLUCION</th>
        <th>ACCION</th>
      </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $detalles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detalle): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
     <tr>
        <td><?php echo e($detalle->id); ?></td>
        <td><?php echo e($detalle->detalle_nombre); ?></td>
        <td><?php echo e($detalle->detalle_apellido); ?></td>
        <td><?php echo e($detalle->tipo_doc_id); ?></td>
        <td><?php echo e($detalle->detalle_nro_documento); ?></td>
        <td><?php echo e($detalle->detalle_resolucion); ?></td>
      	<td style="width: 25%"> 
          <div  style="margin:0 auto; width: 100%; ">
         <?php if($detalle->detalle_reparto=='S'): ?>
            <a href="<?php echo e(route('reparto.create', $detalle->id)); ?>" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a> 
         <?php else: ?>
            <a href="<?php echo e(route('reparto.create', $detalle->id)); ?>" class="btn btn-success btn-xs" disabled="disabled"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>    
         <?php endif; ?>
        <a href="<?php echo e(route('detalles.edit', $detalle->id)); ?>" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a> </div>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
  <?php echo $detalles->render(); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('coactivo.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>