<?php echo e(csrf_field()); ?>

<?php $__env->startSection('tittle','Lista de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Lista de Antecedentes'); ?>
<?php $__env->startSection('content'); ?> 
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
 <script type="text/javascript">
    $(document).ready(function(){
        $('#DataAntecedente').DataTable({
              "language": {
                "paginate": {
                  "previous": "Anterior",
                  "next": "Siguiente"    
                }
              }
            });
    });
</script>
  <a href="<?php echo e(route('antecedentes.create')); ?>" class="btn btn-info">Registrar Antecedente</a>          
<br/>
<br/>
<div id="dialog-form" title="Listado de Documentos"></div>
  <table id="DataAntecedente" class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>OFICIO</th>
        <th>DESCRIPCION</th>
        <th>ACCION</th>      
      </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $antecedentes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $antecedente): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
     <tr>
        <td><?php echo e($antecedente->id); ?></td>
        <td><?php echo e($antecedente->antecedente_oficio); ?></td>
        <td ><?php echo e($antecedente->antecedente_descripcion); ?></td>
        <td style="width: 25%">
          <div  style="margin:0 auto; width: 100%; ">
            <a href="<?php echo e(route('antecedentes.edit', $antecedente->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
            <a onclick="ListarDocumentos(<?php echo e($antecedente->id); ?>,'antecedenteid')" class="btn btn-primary btn-xs" title="Visualizar Doumentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>
          </div>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
 <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>