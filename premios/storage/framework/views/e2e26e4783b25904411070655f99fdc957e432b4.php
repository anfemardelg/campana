<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Crear Roles
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Crear rol
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<style type="text/css">
    fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
    
</style>
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a  id="Button" class="Button"  href="<?php echo e(route('roles.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
	<br>
	<?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
	<?php echo Form::open(array('route' => 'roles.store','method'=>'POST')); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <?php echo Form::text('name', null, array('placeholder' => 'Rol','class' => 'form-control')); ?>

            </div>
        </div>
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre en pantalla:</strong>
                <?php echo Form::text('display_name', null, array('placeholder' => 'Nombre en pantalla','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                <?php echo Form::textarea('description', null, array('placeholder' => 'Descripcion','class' => 'form-control','style'=>'height:100px')); ?>

            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <legend >PERMISOS</legend>
                <br/>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">ADMINISTRACIÓN</legend>
                    <div class="control-group">
                    <?php $__currentLoopData = $permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($value->orden == 1): ?> 
                        	<label><?php echo e(Form::checkbox('permission[]', $value->id, false, array('class' => 'name'))); ?>

                        	<?php echo e($value->display_name); ?></label>
                        	<br/>
                         <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </div>
                </fieldset>
                <br/>
                 <fieldset class="scheduler-border">
                    <legend class="scheduler-border">OPERACIÓN</legend>
                    <div class="control-group">
                    <?php $__currentLoopData = $permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($value->orden == 2): ?> 
                            <label><?php echo e(Form::checkbox('permission[]', $value->id, false, array('class' => 'name'))); ?>

                            <?php echo e($value->display_name); ?></label>
                            <br/>
                         <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="glyphicon glyphicon-floppy-disk">&nbsp;Crear</button>
        </div>
	</div>
	<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>