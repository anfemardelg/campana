<script type="text/javascript">
$(document).ready(function(){
    $('#ReporteTitulo').DataTable({
            dom: 'Bfrtip',
            buttons: [
                    'excel', 'pdf'
                  ],
            "language": {
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"    
            }
            }
        });
});
</script>
<div class="table-responsive"> 
    <table id="ReporteTitulo" class="table" >
        <thead>
            <tr>
                <th>Resolución</th>
                <th>N&deg Documento</th>
                <th>Razón Social ó Nombre</th>
                <th>N&deg Título</th>
                <th>Valor Título</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($row->antecedente_resolucion); ?></td>
                <td><?php echo e($row->antecedente_nro_documento); ?></td>
                <td><?php echo e($row->RazonSocial); ?></td>
                <td><?php echo e($row->titulo_judicial); ?></td>
                <td><?php echo e($row->valor); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
</div>    