<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Crear tasa de interes moratorio
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Crear Tasa
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script>
  $( document).ready(function() {
    $( ".datepicker2" ).datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true
        });
  } );
  </script>



	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('tasas.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
<?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
<?php echo Form::open(array('route' => 'tasas.store','method'=>'POST')); ?>



<div class="row">
		<div class="col-xs-2 ">
            <div class="form-group">
                <strong>Porcentaje:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::text('porcentaje_anual', null, array('placeholder' => 'Porcentaje','class' => 'form-control','onkeypress'=>'return validarDecimales(event,this,true)','required','maxlength'=>'5' )); ?>

            </div>
        </div>

		<div class="col-xs-2 ">
            <div class="form-group">
                <strong>Fecha inicio:</strong>
                <span style="color:red"><strong>*</strong></span>
               
                <?php echo Form::text('fecha_inicio', null, array('placeholder' => 'Fecha inicio','class' => 'form-control datepicker2 ','autocomplete'=>'off','required'  )); ?>


            </div>
        </div>

        <div class="col-xs-2 ">
            <div class="form-group">
                <strong>Fecha fin:</strong>
                <span style="color:red"><strong>*</strong></span>
               
                <?php echo Form::text('fecha_fin', null, array('placeholder' => 'Fecha fin','class' => 'form-control datepicker2 ','autocomplete'=>'off','required'  )); ?>


            </div>
        </div>
        </br>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit">Crear Tasa</button>
        </div>

</div>
<?php $__env->stopSection(); ?>	
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>