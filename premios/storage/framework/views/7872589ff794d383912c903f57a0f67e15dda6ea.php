<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte Título Valor'); ?>
<?php $__env->startSection('title_panel','Reporte Título Valor'); ?>
<?php $__env->startSection('content'); ?>
<script  type="text/javascript" charset="utf-8" >
	function autocompletebuscar(){
		var token = $('input:hidden[name=_token]').val();

		$('#buscar').autocomplete({

		   source: "autocompleteBuscar",
		   headers:{'X-CSRF-TOKEN':token},
		   minLength: 2,
		   select: function( event, ui ) {
		   		
		           $('#idbuscar').val(ui.item.id);		           
		   }                
		});//autocomplet
	}//function autocompletebuscar
	function formatBuscar(){
		$('#buscar').val('');	
	}//function formatBuscar
	function BuscarTitulos(){
		var idBuscar = $('#idbuscar').val();
		var estado   = $('#Estado').val();

		if(!$.trim(idBuscar)){
			alert('Por Favor Ingrese Razón Social ó Numero de Documento');
			$('#buscar').effect("pulsate", {times: 5}, 1000);
			$('#buscar').css('border-color', '#F00');
			return false;
		}

		if(!$.trim(estado)){
			alert('Por Favor Seleccione El Estado');
			$('#Estado').effect("pulsate", {times: 5}, 1000);
			$('#Estado').css('border-color', '#F00');
			return false;
		}
		var token = $('input:hidden[name=_token]').val();
		$.ajax({ //Ajax
            type: 'POST',
            headers:{'X-CSRF-TOKEN':token},
            url: 'ReporteTituloValor',
            async: false,
            dataType: 'html',
            data:({idBuscar:idBuscar,estado:estado }),
            error: function (objeto, quepaso, otroobj) {
                alert('Error de Conexi?n , Favor Vuelva a Intentar');
            },
            success: function (data) {
                 $('#Content_Reporte').html(data);
                } //DATA
        }); //AJAX
	}//function BuscarTitulos
</script>
<div class="form-group">
	<?php echo Form::label('Buscar','Buscar:'); ?>

	<span style="color:red"><strong>*</strong></span>   
	<?php echo Form::text('buscar',null,['class'=>'form-control','placeHolder'=>'Razón Social ó Nombre ó Numero Documento ','autocomplete'=>'off','required','onkeypress'=>'autocompletebuscar()','id'=>'buscar','onclick'=>'formatBuscar()']); ?>    
	<?php echo Form::hidden('idbuscar',null,['class'=>'form-control','id'=>'idbuscar']); ?>    
</div>
<div class="form-group">
	<?php echo Form::label('Estado','Estado:'); ?>

	<span style="color:red"><strong>*</strong></span>   
	<?php echo Form::select('Estado', $Estado, null, ['class'=>'form-control','placeholder' => 'Seleccione el Estado...','id'=>'Estado']); ?>   
</div>	
<div class="form-group">
    <?php echo Form::button('&nbsp;Buscar',['class'=>'glyphicon glyphicon-search','title'=>'Click para Buscar','type'=>'button','onclick'=>'BuscarTitulos()']); ?>

</div>
<div class="form-group">
	<div id="Content_Reporte"></div>
</div>		
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>