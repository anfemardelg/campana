<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('titulovalor.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
    
<table class="table table-responsive">
    <tbody>
        <tr>
            <td>
                <strong>Radicado</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_resolucion); ?></td>
        </tr>
        <tr>
            <td>
                <strong>Razón Social ó Nombre</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_nombre); ?>&nbsp;<?php echo e($ActoAdmin->antecedente_apellido); ?></td>
        </tr>
        <tr>
            <td>
                <strong>N&deg; Documento</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_nro_documento); ?></td>
        </tr>
    </tbody>
</table>     
<hr/>
<?php echo Form::open(['route'=>['titulovalor.store'],'method'=>'POST','enctype'=>'multipart/form-data']); ?>

<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<?php echo Form::hidden('antecedente_id',$ActoAdmin->id,['class'=>'form-control','required']); ?>

<div class="form-group">
    <?php echo Form::label('titulojudicial:','N° Título Judicial:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::text('titulo_judicial',null,['class'=>'form-control','placeHolder'=>'Numero de Título Judicial ','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)']); ?>    
</div>
 
<div class="form-group">
    <?php echo Form::label('Banco:','Banco:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::select('Banco', $Banco, null, ['class'=>'form-control','placeholder' => 'Seleccione el Banco...']); ?>

</div>

<div class="form-group">
    <?php echo Form::label('valor:','Valor del Título:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::text('valor_titulo',null,['class'=>'form-control','placeHolder'=>'Numero de Título Judicial ','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)']); ?> 
</div>
<div class="form-group">
    <?php echo Form::label('Estado','Estado:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::select('Estado', $Estado, null, ['class'=>'form-control','placeholder' => 'Seleccione el Estado...']); ?>

</div>
 <div class="form-group">
    <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

    <input type="file" name="file_primary[]" multiple>
    <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

</div>
<div class="form-group">
    <?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit']); ?>

</div>
<?php echo Form::close(); ?>