<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Etapas'); ?>
<?php $__env->startSection('title_panel','Lista de Etapas'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#data_opciones').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
   
});
</script>
<a href="<?php echo e(route('procesos.create')); ?>" id="Button" class="Button ">Registrar Etapa</a>
<br><br><br><br>
<div id="dialog-form" title="Asociar Estados"></div>
<table id="data_opciones" class="table table-striped display table-responsive"  cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Etapa</th>
            <th>Descripción</th>
            <th>Duración del Proceso en Días</th>
            <th>Notifica vencimiento a # Días</th>
            <th>Actividades</th>
            <th>Responsable</th>
            <th>Opcion</th>
        </tr>
    </thead>
    <tbody>
        <?PHP $i=0; ?>
        <?php $__currentLoopData = $C_Proceso; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_proceso): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($i+1); ?></td>
                <td><?php echo e($row_proceso->proceso_nombre); ?></td>
                <td><?php echo e($row_proceso->proceso_descripcion); ?></td>
                <td><?php echo e($row_proceso->proceso_dias); ?></td>
                <td><?php echo e($row_proceso->proceso_dias_vence); ?></td>
                <td>
                     <ul>
                        <?php $__currentLoopData = $C_Estado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_estado): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if($row_estado->procesoid==$row_proceso->id): ?>
                               <li><?php echo e($row_estado->estado_nombre); ?></li> 
                                <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                      </ul>    
                </td> 
                <td>
                    <?php $__currentLoopData = $C_rol; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($row_proceso->rol_id==$row->id): ?>
                            <?php echo e($row->name); ?>

                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </td>
                <td>
                    <?php if (\Entrust::can('proceso-edit')) : ?>
                     <a href="<?php echo e(route('procesos.edit', $row_proceso->id)); ?>" class="btn btn-warning btn-xs" title="Editar Proceso"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a> 
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('proceso-delete')) : ?>
                     <a href="<?php echo e(route('procesos.destroy', $row_proceso->id)); ?>"  onclick="return confirm('Desea Eliminar la Etapa...!!')"  class="btn btn-danger btn-xs" title="Eliminar Proceso"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                    <?php endif; // Entrust::can ?>
                </td>
            </tr>
        <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>