<?php $__env->startSection('title','Creación de Etapas'); ?>
<?php $__env->startSection('title_panel','Creación de Etapas'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('procesos.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['procesos.store'],'method'=>'POST','enctype'=>'multipart/form-data','id'=>'formProcesos']); ?>

    <?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

    <div class="form-group">
        <?php echo Form::label('nameproceso','Nombre de Etapa :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('proceso_nombre',null,['class'=>'form-control','placeholder'=>'Nombre de la  Etapa','autocomplete'=>'off','required']); ?>        
    </div>
    <div class="form-group">       
        <?php echo Form::label('DescripProceso','Descripción :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::textArea('proceso_descripcion',null,['class'=>'form-control','placeholder'=>'Descripción de la Etapa','required']); ?>

    </div>
    <div class="form-group">
        <table class="table table-striped display table-responsive">
            <tr>
                <td>
                      <?php echo Form::label('diasproces','Duración de la Etapa en Días :'); ?>

                        <span style="color:red"><strong>*</strong></span>  
                </td>
                <td>
                      <?php echo Form::label('venceproces','Notifica vencimiento a # Días:'); ?>

                        <span style="color:red"><strong>*</strong></span>  
                </td>
            </tr>
            <tr>
                <td>
                     <?php echo Form::number('proceso_dias',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>       
                </td>
                <td>
                     <?php echo Form::number('proceso_dias_vence',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>      
                </td>
            </tr>
        </table>      
    </div>
    <div class="form-group">        
        <?php echo Form::label('ProcesoPre-desesor','Etapa Predesesora :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::select('Procesoid',$C_Proceso,null,['class'=>'form-control','placeholder'=>'Seleccione la Etapa','required']); ?>

    </div>
    <div class="form-group">        
        <?php echo Form::label('Responsable','Responsable de la Etapa :'); ?>

        <span style="color:red"><strong>*</strong></span>  
       
        <?php $__currentLoopData = $C_rol; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rol): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
           &nbsp;<strong><?php echo e($rol->name); ?></strong>
            <input type="radio" name="rol" id="<?php echo e($rol->name); ?>" value="<?php echo e($rol->id); ?>" />&nbsp;
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </div>
<br><br>
    <div class="form-group">
        <?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit','id'=>'SaveProceso']); ?>

      </div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>