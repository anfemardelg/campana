<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte de redenciones'); ?>
<?php $__env->startSection('title_panel','Reporte'); ?>
<?php $__env->startSection('content'); ?>


<?php echo Form::open(['method'=>'POST', 'action' => 'ReportesController@diario']); ?>

<div class="form-group">
	<?php echo Form::label('fechaini', 'Fecha Inicial', ['class'=>'col-md-2 control-label']); ?>

	<div class="col-md-3">
		<?php echo Form::text('fechaini','',['id'=>'fechaini','class'=>'form-control', 'readonly']); ?>

	</div>
</div>
<div class="form-group">
	<?php echo Form::label('fechafin', 'Fecha Final', ['class'=>'col-md-2 control-label']); ?>

	<div class="col-md-3">
		<?php echo Form::text('fechafin','',['id'=>'fechafin','class'=>'form-control', 'readonly']); ?>

	</div>
</div>
<div class="form-group">
	<div class="col-md-2">
		<?php echo Form::submit('Reporte', ['id'=>'reporte','class'=>'btn btn-primary']); ?>

	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte diario <?php echo e($existe); ?>

		</legend>
		<table id="Diario" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Numero</th>
				<th>Documento</th>
				<th>Usuario</th>
				<th>Email</th>
				<th>Bono</th>
				<th>Sitio</th>
				<th>Departamento</th>                    
				<th>Ciudad</th>                    
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $diario; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $redencion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($redencion->id_bono); ?></td>
					<td><?php echo e($redencion->documento); ?></td>
					<td><?php echo e($redencion->nombre); ?></td>
					<td><?php echo e($redencion->email); ?></td>
					<td><?php echo e($redencion->bono); ?></td>
					<td><?php echo e($redencion->sitio); ?></td>
					<td><?php echo e($redencion->depto); ?></td>
					<td><?php echo e($redencion->ciudad); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>