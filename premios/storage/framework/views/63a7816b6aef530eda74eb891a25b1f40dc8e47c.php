<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte Expediente'); ?>
<?php $__env->startSection('title_panel','Reporte Expediente'); ?>
<?php $__env->startSection('content'); ?>
<script  type="text/javascript" charset="utf-8" >
	function Buscarreporte(){
		var filter 	 = $('#filtro').is(':checked');	
		
		var estado   = $('#Estado').val();

		if((!filter) && (!$.trim(estado))){
			alert('Por Favor Indique el item de Busqueda...!!');
			$('#Estado').effect("pulsate", {times: 5}, 1000);
			$('#Estado').css('border-color', '#F00');
			$('#filtro').effect("pulsate", {times: 5}, 1000);
			$('#filtro').css('border-color', '#F00');
			return false;
		}else{
			if(filter){
				var filtro = 1;
			}else{
				var filtro = 0;
			}
		}

		var token = $('input:hidden[name=_token]').val();
		$.ajax({ //Ajax
            type: 'POST',
            headers:{'X-CSRF-TOKEN':token},
            url: 'ReporteExpediente',
            async: false,
            dataType: 'html',
            data:({filtro:filtro,estado:estado }),
            error: function (objeto, quepaso, otroobj) {
                alert('Error de Conexi?n , Favor Vuelva a Intentar');
            },
            success: function (data) {
                 $('#Reporte').html(data);
                } //DATA
        }); //AJAX
	}//function Buscarreporte
</script>
<div class="form-group">
	<?php echo Form::label('Filtro','Filtro Estado:'); ?>

	<?php echo Form::select('Estado', $filtro, null, ['class'=>'form-control','placeholder' => 'Seleccione el Estado...','id'=>'Estado']); ?>   
</div>
<div class="form-group">
	<?php echo Form::label('Filtro1','Mayor 5 SMMLV:'); ?>

	<input type="checkbox" name="filtro" id="filtro" value="1">
</div>	
<div class="form-group">
    <?php echo Form::button('&nbsp;Buscar',['class'=>'glyphicon glyphicon-search','title'=>'Click para Buscar','type'=>'button','onclick'=>'Buscarreporte()']); ?>

</div>
<div class="form-group">
	<div id="Reporte"></div>
</div>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>