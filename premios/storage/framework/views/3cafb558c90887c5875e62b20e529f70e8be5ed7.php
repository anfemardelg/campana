<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
Administración de Api
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
    Api
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
    <br>
    <?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
    <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('campanas.create')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
    <br>
    
    <table id="tabla-api" class="table table-bordered">
		<thead>
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Foto</th>
                <th width="200px">Accion</th>
            </tr>
		</thead>
	    <tbody>
        
        <tr>
            <?php $__currentLoopData = $modelmarcas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $modelmarca): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <td><?php echo e($modelmarca->brand_id); ?></td>
            <td><?php echo e($modelmarca->nombre); ?></td>
            <td>
                <img src="<?php echo e($modelmarca->logo); ?>" width="100px" height="60px">
            </td>
            <td>
                <a class="btn btn-info" href="<?php echo e(route('Products.product')); ?>?valor=<?php echo e($modelmarca->brand_id); ?>">Productos</a>
            </td>
        <tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>    
	    </tbody>
	</table>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>