<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('title','Default'); ?></title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <style type="text/css" title="currentStyle">
     @import  "<?php echo e(asset('plugins/DataTables-1.10.12/media/css/jquery.dataTables.min.css')); ?>";
    </style> 

     <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/bootstrap/css/bootstrap.css')); ?>">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css" title="currentStyle">
    @import  "<?php echo e(asset('plugins/DataTables-1.10.12/media/css/jquery.dataTables.min.css')); ?>";
</style>     
<script src="<?php echo e(asset('plugins/jquery/js/jquery-3.1.1.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery/js/jquery-ui/jquery-1.12.4.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jquery/js/jquery-ui/jquery-ui.js')); ?>"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo e(asset('plugins/DataTables-1.10.12/media/js/jquery.dataTables.min.js')); ?>"></script>
    
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true
        });
  } );
  </script>
  
</head>

  <body>

   <?php echo $__env->make('admin.template.partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-8 col-md-offset-2">
                 <div class="panel panel-default">
                     <div class="panel-heading" style="font-size:18px"><strong><?php echo $__env->yieldContent('title_panel','Titulo del panel'); ?></strong></div>
                     <div class="panel-body">
                      <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                       <?php echo $__env->yieldContent('content'); ?>

                     </div>
                 </div>
             </div>
         </div>
    </div>

      <!--<script src="/jquery/jquery-3.1.1"></script>-->
      <!-- <script src="/js/app.js"></script> -->
      


  </body>
</html>
