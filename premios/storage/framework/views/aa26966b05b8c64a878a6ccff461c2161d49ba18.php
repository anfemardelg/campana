<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Editar usuario
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="row">
	    <div class="col-lg-12 margin-tb">

	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('users.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
	<?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
	<?php echo Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]); ?>

	<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>usuario:</strong>
		                <?php echo Form::text('username', null, array('placeholder' => 'usuario','class' => 'form-control')); ?>

		            </div>
		    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Nombres:</strong>
		                <?php echo Form::text('usr_name', null, array('placeholder' => 'Nombres','class' => 'form-control')); ?>

		            </div>
		    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Apellidos:</strong>
		                <?php echo Form::text('usr_lname', null, array('placeholder' => 'Apellidos','class' => 'form-control')); ?>

		            </div>
		    </div>

				<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
										<strong>Identificacion:</strong>
										<?php echo Form::text('usr_personalid', null, array('placeholder' => 'Nro identificacion','class' => 'form-control')); ?>

								</div>
				</div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <?php echo Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Password:</strong>
                <?php echo Form::password('password', array('placeholder' => 'Password','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Confirmar Password:</strong>
                <?php echo Form::password('confirm-password', array('placeholder' => 'Confirmar Password','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Roles:</strong>
                <?php echo Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')); ?>

            </div>
        </div>
				
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Submit</button>
        </div>
	</div>
	<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>