<?php $__env->startSection('tittle','Recepcion  Reparto'); ?>
<?php $__env->startSection('title_panel','Recepcion Reparto'); ?>
<?php $__env->startSection('content'); ?>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>CODIGO EXPEDIENTE</th>
        <th>CODIGO RESOLUCION</th>
        <th>FECHA RESOLUCION</th>
        <th>ACCION</th>
      </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $repartos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reparto): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
      <?php $detalle=App\Detalle::find($reparto->detalle_id); ?>     
     <tr>
        <td><?php echo e($reparto->id); ?></td>
        <td><?php echo e($detalle->detalle_codigo); ?></td>
        <td><?php echo e($detalle->detalle_resolucion); ?></td>
        <td><?php echo e($detalle->detalle_fecharesol); ?></td>
        <td style="width: 25%">
            <div  style="margin:0 auto; width: 100%; ">
          <a href="<?php echo e(route('reparto.recibir', $reparto->id)); ?>" onclick="return confirm('Esta seguro de Recibir el Expediente?')" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span></a> </div>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</div>
    </tbody>
  </table>
  <?php echo $repartos->render(); ?>


<?php $__env->stopSection(); ?>







<?php echo $__env->make('coactivo.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>