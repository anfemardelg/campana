<?php echo e(csrf_field()); ?>

<?php $__env->startSection('tittle','Lista de Anotaciones'); ?>
<?php $__env->startSection('title_panel','Lista de Anotaciones'); ?>
<?php $__env->startSection('content'); ?>

  <?php 
  $existe=0;
  foreach ($anotaciones as $var ) {
    $existe=1;
  }
  ?>

  <?php if (\Entrust::can('anotaciones-edit')) : ?>
  <?php if($existe==1): ?>
  <a href="<?php echo e(route('anotaciones.create', $idExpediente)); ?>" class="btn btn-info" disabled>Cambiar Estado</a>
  <?php else: ?>
    <a href="<?php echo e(route('anotaciones.create', $idExpediente)); ?>" class="btn btn-info">Cambiar Estado</a> 
  <?php endif; ?>
  <?php endif; // Entrust::can ?>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
 <script type="text/javascript">
$(document).ready(function(){
    $('#data_anotaciones').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<br>
 <div class="table-responsive">
  <table class=   "table">
                <tbody>
                  <tr>
                    <td>Nombre Sancionado:</td>
                    <td> <?php echo e($expediente[0]->antecedente_nombre); ?></td>
                  </tr>
                  <tr>
                    <td>Apellido Sancionado</td>
                    <td><?php echo e($expediente[0]->antecedente_apellido); ?></td>
                  </tr>

                   <tr>
                    <td>Direccion</td>
                    <td> <?php echo e($expediente[0]->antecedente_direccion1); ?></td>
                  </tr>
                   <tr>
                    <td>Numero Documento</td>
                    <td> <?php echo e($expediente[0]->antecedente_nro_documento); ?></td>
                  </tr>
                    <tr>
                    <td>Sancion</td>
                    <td> <?php echo e($expediente[0]->antecedente_sancion); ?></td>
                  </tr>
                   <tr>
                    <td>Sancion</td>
                    <td> <?php echo e($expediente[0]->antecedente_concepto); ?></td>
                  </tr>
                  <tr>
                    <td>Resolucion</td>
                    <td> <?php echo e($expediente[0]->antecedente_resolucion); ?></td>
                  </tr>
                  <tr>
                    <td>Fecha Resolucion</td>
                    <td> <?php echo e($expediente[0]->antecedente_fecharesol); ?></td>
                  </tr>
               </tbody>
              </table>  
</div> 

<hr style="border:1px solid black"/>
 <br>
<div id="dialog-form" title="Listado de Documentos"></div>  
  <table id="data_anotaciones" class="table table-striped display"  cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>PROCESO</th>
        <th>ESTADO</th>
        <th>ANOTACION</th>
        <th></th>  
      </tr>
    </thead>
    <tbody>       
    <?php $__currentLoopData = $anotaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $anotacion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>       
     <tr>
        <td><?php echo e($anotacion->id); ?></td>
        <td><?php echo e($anotacion->proceso_nombre); ?></td>
        <td><?php echo e($anotacion->estado_nombre); ?></td>
        <td><?php echo e($anotacion-> anotacion_anotaciones); ?></td>
        <td style="width: 25%"> 
            <div  style="margin:0 auto; width: 100%; ">
                <?php if($max_activo[0]->ultimo==$anotacion->id): ?>
                    <a href="<?php echo e(route('anotaciones.adicionarProceso', $anotacion->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
                <?php endif; ?>
                 <a onclick="ListarDocanotacion(<?php echo e($anotacion->id); ?>,'anotacionid')" class="btn btn-primary btn-xs" title="Visualizar Doumentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>     
            </div>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>