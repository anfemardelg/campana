<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                <div>
                 <img src="<?php echo e(asset('imagen/BancoOccidente.png')); ?>"  width="200" style="margin-top: -13%">
                </div>
            </a>
        </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
     
            <ul class="nav navbar-nav navbar-left">
               <?php if (\Entrust::can('admin-show')) : ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <?php if (\Entrust::can('admin-user-show')) : ?>
                    <li><a href="<?php echo e(route('users.index')); ?>">Usuarios</a></li>
                    <?php endif; // Entrust::can ?> 
                    <?php if (\Entrust::can('admin-rol-show')) : ?>  
                    <li><a href="<?php echo e(route('roles.index')); ?>">Roles</a></li>
                    <?php endif; // Entrust::can ?>
                  </ul>
                </li>
               <?php endif; // Entrust::can ?>  
               <?php if (\Entrust::can('importar-show')) : ?>    
                <li>
                  <a href="<?php echo e(route('importardata.index')); ?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false" >
                      Cargar Archivo<span class="caret"></span>
                  </a>                 
                </li>
                <?php endif; // Entrust::can ?>
				<?php if (\Entrust::can('reporte-show')) : ?>
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
                  <ul class="dropdown-menu">                    
                    <li><a href="<?php echo e(route('reportes.index')); ?>">Diario</a></li>
                    <li><a href="<?php echo e(route('reportes.agrupado')); ?>">Por bono</a></li>
                  </ul>
                </li>
				<?php endif; // Entrust::can ?>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                <?php if(Auth::guest()): ?>
                    <li><a href="<?php echo e(url('/login')); ?>">Ingresar</a></li>
                    <!-- <li><a href="<?php echo e(url('/register')); ?>">Registrarse</a></li> -->
                <?php else: ?>
               
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo e(Auth::user()->usr_name); ?> <?php echo e(Auth::user()->usr_lname); ?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?php echo e(url('/logout')); ?>"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>