<?php $__env->startSection('tittle','Edición de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Edición de Antecedentes'); ?>
<?php $__env->startSection('content'); ?>

 <?php echo Form::open(['route'=>['detalles.update',$antecedente],'method'=>'PUT','enctype'=>'multipart/form-data']); ?>

 

   <div class="form-group">
        <?php echo Form::label('antecedente_oficio','Oficio:'); ?>

        <?php echo Form::text('antecedente_oficio',$antecedente->antecedente_oficio,['class'=>'form-control','placeHolder'=>'Oficio','autocomplete'=>'off','required']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('antecedente_descripcion','Anotaciones:'); ?>

            <?php echo Form::textarea('antecedente_descripcion',$antecedente->antecedente_descripcion,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

    </div>

 <div class="form-group">
    <?php echo Form::hidden('antecedente_id',$antecedente->id); ?>

  </div>

  <div class="form-group">
  	<?php echo Form::label('antecedente_codigo','Expediente:'); ?>

  	<?php echo Form::text('antecedente_codigo',$antecedente->antecedente_codigo,['class'=>'form-control','placeHolder'=>'Expediente','required']); ?>

  </div>
   <div class="form-group">
    <?php echo Form::label('antecedente_nombre','Nombre Sancionado:'); ?>

    <?php echo Form::text('antecedente_nombre',$antecedente->antecedente_nombre,['class'=>'form-control','placeHolder'=>'Nombre','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_apellido','Apellido Sancionado:'); ?>

    <?php echo Form::text('antecedente_apellido',$antecedente->antecedente_apellido,['class'=>'form-control','placeHolder'=>'Apellido']); ?>

  </div>


   <div class="form-group">
    <?php echo Form::label('Tipo Documento:','Tipo Documento:'); ?>

    <?php echo Form::select('tipo_doc_id', ['1' => 'Cedula', '2' => 'Nit', '3' => 'Cedula de Extranjeria',  '4' => 'Pasaporte'], $antecedente->tipo_doc_id, ['class'=>'form-control','placeholder' => 'Seleccione el tipo...']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::hidden('antecendente_id',$antecedente->antecendente_id); ?>

  </div>


  <div class="form-group">
    <?php echo Form::label('antecedente_nro_documento','Numero Documento:'); ?>

    <?php echo Form::text('antecedente_nro_documento',$antecedente->antecedente_nro_documento,['class'=>'form-control','placeHolder'=>'Nro Documento']); ?>

  </div>
<div class="form-group">
    <?php echo Form::label('antecedente_telefono','Telefono:'); ?>

    <?php echo Form::text('antecedente_telefono',$antecedente->antecedente_telefono,['class'=>'form-control','placeHolder'=>'111-2222','autocomplete'=>'off']); ?>

  </div>

<div class="form-group">
    <?php echo Form::label('antecedente_email','Correo Electronico:'); ?>

    <?php echo Form::email('antecedente_email',$antecedente->antecedente_email,['class'=>'form-control','placeHolder'=>'aaa@yyyy.com','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_direccion1','Dirección Principal:'); ?>

    <?php echo Form::text('antecedente_direccion1',$antecedente->antecedente_direccion1,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>

  </div>
  <div class="form-group">
    <?php echo Form::label('antecedente_direccion2','Dirección Opcional:'); ?>

    <?php echo Form::text('antecedente_direccion2',$antecedente->antecedente_direccion2,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>

  </div>


  <div class="form-group">
  	<?php echo Form::label('antecedente_sancion','Sanción:'); ?>

  	<?php echo Form::text('antecedente_sancion',$antecedente->antecedente_sancion,['class'=>'form-control','placeHolder'=>'Sanción','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_concepto','Concepto:'); ?>

    <?php echo Form::textarea('antecedente_concepto',$antecedente->antecedente_concepto,['class'=>'form-control','placeHolder'=>'Concepto','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_resolucion','Resolución:'); ?>

    <?php echo Form::text('antecedente_resolucion',$antecedente->antecedente_resolucion,['class'=>'form-control','placeHolder'=>'Resolución','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_fecharesol','Fecha Resolución:'); ?>

    <?php echo Form::text('antecedente_fecharesol',$antecedente->antecedente_fecharesol,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'datepicker']); ?>

  </div>
 <div class="form-group">
   <?php echo Form::label('userid','Nombre Abogado:'); ?>

   <?php echo Form::select('userid',$abogado,[$antecedente->userid] , ['class'=>'form-control','placeholder' => 'Seleccione Abogado...'] );; ?>

  </div>
 <div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
            <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx'); ?>

   </div>
   <div class="form-group">
  	<?php echo Form::button('&nbsp;Actualizar ',['class'=>'btn btn-primary glyphicon glyphicon-floppy-disk','type'=>'submit','title'=>'Click para Actualizar']); ?>

  </div>
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>