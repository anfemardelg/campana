<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Administracion de usuarios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-success" href="<?php echo e(route('users.create')); ?>"> Crear usuarios</a>
	        </div>
	    </div>
	</div>
<br/>
	<?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Usuario</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Roles</th>
			<th>Accion</th>
		</tr>
	<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($user->username); ?></td>
		<td><?php echo e($user->usr_name); ?></td>
		<td><?php echo e($user->usr_lname); ?></td>
		<td>
			<?php if(!empty($user->roles)): ?>
				<?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<label class="label label-success"><?php echo e($v->display_name); ?></label>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			<?php endif; ?>
		</td>
		<td>
			<!-- <a class="btn btn-info" href="<?php echo e(route('users.show',$user->id)); ?>">Show</a> -->
			<a class="btn btn-primary" href="<?php echo e(route('users.edit',$user->id)); ?>">Editar</a>
			<!-- <?php echo Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']); ?> -->
            <!-- <?php echo Form::submit('Delete', ['class' => 'btn btn-danger']); ?> -->
        	<?php echo Form::close(); ?>

		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	</table>
	<?php echo $data->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>