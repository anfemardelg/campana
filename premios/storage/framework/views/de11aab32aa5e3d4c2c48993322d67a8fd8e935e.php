<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte Por bono'); ?>
<?php $__env->startSection('title_panel','Reporte'); ?>
<?php $__env->startSection('content'); ?>

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte por bono
		</legend>
		<table id="Agrupado" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Bono</th>
				<th>Cantidad</th>                   
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $agrupado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $redencion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($redencion->bono); ?></td>
					<td><?php echo e($redencion->cant); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>