<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Liquidador'); ?>
<?php $__env->startSection('title_panel','LIQUIDACION DE INTERESES MORATORIOS'); ?>
<?php $__env->startSection('content'); ?>  

<script type="text/javascript">
  function cuotasdinamicas(){
      var token = $('input:hidden[name=_token]').val();
      var cuotas =$('#nrocuotas').val();
      var valortotal =$('#valortotal').val(); 
      var encid =$('#encid').val(); 
      //var fechapago =$('#fechapago').val();
      //var resultado = parseFloat(valortotal)/parseInt(cuotas);
      console.log(encid);
      $.ajax({ //Ajax
            type: 'POST',
            headers:{'X-CSRF-TOKEN':token},
            url: '../Cuotasdinamicas',
            async: false,
            dataType: 'html',
            data:({cuotas: cuotas,valortotal:valortotal,encid:encid}),
            error: function (objeto, quepaso, otroobj) {
                alert('Error de Conexi?n , Favor Vuelva a Intentar');
            },
            success: function (data) {
                   $('#contenedorcuotas').html(data);
                } //DATA
        }); //AJAX

  }
</script>
      <?php if($liqview): ?>
        <table id="res" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>RESOLUCION</th>
              <th>RAZON SOCIAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo e($liquidacion[0]->antecedente_resolucion); ?></td>
              <td><?php echo e($liquidacion[0]->razonsocial); ?></td>
            </tr>
          </tbody>
        </table>

        <br/>
        <table id="Liq" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>IMPUESTOS</th>
              <th>VENCIMIENTO LEGAL</th>  
              <th>FECHA DE PAGO</th>
            </tr>
          </thead>
          <tbody>
            <tr>   
              <td>$<?php echo e(number_format($liquidacion[0]->valor_base)); ?></td>
              <td><?php echo e($liquidacion[0]->vencimiento_legal); ?></td>
              <td><?php echo e($liquidacion[0]->fecha_pago); ?></td>
              <td> <?php echo Form::hidden('encid', $liquidacion[0]->id, array('class' => 'form-control', 'id'=>'encid')); ?></td>  
            </tr>
          </tbody>
        </table>
       <?php endif; ?>  
       <?php if($liqview2): ?>
       <table id="rzocial" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>RAZON SOCIAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo e($liqsinres[0]->empresa); ?>

<?php echo Form::hidden('encid', $liqsinres[0]->id, array('class' => 'form-control', 'id'=>'encid')); ?>

              </td>
            </tr>
          </tbody>
        </table>
       <?php endif; ?>

        <br/>
        <table id="detalle" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>PORCENTAJE</th>
              <th>DESDE</th>  
              <th>HASTA</th>
              <th>T.DIARIA</th>
              <th>DIAS</th>
              <th>INTERES</th>
              <th>NUEVO SALDO</th>
              <th>ACUMULA</th>
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $detalle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
           <tr>    
              <td><?php echo e(number_format($det->porcentaje_anual,2)); ?>%</td>
              <td><?php echo e($det->fecha_inicio); ?></td>
              <td><?php echo e($det->fecha_fin); ?></td>
              <td><?php echo e($det->tasa_diaria); ?>%</td>
              <td><?php echo e($det->dias_mora); ?></td>
              <td>$&nbsp<?php echo e(number_format($det->interes)); ?></td>
              <td>$&nbsp<?php echo e(number_format($det->nuevo_saldo)); ?></td>  
              <td style="text-align: right;"><?php echo e($det->dias_acumulados); ?></td>  
           </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
          </tbody>
        </table>
        <br/>
        <table id="resumen" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>RESUMEN</th>     
            </tr>
          </thead>
          <tbody>
            <tr>  
             <?php if($liqview): ?>
              <td>VALOR IMPUESTO</td>  
              <td>$<?php echo e(number_format($liquidacion[0]->valor_base,2)); ?></td>   
             <?php endif; ?>
             <?php if($liqview2): ?>
              <td>VALOR IMPUESTO</td>  
              <td>$<?php echo e(number_format($liqsinres[0]->valor_base,2)); ?></td>   
             <?php endif; ?>
            </tr>
            <tr>  
              <td>VALOR INTERES</td>  
              <td>$<?php echo e(number_format($resumen[0]->t_interes,2)); ?></td>     
            </tr>
            <tr>  
              <td>VALOR A PAGAR</td>  
              <td>$<?php echo e(number_format($resumen[0]->t_nuevo_saldo,2)); ?> 
                  <?php echo Form::hidden('valortotal', $resumen[0]->t_nuevo_saldo, array('class' => 'form-control', 'id'=>'valortotal')); ?>

              </td>    
            </tr>
            <tr>
              <td>NUMERO DE CUOTAS</td>  
              <td>
               <div class="col-xs-2">
                 <?php echo Form::text('nrocuotas', null, array('class' => 'form-control', 'onBlur'=>'cuotasdinamicas()', 'id'=>'nrocuotas')); ?>

               </div>  
              </td>    
            </tr>
            <tr>
              <td colspan="2">
                <div id="contenedorcuotas">
                  
                </div>
              </td>
            </tr>
          </tbody>
        </table>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>