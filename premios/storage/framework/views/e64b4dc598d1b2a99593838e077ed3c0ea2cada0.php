<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Estado de bonos'); ?>
<?php $__env->startSection('title_panel','Bonos'); ?>
<?php $__env->startSection('content'); ?>

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Estado de los bonos
		</legend>
		<table id="Estados" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Numero</th>
				<th>Bono</th>
				<th>Documento</th>
				<th>Usuario</th>
				<th>Estado</th>
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $estados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $estado): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($estado->id_bono); ?></td>
					<td><?php echo e($estado->name); ?></td>
					<td><?php echo e($estado->documento); ?></td>
					<td><?php echo e($estado->nombre); ?></td>
					<td><?php echo e($estado->estado); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>