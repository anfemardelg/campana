<?php echo e(csrf_field()); ?>

<?php $__env->startSection('tittle','Lista de Expedientes'); ?>
<?php $__env->startSection('title_panel','Lista de Expedientes'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/reparto.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.TableExpediente').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Antecedentes Sin Expediente</a></li>
    <li><a href="#tabs-2">Expedientes en proceso</a></li>
    <li><a href="#tabs-3">Expedientes por Vencer</a></li>
  </ul>
  <div id="tabs-1">
    <?php if (\Entrust::can('expedientes-create')) : ?>
    <a href="<?php echo e(route('expedientes.create')); ?>" class="btn btn-info">Registrar Expediente</a> 
    <?php endif; // Entrust::can ?>
      <br/>
      <br>
      <table id="sinExpediente" class="table table-striped display TableExpediente"  cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>RADICADO</th>
        <th>FOLIOS</th>
        <th>ESTADO</th>
        <th>FECHA RADICACION</th>
        <th>ACCION</th>
      </tr>
    </thead>
    <tbody>
  <?php $__currentLoopData = $antecentesinexpediente; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ante): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr>
        <td><?php echo e($ante->id); ?></td>
        <td><?php echo e($ante->antecedente_oficio); ?></td>
        <td><?php echo e($ante->antecedente_codigo); ?></td>
        <td></td> 
       <td><?php echo e($ante->created_at); ?></td>
        <td style="width: 25%"> 
        <div  style="margin:0 auto; width: 100%; ">
       
        </div>
        </td>
      </tr>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>      
  </div>
  <div id="tabs-2">
    <div id="dialog-form" title="Listado de Documentos"></div>  
    <table class="table table-striped TableExpediente" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>ID</th>
        <th>RADICADO</th>
        <th>FOLIOS</th>
        <th>ESTADO</th>
        <th>FECHA RADICACION</th>
        <th>ACCION</th>
      </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $expedientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $expediente): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
     <tr>
        <td><?php echo e($expediente->id); ?></td>
        <td><?php echo e($expediente->expediente_rad); ?></td>
        <td><?php echo e($expediente->expediente_folio); ?></td>
        <td>'RADICADO - APERTURA EXPEDIENTE'</td>
        <td><?php echo e($expediente->expediente_fecha); ?></td>
        <td style="width: 25%"> 
        <div  style="margin:0 auto; width: 100%; ">
        <?php if (\Entrust::can('expedientes-edit')) : ?>
        <a href="<?php echo e(route('expedientes.edit', $expediente->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
        <?php endif; // Entrust::can ?>
         <a href="<?php echo e(route('anotaciones.listar', $expediente->id)); ?>" class="btn btn-success btn-xs" title="Adicionar o Modificar Anotaciones" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>   
        <a onclick="ListarDocumentos(<?php echo e($expediente->id); ?>,'expedienteid')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>     
        </div>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
   
  </div>
    <div id="tabs-3">
        <table class="table table-striped TableExpediente" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Folio</th>
                <th>Radicado</th>
                <th>Fecha</th>  
                <th>Proceso</th>
                <th>Estado</th>
                <th>#Días</th>
                <th>opciones</th>
              </tr>
            </thead>
            <tbody>
                 <?php $__currentLoopData = $venciendo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ven): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <tr>
                    <td><?php echo e($ven->expedienteid); ?></td>                        
                    <td><?php echo e($ven->expediente_folio); ?></td>      
                    <td><?php echo e($ven->expediente_rad); ?></td>
                    <td><?php echo e($ven->expediente_fecha); ?></td>
                    <td><?php echo e($ven->proceso_nombre); ?></td>
                    <td><?php echo e($ven->estado_nombre); ?></td>
                    <td><?php echo e($ven->proceso_dias); ?></td>
                    <td></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>
        </table>
        
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>