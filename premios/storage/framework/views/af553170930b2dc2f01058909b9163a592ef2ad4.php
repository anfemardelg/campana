<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Voluntad de pago
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Voluntad de pago
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#vpago').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>


<table id="vpago" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Resolucion</th>
			<th>Razon social</th>
			<th>radicado expediente</th>
			<th>Accion</th>
		</tr>
		</thead>
	<tbody>
	<?php $__currentLoopData = $voluntadpago; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $vpago): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($vpago->antecedente_resolucion); ?></td>
		<td><?php echo e($vpago->razonsocial); ?></td>
		<td><?php echo e($vpago->expediente_rad); ?></td>
		<td>		
		    <?php if (\Entrust::can('voluntadpago-edit')) : ?>
			<a class="btn btn-primary" href="<?php echo e(route('vpago.edit',$vpago->id)); ?>">Editar</a>
			<?php endif; // Entrust::can ?>

		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	</tbody>
	</table>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>