<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Crear Roles
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Roles
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#tablaroles').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$('#eliminar').click(function(){
			if(confirm('Seguro desea eliminar el rol...?')){
				return true;
			}else{
				return false;
			}
		});
	});
</script>
	<div class="row">
	    <div class="col-lg-12 margin-tb">

	        <div class="pull-right">
	        	<?php if (\Entrust::can('role-create')) : ?>
	            <a class="btn btn-success" href="<?php echo e(route('roles.create')); ?>"> Crear Rol</a>
	            <?php endif; // Entrust::can ?>
	        </div>
	    </div>
	</div>
	<br>
	<?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
	<table  id="tablaroles" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Nombre</th>
			<th>Descripcion</th>
			<th width="200px">Accion</th>
		</tr>
		</thead>
	<tbody>	
	<?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $role): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($role->display_name); ?></td>
		<td><?php echo e($role->description); ?></td>
		<td>
			<!-- <a class="btn btn-info" href="<?php echo e(route('roles.show',$role->id)); ?>">Mostrar</a> -->
			<?php if (\Entrust::can('role-edit')) : ?>
			<a class="btn btn-primary" href="<?php echo e(route('roles.edit',$role->id)); ?>">Editar</a>
			<?php endif; // Entrust::can ?>
			<?php if (\Entrust::can('role-delete')) : ?>

			<?php echo Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']); ?>

            <?php echo Form::submit('Eliminar', ['class' => 'btn btn-danger','id'=>'eliminar']); ?>

        	<?php echo Form::close(); ?>

       	<?php endif; // Entrust::can ?>
		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	</tbody>
	</table>
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>