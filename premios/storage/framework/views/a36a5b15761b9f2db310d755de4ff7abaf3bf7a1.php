<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Anotaciones'); ?>
<?php $__env->startSection('title_panel','Anotaciones'); ?>
<?php $__env->startSection('content'); ?>

  <?php 
  $existe=0;
  foreach ($anotaciones as $var ) {
    $existe=1;
  }
  ?>
<div class="row">
     <div class="col-lg-12 margin-tb">
  <?php if (\Entrust::can('anotaciones-edit')) : ?>
  <?php if($existe==1): ?>
  
  <?php else: ?>
<div class="pull-left">
    <a id="Button" href="<?php echo e(route('anotaciones.create', $idExpediente)); ?>" class="btn btn-info">Cambiar Estado</a> 
</div>    
  <?php endif; ?>
  <?php endif; // Entrust::can ?>
	        <div class="pull-right">
	            <a  id="Button"  href="<?php echo e(route('expedientes.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
 <script type="text/javascript">
$(document).ready(function(){
    $('#data_anotaciones').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<br/>
<br/>

 <div class="table-responsive">
     <div style="text-align:right">         
        <a data-toggle="collapse" data-target="#demo" style="cursor:pointer">Ver más información</a>    
    </div>     

 <table class="table table-responsive">
    <tbody>
    <?php if($ROL_user==2): ?>      
        <tr>
            <td><strong>Abogado:</strong></td>
            <td> <?php echo e($expediente[0]->usr_name); ?>&nbsp;<?php echo e($expediente[0]->usr_lname); ?></td>
        </tr>  
    <?php endif; ?>    
        <tr>
            <td><strong>Razón Social ó Nombre:</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_nombre); ?>&nbsp;<?php echo e($expediente[0]->antecedente_apellido); ?></td>
        </tr>       
        <tr>
            <td><strong>Dirección</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_direccion1); ?></td>
        </tr>
        <tr>
            <td><strong>Número Documento</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_nro_documento); ?></td>
        </tr>
        <tr>
            <td><strong>Valor</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_sancion); ?>&nbsp;&nbsp;<?php echo e($expediente[0]->TextCategoria); ?></td>

        </tr>
       
        <tr>
            <td><strong>Resolución</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_resolucion); ?></td>
        </tr>
        <tr>
            <td><strong>Fecha Resolución</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_fecharesol); ?></td>
        </tr>
    </tbody>
</table>  
 
 <div id="demo" class="collapse">
    <table class= "table table-responsive">
    <tbody>
        <tr>
            <td colspan="2"><strong>Dirección Alterna</strong></td>
        </tr>
        <tr>
            <td colspan="2">
                <table  class= "table table-responsive">
                    <thead>
                        <tr>
                            <th>Dirección</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $direcciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dir): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <tr>
                                <td><?php echo e($dir->direccion); ?></td>
                                <td><?php echo e($dir->telefono); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><strong>E-mail ó Correo Electronico</strong></td>
            <td><?php echo e($expediente[0]->antecedente_email); ?></td>
        </tr>
        <tr>
            <td><strong>Telefono</strong></td>
            <td><?php echo e($expediente[0]->antecedente_telefono); ?></td>
        </tr>
        <tr>
            <td><strong>Oficio</strong></td>
            <td><?php echo e($expediente[0]->antecedente_oficio); ?></td>
        </tr>
        <tr>
            <td><strong>Descripción</strong></td>
            <td><?php echo e($expediente[0]->antecedente_descripcion); ?></td>
        </tr>
        <tr>
            <td><strong>Estado</strong></td>
            <td><?php echo e($expediente[0]->parametro_descripcion); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Radicado</strong></td>
            <td><?php echo e($expediente[0]->expediente_rad); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Folio</strong></td>
            <td><?php echo e($expediente[0]->expediente_folio); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Descripción</strong></td>
            <td><?php echo e($expediente[0]->expediente_descripcion); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Fecha</strong></td>
            <td><?php echo e($expediente[0]->expediente_fecha); ?></td>
        </tr>
    </tbody>
    </table>  
  </div>
</div> 

<hr style="border:1px solid black"/>
 <br>
<div id="dialog-form"  title="Listado de Documentos"></div>  
  <table id="data_anotaciones" class="table table-striped display table-responsive"  cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>#</th>
        <th>CONSECUTIVO</th>  
        <th>FECHA</th>  
        <th>ETAPA</th>
        <th>ACTIVIDAD</th>
        <th>ANOTACION</th>
         <?php if($ROL_user!=2): ?>   
        <th>OPCIONES</th>  
        <?php endif; ?>  
      </tr>
    </thead>
    <tbody>    
    <?PHP 
    $i=0;
    ?>    
    <?php $__currentLoopData = $anotaciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $anotacion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>       
     <tr>
        <td><?php echo e($i+1); ?></td>
        <td><?php echo e($anotacion->Dato); ?></td>
        <td><?php echo e($anotacion->fecha); ?></td>        
        <td><?php echo e($anotacion->proceso_nombre); ?></td>
        <td><?php echo e($anotacion->estado_nombre); ?></td>
        <td><?php echo e($anotacion->anotacion_anotaciones); ?></td>
        <td style="width: 25%"> 
            <div  style="margin:0 auto; width: 100%; ">
            <?PHP 
            
                if(($anotacion->procesoid==10) || ($anotacion->procesoid==19) || ($anotacion->procesoid==9)){
                    $ver_icon = false;
                }else{
                    $ver_icon = true;
                }
                
            ?>
            <?php if($ver_icon): ?>
                <?php if($max_activo[0]->ultimo==$anotacion->id): ?>					
						<a href="<?php echo e(route('anotaciones.adicionarProceso', $anotacion->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>					 
                <?php endif; ?>
            <?php endif; ?>    
                <?php $__currentLoopData = $ViewDoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                     <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($view->valor_id == $anotacion->id): ?>
                             <a onclick="ListarDocanotacion(<?php echo e($anotacion->id); ?>,'anotacionid')" class="btn btn-primary btn-xs" title="Visualizar Doumentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>   
                        <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </div>
        </td>        
      </tr>
        <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>