<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Repositorio Carga Usuarios'); ?>
<?php $__env->startSection('title_panel','Repositorio De Usuarios Cargados'); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <fieldset>
            <legend>Repositorio De Usuarios Cargados</legend>
            
            <br/>
            <br/>

            <table id="Repositorio" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha de Carga</th>
                        <th>Nombre Archivo</th>
                        <th>Opción</th>                  
                    </tr>
                </thead>            
                <tbody>
                    <?php 
                    $n=1;
                     ?>
                    <?php $__currentLoopData = $Datos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
                    <tr>
                        <td><?php echo e($n++); ?></td>
                        <td style="text-align:center"><?php echo e(Carbon\Carbon::parse($Row->fechacarga)->format('Y-m-d')); ?></td>
                        <td><?php echo e($Row->archivo); ?></td>
                        <td>
                            <div style="margin-left: 40%;">
                                <img src="<?php echo e(asset('imagen/icono/1494555369_excel.png')); ?>" id="DescargarData" onclick="DownloadRepositorio('<?php echo e($Row->idrepositorio); ?>')" style="cursor:pointer" width="20" title="Descargar Datos" >
                            </div>    
                        </td>
                    </tr>     
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </tbody>    
            </table>  
            </fieldset>
        </div>
    </div>
</div>   

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="application/javascript" src="<?php echo e(asset('js/redemir/importardata/repositorio.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>