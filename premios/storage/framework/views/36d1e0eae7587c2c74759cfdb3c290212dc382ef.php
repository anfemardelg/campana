<script type="text/javascript" src="<?php echo e(asset('js/parametro.js')); ?>"></script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('parametro.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['parametro.update',$parametro->id],'method'=>'PUT','enctype'=>'multipart/form-data']); ?>

<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<?php echo Form::hidden('parametro_id',$parametro->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<div class="form-group">
    <?php echo Form::label('Dominio','Dominio:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::text('Dominio',$parametro->parametro_dominio,['class'=>'form-control','placeHolder'=>'Escriba el Dominio ','autocomplete'=>'off','required','onkeypress'=>'autocompleteDominio()','id'=>'Dominio']); ?>    
</div>
<div class="form-group">
    <?php echo Form::label('Codigo','Codigo:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::text('codigo',$parametro->parametro_codigo,['class'=>'form-control','placeHolder'=>'Escriba Codigo','autocomplete'=>'off','required']); ?>    
</div>
<div class="form-group">
    <?php echo Form::label('descrip','Descripción:'); ?>

    
    <?php echo Form::text('descripciom',$parametro->parametro_descripcion,['class'=>'form-control','placeHolder'=>'Descripción... ','autocomplete'=>'off']); ?>    
</div>
<div class="form-group">
    <?php echo Form::label('estado','Estado:'); ?>

    
    <?php echo Form::select('estado', $Estado, $parametro->parametro_estado, ['class'=>'form-control','placeholder' => 'Seleccione el Estado...']); ?>

</div>
<div class="form-group">
   <?php echo Form::button('&nbsp;Modificar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Modificar','type'=>'submit']); ?>

</div>
<?php echo Form::close(); ?>