<?php $__env->startSection('title','Anotaciones del Expediente'); ?>
<?php $__env->startSection('title_panel','Anotaciones del Expediente'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/reparto.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/anotaciones.js')); ?>"></script>
<script>
  $( function() {
    $( "#fecha" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true
        });            
  } );
  </script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('anotaciones.listar',$C_expediente->id)); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['anotaciones.store'],'method'=>'POST','enctype'=>'multipart/form-data','id'=>'formAntecedente']); ?>

<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<div class="form-group">
   <?php echo Form::hidden('expedienteid',$C_expediente->id,['class'=>'','id'=>'expedienteid']); ?>

</div>
<div class="table-responsive">
     <div style="text-align:right">         
        <a data-toggle="collapse" data-target="#demo" style="cursor:pointer">Ver más información</a>    
    </div>     

 <table class="table table-responsive">
    <tbody>
    <?php if($ROL_user==2): ?>      
        <tr>
            <td><strong>Abogado:</strong></td>
            <td> <?php echo e($expediente[0]->usr_name); ?>&nbsp;<?php echo e($expediente[0]->usr_lname); ?></td>
        </tr>  
    <?php endif; ?>    
        <tr>
            <td><strong>Razón Social ó Nombre:</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_nombre); ?>&nbsp;<?php echo e($expediente[0]->antecedente_apellido); ?></td>
        </tr>       
        <tr>
            <td><strong>Dirección</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_direccion1); ?></td>
        </tr>
        <tr>
            <td><strong>Número Documento</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_nro_documento); ?></td>
        </tr>
        <tr>
            <td><strong>Valor</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_sancion); ?>&nbsp;&nbsp;<?php echo e($expediente[0]->TextCategoria); ?></td>

        </tr>
        <tr>
            <td><strong>Concepto</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_concepto); ?></td>
        </tr>
        <tr>
            <td><strong>Resolución</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_resolucion); ?></td>
        </tr>
        <tr>
            <td><strong>Fecha Resolución</strong></td>
            <td> <?php echo e($expediente[0]->antecedente_fecharesol); ?></td>
        </tr>
    </tbody>
</table>  
 
 <div id="demo" class="collapse">
    <table class= "table table-responsive">
    <tbody>
        <tr>
            <td colspan="2"><strong>Dirección Alterna</strong></td>
        </tr>
        <tr>
            <td colspan="2">
                <table  class= "table table-responsive">
                    <thead>
                        <tr>
                            <th>Dirección</th>
                            <th>Telefono</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $direcciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dir): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <tr>
                                <td><?php echo e($dir->direccion); ?></td>
                                <td><?php echo e($dir->telefono); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><strong>E-mail ó Correo Electronico</strong></td>
            <td><?php echo e($expediente[0]->antecedente_email); ?></td>
        </tr>
        <tr>
            <td><strong>Telefono</strong></td>
            <td><?php echo e($expediente[0]->antecedente_telefono); ?></td>
        </tr>
        <tr>
            <td><strong>Oficio</strong></td>
            <td><?php echo e($expediente[0]->antecedente_oficio); ?></td>
        </tr>
        <tr>
            <td><strong>Descripción</strong></td>
            <td><?php echo e($expediente[0]->antecedente_descripcion); ?></td>
        </tr>
        <tr>
            <td><strong>Estado</strong></td>
            <td><?php echo e($expediente[0]->antecedente_estado); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Radicado</strong></td>
            <td><?php echo e($expediente[0]->expediente_rad); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Folio</strong></td>
            <td><?php echo e($expediente[0]->expediente_folio); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Descripción</strong></td>
            <td><?php echo e($expediente[0]->expediente_descripcion); ?></td>
        </tr>
        <tr>
            <td><strong>Expediente Fecha</strong></td>
            <td><?php echo e($expediente[0]->expediente_fecha); ?></td>
        </tr>
    </tbody>
    </table>  
  </div>
</div> 

<div class="form-group">
    <?php echo Form::label('year','Año del Consecutivo:'); ?>

    <span style="color:red"><strong>*</strong></span>  
    <?php echo Form::select('year',$C_year,null,['class'=>'form-control','placeholder'=>'Seleccione el Año del Consecutivo','required','id'=>'year']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('fecha_','Fecha:'); ?>

    <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('fecha',null,['class'=>'form-control','placeholder'=>'mm/dd/yyyy','required', 'id'=>'fecha','readonly'=>'readonly','autocomplete'=>'off']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('Procesoid','Etapa:'); ?>

    <span style="color:red"><strong>*</strong></span>         
    <?php echo Form::select('Procesoid',$C_proceso, null, ['class'=>'form-control','placeholder' => 'Seleccione Proceso...','required'] );; ?>

</div>
  <div class="form-group">
    <?php echo Form::label('Estadoid','Activida:'); ?>

    <span style="color:red"><strong>*</strong></span>  
     <div id="DivSelect">  
    <?php echo Form::select('Estadoid',$C_estado,null,['class'=>'form-control','placeholder'=>'Seleccione el Estado','required','id'=>'Estadoid']); ?>

      </div> 
  </div>

  <div class="form-group">
  	<?php echo Form::label('anotaciones_descripcion','Anotaciones:'); ?>

    <span style="color:red"><strong>*</strong></span>           
  	<?php echo Form::textArea('anotacion_anotaciones',null,['class'=>'form-control','placeholder'=>'Anotaciones','required']); ?>

  </div>
<div class="form-group">
            <?php echo Form::label('anotaciones_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
             <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

   </div>
   <div class="form-group">
  	<?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit','id'=>'SaveAnotacion']); ?>

  </div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>