<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Administracion de usuarios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Usuarios
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>

	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
            <?php if (\Entrust::can('user-create')) : ?>
	            <a class="btn btn-success" href="<?php echo e(route('users.create')); ?>"> Crear Usuarios</a>
            <?php endif; // Entrust::can ?>
	        </div>
	    </div>
	</div>
<br/>
	<?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
	<table id="tablauser" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Usuario</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Estado</th>
			<th width="200px">Roles</th>
			<th width="200px">Accion</th>
		</tr>
		</thead>
	<tbody>
	<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($user->username); ?></td>
		<td><?php echo e($user->usr_name); ?></td>
		<td><?php echo e($user->usr_lname); ?></td>
		<td> 

			<?php if($user->usr_status==0): ?>
				Inactivo
			<?php else: ?>
				Activo
			<?php endif; ?>	

		</td>
		<td>
			<?php if(!empty($user->roles)): ?>
				<?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<label class="label label-success"><?php echo e($v->display_name); ?></label>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			<?php endif; ?>
		</td>
		<td>
		
    <?php if (\Entrust::can('user-edit')) : ?>
			<a class="btn btn-primary" href="<?php echo e(route('users.edit',$user->id)); ?>">Editar</a>
			<?php echo Form::open(['method' => 'UPDATE','route' => ['users.user_status_updt', $user->id],'style'=>'display:inline']); ?>

            <?php echo Form::submit('Estado', ['class' => 'btn btn-danger','id'=>'Estado']); ?>

        	<?php echo Form::close(); ?>

    <?php endif; // Entrust::can ?>

			

		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
	</tbody>
	</table>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>