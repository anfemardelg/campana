<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Liquidador'); ?>
<?php $__env->startSection('title_panel','LIQUIDACION DE INTERESES MORATORIOS'); ?>
<?php $__env->startSection('content'); ?>

<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>

<script>
  $( document).ready(function() {
    $( ".datepicker2" ).datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
           
        });
    $( ".datepicker3" ).datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            minDate:0
        });
    
  });


 function autocomplete_oficio() { 
		 var token = $('input:hidden[name=_token]').val();
		
		 $('#antecedente_oficio').autocomplete({

		           source: "autocompletar",
		           headers:{'X-CSRF-TOKEN':token},
		           minLength: 2,
		           select: function( event, ui ) {
		           		
		                   $('#id_oficio').val(ui.item.id);
		                   
		           }                
		       });
  }

  function formatcaja(){
  	 $('#antecedente_oficio').val('');
  	 $('#id_oficio').val('');
  }

  function comparar_fecha(){
  	 var fecha_vencimiento =   $('#fecha_venc').val();
  	 var fecha_pago =   $('#fecha_pag').val();

     //Split de las fechas recibidas para separarlas
    var x = fecha_vencimiento.split("/");
    var z = fecha_pago.split("/");
//Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
    f_vencimiento = x[2] + "-" + x[1] + "-" + x[0];
    f_pago = z[2] + "-" + z[1] + "-" + z[0];


	 if (Date.parse(f_vencimiento) > Date.parse(f_pago)){
	  		alert('Fecha de vencimiento no puede ser superior a fecha de pago !!!');
	  		return false;
	    }
	    else{
	        return true;
	    	 }

  }

  </script>

<?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
<?php echo Form::open(array('route' => 'liquidador.store','method'=>'POST')); ?>


<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>INFORMACION</th>
				<th>INFORMACION GENERAL</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
				 <strong>Empresa:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>

				<td>
					<div class="col-xs-6">
					   <?php echo Form::text('empresa', null, array('placeholder' => 'Nombre empresa','class' => 'form-control ','required' )); ?>

					</div>
				</td>
			</tr>
			<!--<tr>
				<td>
				 <strong>Tipo de impuesto:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>
				<td>
					<div class="col-xs-6">-->
					   <!---<?php echo Form::text('tipo_impuesto', null, array('placeholder' => 'Tipo de impuesto','class' => 'form-control ','required' )); ?>-->
					<!--</div>
				</td>
			</tr>
			<tr>
				<td>
				 <strong>Periodo gravable:</strong>
                 <span style="color:red"><strong>*</strong></span>
                 </td>
				<td>
					<div class="col-xs-6">-->
					   <!--<?php echo Form::text('periodo_gravable', null, array('placeholder' => 'Año periodo gravable','class' => 'form-control ','onkeypress'=>'return validarNumeros(event)','maxlength'=>'4','required' )); ?>-->
					<!--</div>
				</td>
			</tr>-->
			<tr>
				<td>
				 <strong>Valor base:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>
				<td>
					<div class="col-xs-6">
					   <?php echo Form::text('valor_base', null, array('placeholder' => 'Valor base','class' => 'form-control ','onkeypress'=>'return validarNumeros(event)','required' )); ?>

					</div>
				</td>
			</tr>
			<tr>
				<td>
				 <strong>Vencimiento Legal:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>
				<td>
					<div class="col-xs-6">
					   <?php echo Form::text('vencimiento_legal', null, array('placeholder' => 'Fecha vencimiento legal','class' => 'form-control datepicker2','autocomplete'=>'off','required' ,'id'=>'fecha_venc')); ?>

					</div>
				</td>
			</tr>
			<tr>
				<td>
				 <strong>Fecha de pago:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>
				<td>
					<div class="col-xs-6">
					   <?php echo Form::text('fecha_pago', null, array('placeholder' => 'Fecha de pago','class' => 'form-control datepicker3','autocomplete'=>'off','required','id'=>'fecha_pag')); ?>

					</div>
				</td>
			</tr>
			<tr>
				<td>
				 <strong>Tipo liquidacion:</strong>
                 <span style="color:red"><strong>*</strong></span>
				</td>
				<td>
				  <div class="col-xs-6">
				   <?php echo Form::select('tipoliquidacion',$tipoliquidacion, null,['class'=>'form-control'] );; ?>

				  </div>
				</td>
			</tr>
			<tr>
				<td>
				 <strong>Resolucion:</strong>
				</td>
				<td>
				  <div class="col-xs-6">
				  <?php echo Form::text('antecedente_oficio', null, array('placeholder' => 'Seleccione numero de oficio','class' => 'typeahead form-control','id' => 'antecedente_oficio', 'onKeyPress'=> 'autocomplete_oficio()','onClick'=>'formatcaja()')); ?>

				  <?php echo Form::hidden('id_oficio', null, array('placeholder' => 'Digite numero de resolucion','class' => 'typeahead form-control','id' => 'id_oficio')); ?>

				  </div>
				</td>
			</tr>
		</tbody>
	</table>  
</div>   
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
	<button type="submit" onclick=" return comparar_fecha()" >Liquidar</button>
</div>

<?php echo Form::close(); ?>   
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>