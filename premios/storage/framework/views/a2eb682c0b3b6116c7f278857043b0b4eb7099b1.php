<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
    Actualizar campaña
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
    Actualizar campaña
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('campanas.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>URL Campaña:</strong>
            <span style="color:red"><strong>*</strong></span>
            <?php echo Form::text('url', null, array('placeholder' => 'URL','class' => 'form-control')); ?>

        </div>
        
        <button type="submit" class="btn btn-primary">Copiar</button>
    </div>
    <?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>		
        
	<?php endif; ?>
	<?php echo Form::model($ps_shop, ['method' => 'PATCH','route' => ['users.update', $user->id]] ,'enctype'=>'multipart/form-data')); ?>

    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::text('nombre', null, array('placeholder' => 'Nombre Campaña','class' => 'form-control')); ?>

            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inicio Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <!--<input type="date" name="bday">-->
                <?php echo Form::date('inicio', null, array('placeholder' => 'Fecha Inicio Campaña','class' => 'form-control')); ?>

            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fin Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::date('final', null, array('placeholder' => 'Fecha Fin Campaña','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imagen Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <label for="exampleInputFile"></label>
                <?php echo Form::file('imagen', null, array('placeholder' => 'Imagen','class' => 'form-control')); ?>

                <p class="help-block">Tamaño imagen px .</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Productos Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::text('productos', null, array('placeholder' => 'Productos','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-success">Actualizar</button>
        </div>
    </div>    
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>