<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Liquidador'); ?>
<?php $__env->startSection('title_panel','LIQUIDACION DE INTERESES MORATORIOS'); ?>
<?php $__env->startSection('content'); ?>
      <style type="text/css">
         th{
          background-color: #D2D1D1;
         }
         .tdcolor{
          background-color: #D2D1D1
         }
         table,td{
          border: 1 px solid;
         }
       </style>
   

      <?php if($liqview): ?>
        <table id="res" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>RESOLUCION</th>
              <th>RAZON SOCIAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo e($liquidacion[0]->antecedente_resolucion); ?></td>
              <td><?php echo e($liquidacion[0]->razonsocial); ?></td>
            </tr>
          </tbody>
        </table>

        <table id="Liq" class="table">
          <thead>
            <tr>
              <th>IMPUESTOS</th>
              <th>VENCIMIENTO LEGAL</th>  
              <th>FECHA DE PAGO</th>
            </tr>
          </thead>
          <tbody>
            <tr>    
              <td>$<?php echo e(number_format($liquidacion[0]->valor_base)); ?></td>
              <td><?php echo e($liquidacion[0]->vencimiento_legal); ?></td>
              <td><?php echo e($liquidacion[0]->fecha_pago); ?></td>
            </tr>
          </tbody>
        </table>    
        <?php endif; ?>  
       <?php if($liqview2): ?>
       <table id="rzocial" class="table table-striped table-responsive">
          <thead>
            <tr>
              <th>RAZON SOCIAL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo e($liqsinres[0]->empresa); ?></td>
              <td> <?php echo Form::hidden('encid', $liqsinres[0]->id, array('class' => 'form-control', 'id'=>'encid')); ?></td>  
            </tr>
          </tbody>
        </table>
       <?php endif; ?>

        <br/>
        <table id="detalle" class="table" >
          <thead>
            <tr>
              <th>PORCENTAJE</th>
              <th>DESDE</th>  
              <th>HASTA</th>
              <th>T.DIARIA</th>
              <th>DIAS</th>
              <th>INTERES</th>
              <th>NUEVO SALDO</th>
              <th>ACUMULA</th>
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $detalle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
           <tr>    
              <td><?php echo e($det->porcentaje_anual); ?>%</td>
              <td><?php echo e($det->fecha_inicio); ?></td>
              <td><?php echo e($det->fecha_fin); ?></td>
              <td><?php echo e($det->tasa_diaria); ?>%</td>
              <td><?php echo e($det->dias_mora); ?></td>
              <td>$<?php echo e(number_format($det->interes)); ?></td>
              <td>$<?php echo e(number_format($det->nuevo_saldo)); ?></td>  
              <td><?php echo e($det->dias_acumulados); ?></td>     
           </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
          </tbody>
        </table>
        <br/>
        <table id="resumen" class="table" >
          <thead>
            <tr>
              <th colspan="2">RESUMEN</th>     
            </tr>
          </thead>
          <tbody>
            <tr> 
             
             <?php if($liqview): ?>
              <td class="tdcolor">TOTAL IMPUESTO</td>  
              <td>$<?php echo e(number_format($liquidacion[0]->valor_base,2)); ?></td>   
             <?php endif; ?>

             <?php if($liqview2): ?>
              <td class="tdcolor">TOTAL IMPUESTO</td>  
              <td>$<?php echo e(number_format($liqsinres[0]->valor_base,2)); ?></td>   
             <?php endif; ?>
            
            </tr>
            <tr>  
              <td class="tdcolor">TOTAL INTERES</td>  
              <td>$<?php echo e(number_format($resumen[0]->t_interes)); ?></td>     
            </tr>
            <tr>  
              <td class="tdcolor">TOTAL A PAGAR</td>  
              <td>$<?php echo e(number_format($resumen[0]->t_nuevo_saldo)); ?></td>    
            </tr>
          </tbody>
        </table>
        <br/>
        <table id="cuota" class="table">
          <thead>
            <tr>
              <th>VALOR CUOTA</th>
              <th>FECHA PAGO</th>  
            </tr>
          </thead>
          <tbody>
          <?php $__currentLoopData = $cuota; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cta): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
           <tr>    
              <td>$<?php echo e(number_format($cta->valor_cuota)); ?></td>
              <td><?php echo e($cta->fecha_pago); ?></td>
           </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
          </tbody>
        </table>
        <?php if($liqview): ?> 
        <div style="text-align: center;margin-left:45%;">
          <img src="<?php echo e(asset('imagen/1477094440_pdf.png')); ?>"  onclick="window.open('../../liquidador/pdf/<?php echo e($liquidacion[0]->id); ?>')"  width="30" style="cursor:pointer" class="img-responsive" alt="responsive image" title="Generar PDF">
        <?php endif; ?>
        <?php if($liqview2): ?> 
        <div style="text-align: center;margin-left:45%;">
          <img src="<?php echo e(asset('imagen/1477094440_pdf.png')); ?>"  onclick="window.open('../../liquidador/pdf/<?php echo e($liqsinres[0]->id); ?>')"  width="30" style="cursor:pointer" class="img-responsive" alt="responsive image" title="Generar PDF">
        <?php endif; ?>

        </div>
  
<?php echo Form::close(); ?>   
<?php $__env->stopSection(); ?>



       


<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>