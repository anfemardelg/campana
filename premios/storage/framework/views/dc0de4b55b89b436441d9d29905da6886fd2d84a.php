<?php $__env->startSection('title','Editar  Procesos'); ?>
<?php $__env->startSection('title_panel','Editar  Procesos'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
    fieldset.scheduler-border {
                border: 1px groove #ddd !important;
                padding: 0 1.4em 1.4em 1.4em !important;
                margin: 0 0 1.5em 0 !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;

    }
</style>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script  type="text/javascript">
    $(document).ready(function() {
    $('#estados').DataTable( {
        "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false
    } );
} );
</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('procesos.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['procesos.update',$C_Proceso],'method'=>'PUT','enctype'=>'multipart/form-data','id'=>'formprocesoedit']); ?>

    <?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

    <?php echo Form::hidden('id',$C_Proceso->id,['class'=>'form-control','required']); ?>

    <div class="form-group">
        <?php echo Form::label('nameproceso','Nombre del Proceso :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('proceso_nombre',$C_Proceso->proceso_nombre,['class'=>'form-control','placeHolder'=>'Nombre del Proceso','autocomplete'=>'off','required']); ?>        
    </div>
    <div class="form-group">       
        <?php echo Form::label('DescripProceso','Descripción :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::textArea('proceso_descripcion',$C_Proceso->proceso_descripcion,['class'=>'form-control','placeHolder'=>'Descripción del proceso','required']); ?>

    </div>
    <div class="form-group">
        <table class="table table-striped display table-responsive">
            <tr>
                <td>
                      <?php echo Form::label('diasproces','Duración del Proceso en Días :'); ?>

                        <span style="color:red"><strong>*</strong></span>  
                </td>
                <td>
                      <?php echo Form::label('venceproces','Notifica vencimiento a # Días:'); ?>

                        <span style="color:red"><strong>*</strong></span>  
                </td>
            </tr>
            <tr>
                <td>
                     <?php echo Form::number('proceso_dias',$C_Proceso->proceso_dias,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>       
                </td>
                <td>
                     <?php echo Form::number('proceso_dias_vence',$C_Proceso->proceso_dias_vence,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>      
                </td>
            </tr>
        </table>      
    </div>

    <div class="form-group">        
        <?php echo Form::label('ProcesoPre-desesor','Proceso Predesesor :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::select('Procesoidpre',$C_ProcesoSelect,$C_Proceso->proceso_parent_id,['class'=>'form-control','placeHolder'=>'Seleccione el Proceso','required']); ?>

    </div>
<div class="form-group">        
        <?php echo Form::label('Responsable','Responsable de la Etapa :'); ?>

        <span style="color:red"><strong>*</strong></span>  
       
        <?php $__currentLoopData = $C_rol; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rol): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>   
            <?PHP 
            if($rol->id==$C_Proceso->rol_id){
                    $check_rol = 'checked="checked"';                    
            }else{
                    $check_rol= '';                    
            }
            ?>
           &nbsp;<strong><?php echo e($rol->name); ?></strong>
            <input type="radio" name="rol" id="<?php echo e($rol->name); ?>" value="<?php echo e($rol->id); ?>" <?php echo e($check_rol); ?>  />&nbsp;
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </div>
<br>    
<hr>
    <fieldset class="scheduler-border">
            <legend class="scheduler-border">Estados Asociados</legend>            
    <?php if($ver!=1): ?>        
            <table id="estados" class="table table-striped display table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Estado</th>
                        <th>Descripción</th>
                        <th>Opción</th>
                    </tr>    
                </thead>
            <?PHP $i=0; ?>    
            <?php $__currentLoopData = $C_Estado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <tr>
                        <td><?php echo e($i+1); ?></td>
                        <td><?php echo e($row->estado_nombre); ?></td>
                        <td><?php echo e($row->estado_descripcion); ?></td>
                        <td></td>
                    </tr>
            <?PHP $i++; ?>    
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </table>            
    <?php else: ?>
        <span><strong>No tiene estados asociados</strong></span>              
    <?php endif; ?>
    </fieldset>  
<br><br>
    <div class="form-group">
       	<?php echo Form::button('&nbsp;Modificar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar Modificacion','type'=>'submit','id'=>'SaveAnotacion']); ?>

      </div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>