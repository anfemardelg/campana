<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Expedientes'); ?>
<?php $__env->startSection('title_panel','Lista de Expedientes'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/reparto.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#sinExpediente').DataTable({
            dom: 'Bfrtip',
            buttons: [
                            'excel', 'pdf'
                          ],
            "language": {
                "paginate": {
                                    "previous": "Anterior",
                                    "next": "Siguiente"    
                }
            }
        });
    
    $('#ConExpediente').DataTable({
            dom: 'Bfrtip',
            buttons: [
                            'excel', 'pdf'
                          ],
            "language": {
                "paginate": {
                                    "previous": "Anterior",
                                    "next": "Siguiente"    
                }
            }
        });
    
    $('#Vencidas').DataTable({
            dom: 'Bfrtip',
            buttons: [
                            'excel', 'pdf'
                          ],
            "language": {
                "paginate": {
                                    "previous": "Anterior",
                                    "next": "Siguiente"    
                }
            }
        });
  
});
</script>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Acto Administrativo Sin Expediente</a></li>
    <li><a href="#tabs-2">Expedientes en proceso</a></li>
    <li><a href="#tabs-3">Expedientes por Vencer</a></li>
  </ul>
    <input type="hidden" id="userid" name="userid" value="<?php echo e(Auth::user()->id); ?>" />

  <div id="tabs-1">
    <?php if (\Entrust::can('expedientes-create')) : ?>
    <a href="<?php echo e(route('expedientes.create')); ?>" id="Button" class="Button">Registrar Expediente</a> 
    <?php endif; // Entrust::can ?>
      <br/>
      <br>
      <div class="table-responsive"> 
      <table id="sinExpediente" class="table" >
    <thead>
      <tr>
        <th>#</th>
        <th style="width: 25%">ORIGEN</th>
        <th>FECHA RECEPCIÓN</th>
        <th>RESOLUCIÓN</th>
        <th>FECHA RESOLUCIÓN</th>
        <th>FECHA EJECUCIÓN</th>
        <th>OFICIO</th>  
         <?php if($Rol_user==2): ?>  
        <th>ABOGADO RESPONSABLE</th>  
        <?php endif; ?>           
        <th>ACCION</th>            
      </tr>
    </thead>
    <tbody>
    <?PHP $i = 0; ?>   
     
  <?php $__currentLoopData = $antecentesinexpediente; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ante): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr>
        <td><?php echo e($i+1); ?></td>
        <td style="width: 25%"><?php echo e($ante->parametro_codigo); ?></td>
        <td><?php echo e($ante->fecha_recepcion); ?></td>
        <td><?php echo e($ante->antecedente_resolucion); ?></td>
        <td><?php echo e($ante->antecedente_fecharesol); ?></td>
        <td><?php echo e($ante->fecha_ejecucion); ?></td>
        <td><?php echo e($ante->antecedente_oficio); ?></td>
        <?php if($Rol_user==2): ?>
        <td><?php echo e($ante->usr_name); ?> <?php echo e($ante->usr_lname); ?></td>
        <?php endif; ?>
        <td> 
        <div  style="margin:0 auto; width: 100%; ">
                <?php $__currentLoopData = $ViewSinDoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rowSn): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                     <?php $__currentLoopData = $rowSn; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $viewSn): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($viewSn->valor_id == $ante->id): ?>
                             <a onclick="ListarDocumentos(<?php echo e($ante->id); ?>,'antecedenteid')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>   
                        <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
        </td>
      </tr>
        <?PHP $i++; ?>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table> 
      </div>      
  </div>
  <div id="tabs-2">
    <div id="dialog-form" title="Listado de Documentos"></div>  
    <div class="table-responsive">  
    <table id="ConExpediente"  class="table" >
    <thead>
      <tr>
        <th>#</th>
        <th>RESOLUCIÓN</th>
        <th>RADICADO</th>
        <th>FOLIO</th>
        <th>ESTADO</th>
        <th>FECHA RADICACIÓN</th>
         <?php if($Rol_user==2): ?>  
        <th>ABOGADO RESPONSABLE</th>  
        <?php endif; ?>    
        <th>ACCION</th>
      </tr>
    </thead>
    <tbody>
        <?PHP $j= 0; ?>
    <?php $__currentLoopData = $expedientes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $expediente): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
     <tr>
        <td><?php echo e($j+1); ?></td> 
        <td><?php echo e($expediente->antecedente_resolucion); ?></td>
        <td><?php echo e($expediente->expediente_rad); ?></td>
        <td><?php echo e($expediente->expediente_folio); ?></td>
        <?php $__currentLoopData = $esxp_Stado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

        <?php if($Row[0]->expedienteid==$expediente->expedienteid): ?>
        <td><?php echo e($Row[0]->estado_view); ?></td>
        
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <td><?php echo e($expediente->expediente_fecha); ?></td>
         <?php if($Rol_user==2): ?>
        <td><?php echo e($expediente->usr_name); ?> <?php echo e($expediente->usr_lname); ?></td>
        <?php endif; ?> 
        <td style="width: 25%"> 
        <?php if($expediente->antecedente_estado==18): ?>
        <div  style="margin:0 auto; width: 100%; ">
        <?php if (\Entrust::can('expedientes-edit')) : ?>
        <a href="<?php echo e(route('expedientes.edit', $expediente->id)); ?>" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
        <?php endif; // Entrust::can ?>
        <?php if (\Entrust::can('expedientes-delete')) : ?>
        <a href="<?php echo e(route('expedientes.destroy', $expediente->id)); ?>" onclick="return confirm('Desea Eliminar el expediente...!!')" class="btn btn-danger btn-xs" title="Eliminar Expediente"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
       <?php endif; // Entrust::can ?>              
         <a href="<?php echo e(route('anotaciones.listar', $expediente->id)); ?>" class="btn btn-success btn-xs" title="Adicionar o Modificar Anotaciones" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a> 
            <?php $__currentLoopData = $ViewDoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                     <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($view->valor_id == $expediente->id): ?>
                            <a onclick="ListarDocumentos(<?php echo e($expediente->id); ?>,'expedienteid')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>     
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
             
        </div>
        <?php endif; ?>
        </td>
      </tr>
        <?PHP  $j++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
  </table>
      </div>
  </div>
    <div id="tabs-3">
        <div class="table-responsive">
        <table id="Vencidas" class="table" >
            <thead>
              <tr>
                <th>#</th>  
                <th>Resolución</th>
                <th>Fecha Resolución</th>  
                <th>Fecha Ejecución</th>    
                <th>Razón Social ó Nombre</th>   
                <!--<th>Folio</th>-->
                <th>Radicado</th>
                <th>Fecha Inicio</th>                  
                <th>Etapa</th>
                <th>Fecha Limite Etapa</th>    
                <th>Actividad</th>
                <th>Fecha Limite Actividad</th>  
                <!--<th># Días de Etapa</th>               
                <th># Días de Actividad</th>-->     
                <?php if($Rol_user==2): ?>  
                <th>Abogado Responsable</th>  
                <?php endif; ?>      
              </tr>
            </thead>
            <tbody>
                <?PHP $n=0; ?>
                 <?php $__currentLoopData = $venciendo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ven): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <tr>
                    <td><?php echo e($n+1); ?></td>
                    <td><?php echo e($ven->antecedente_resolucion); ?></td>
                    <td><?php echo e($ven->antecedente_fecharesol); ?></td>
                    <td><?php echo e($ven->fecha_ejecucion); ?></td>
                    <td style="width: 25%"><?php echo e($ven->razonsocial); ?></td>
                    <!--<td><?php echo e($ven->expediente_folio); ?></td>-->
                    <td><?php echo e($ven->expediente_rad); ?></td>
                    <td><?php echo e($ven->fecha_inicial); ?></td>   
                    <?php $__currentLoopData = $C_FechasData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Row_fec): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($Row_fec[0]->expedienteid==$ven->expedienteid): ?>
                        <td><?php echo e($Row_fec[0]->proceso_nombre); ?></td>
                        <td><?php echo e($Row_fec[0]->fecha_fin_etapa); ?></td>
                        <td><?php echo e($Row_fec[0]->estado_nombre); ?></td>
                        <?php if($Row_fec[0]->fecha_inicial != $Row_fec[0]->fecha_fin_Actividad): ?>
                        <td><?php echo e($Row_fec[0]->fecha_fin_Actividad); ?></td>
                        <?php else: ?>
                        <td></td>
                        <?php endif; ?>
                        <?php endif; ?>                     
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>  
                    
                    <!--<td><?php echo e($ven->proceso_dias); ?></td>                   
                    <td><?php echo e($ven->estado_dias); ?></td>-->
                    <?php if($Rol_user==2): ?>  
                    <td><?php echo e($ven->abogado); ?></td>  
                    <?php endif; ?> 
                </tr>
                <?PHP $n++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>
        </table>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>