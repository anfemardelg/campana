<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
 Editar voluntad de pago
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Editar voluntad de pago
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script>
  $( document).ready(function() {
    $( ".datepicker2" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
           
        });

    $( ".datepicker3" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
           
        });
    
  });

 
  function ValidarValor(i){
    var valor_cuota  = parseFloat($('#valor_cuota_'+i).val());
    var valor_pagado = parseFloat($('#valor_pagado_'+i).val());

    if(valor_pagado > valor_cuota){
      alert('El valor Pago es Superior al Valor de la Cuota...!!');
      $('#valor_pagado_'+i).val('');
      return false;
    }
  }//function ValidarValor

function validarFecha(i){
  var v_fecha_ini = $('#fecha_ini_'+i).val();
  var v_fecha_pagorealizado = $('#fecha_f_'+i).val();

    var x = v_fecha_ini.split("/");
   // var z = v_fecha_pagorealizado.split("/");

    f_inicio= x[2] + "-" + x[1] + "-" + x[0];
  //  f_pago_realizado = z[2] + "-" + z[1] + "-" + z[0];



  if(Date.parse(v_fecha_pagorealizado)>Date.parse(f_inicio)){
    alert('La fecha de pago no puede ser superior a la fecha a pagar');
    $('#fecha_f_'+i).val('');

    return false;
  }else{
    return true;
  }
}


  function Validarpago(){
    var index = $('#index').val();
    var respuesta = true;
      for (i=0;i<index; i++) {
          var valor_cuota  = '';
          var valor_pagado  = '';
          valor_cuota  = parseFloat($('#valor_cuota_'+i).val());
          valor_pagado = parseFloat($('#valor_pagado_'+i).val());
        
        if(valor_pagado > valor_cuota){
          alert('El Valor Pago es Superior al Valor de la Cuota...!!');
          $('#valor_pagado_'+i).val('');
          $('#valor_pagado_'+i).effect("pulsate", {times: 5}, 1000);
          $('#valor_pagado_'+i).css('border-color', '#F00');
          respuesta = false; 
        }
    }//for
    var valor=respuesta;
    if (respuesta){
    
      valor= comparar_fecha();
    }
    return valor;
  }

   function comparar_fecha(){
     var index = $('#index').val();
     var respuesta = true;
     for (i=0;i<index; i++) {
       var v_fecha_inicio  = '';
       var fecha_fin  = '';

       var v_fecha_inicio =   $('#fecha_ini_'+i).val();
       var fecha_fin    =   $('#fecha_f_'+i).val();

       //Split de las fechas recibidas para separarlas
       var x = v_fecha_inicio.split("/");
      // var z = fecha_fin.split("/");
       //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
       fecha_inicio = x[2] + "-" + x[1] + "-" + x[0];
      // fecha_fin = z[2] + "-" + z[1] + "-" + z[0];
      
       if (Date.parse(fecha_fin) > Date.parse(fecha_inicio)){
              alert('Fecha de pago realizado no puede ser inferior a la fecha de pago !!!');
             respuesta = false; 
          }        
    }
     return respuesta;
  }  
  function VerFile(i){
    if($('#Doc_'+i).is(':checked')){
      $('#file_'+i).css('display','inline');
      $('#file_'+i).attr('required',true);
    }else{
      $('#file_'+i).css('display','none');
      $('#file_'+i).removeAttr('required');
    }
  }//function VerFile(){}
  </script>


  <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('vpago.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
<br>

<?PHP $i=0; ?>   
 <?php $__currentLoopData = $vpago; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>   
<?php echo Form::open(['route'=>['vpago.update',$row->id],'method'=>'Patch','enctype'=>'multipart/form-data']); ?>

   <div id="dialog-form" title="Listado de Documentos"></div>  
<table class="table">
    <tbody>   
      
    <tr>
        <td>
          <div class="form-group">
             <?php echo Form::label('vcuota:','Valor cuota:'); ?>     
              <?php echo Form::hidden('id_vpago[]', $row->id, array('class' => 'typeahead form-control','id' => 'id_vpago')); ?>          
              <input type="text" name="valor_cuota[]" id="valor_cuota_<?PHP echo $i?>" value="<?php echo e($row->valor_cuota); ?>" disabled="disabled"  />
          </div>
        </td>
        <td>    
          <div class="form-group">
             <?php echo Form::label('fapagar:','Fecha a pagar:'); ?>

              <input type="text" name="fecha_pago[]" id="fecha_ini_<?PHP echo $i?>" value="<?php echo e($row->fecha_pago); ?>" disabled ="disabled" /> 
          </div>
        </td>
        <td>
          <div class="form-group">
               <?php echo Form::label('fapagor:','Fecha de pago:'); ?>

              <input type="text" class="form-control datepicker2" name="fecha_pagorealizado[]" id="fecha_f_<?PHP echo $i; ?>" placeholder="fecha pago realizado" value="<?php echo e($row->fecha_pago_realizado); ?>" onblur="validarFecha(<?PHP echo $i; ?>)"  />
          </div>
        </td>  
        <td>
          <div class="form-group">
              <?php echo Form::label('vapago:','Valor pagado:'); ?>     
              <input type="text" class="form-control" onkeypress="return validarNumeros(event)" name="valor_pagado[]" id="valor_pagado_<?PHP echo $i?>" value="<?php echo e($row->valor_pagado); ?>"  onblur="ValidarValor(<?PHP echo $i; ?>)"  />
          </div>    
        </td>
        <td>
          <div class="form-group">

                  <?php echo Form::label('anotaciones_files','Adjuntar Documento:'); ?>

                  <input type="checkbox" name="adjunto[]" id="Doc_<?PHP echo $i; ?>" onchange="VerFile(<?PHP echo $i?>)" value="<?PHP echo $i?>"> 
                  <input type="file" id="file_<?PHP echo $i ?>" style="display: none" name="file_primary[]"  multiple>
                  <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

                  
         </div>
        </td>
        <td> 
          <div  style="margin:0 auto; width: 100%; ">  
            <?php if($v): ?>  

             <?php $__currentLoopData = $viewvpago; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
         
              <?php if($view[0]->valor_id == $row->id): ?>
                     <a onclick="ListarDocumentos(<?php echo e($row->id); ?>,'liquidacion_cuotasid','../../')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>   
              <?php endif; ?> 
           
             <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endif; ?> 
          </div>
        </td>
    </tr>
    <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>         


<input type="hidden" name="index" id="index" value="<?PHP echo $i?>">
 <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		<button type="submit" class="btn" onclick="return Validarpago(); comparar_fecha()">Aceptar</button>        
 </div>
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>	
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>