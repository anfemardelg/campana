<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Importar Data'); ?>
<?php $__env->startSection('title_panel','Importar Data'); ?>
<?php $__env->startSection('content'); ?>

<?php echo Form::open(['action' => 'ImportController@import','enctype'=>'multipart/form-data']); ?>


    <div class="form-group">
        <label for="exampleInputFile"></label>
        <input type="file" name="FileData" id="FileData" />
        <p class="help-block">Archivos Permitidos  xls , xlsx , txt .</p>
    </div>
    <br><br>
    <input class="btn btn-success" type="submit" onClick="disabled='disabled'" name="EnviarData" id="EnviarData" value="Cargar Data" >                
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>