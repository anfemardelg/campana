<?php $__env->startSection('tittle','Anotaciones del Expediente'); ?>
<?php $__env->startSection('title_panel','Anotaciones del Expediente'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/reparto.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/anotaciones.js')); ?>"></script>

<?php echo Form::open(['route'=>['anotaciones.update',$C_anotacion],'method'=>'PUT','enctype'=>'multipart/form-data','id'=>'formAntecedenteedit']); ?>

<div class="form-group">
    <?php echo Form::label('oficio','Oficio:'); ?>

    <?php echo Form::label('oficio',$C_antecedente->antecedente_oficio,['class'=>'']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('folio','Folio:'); ?>

    <?php echo Form::label('folio',$C_expediente->expediente_folio,['class'=>'']); ?>

    <?php echo Form::hidden('expedienteid',$C_expediente->id,['class'=>'','id'=>'expedienteid']); ?>

    <?php echo Form::hidden('anotacionid',$C_anotacion->id,['class'=>'','id'=>'anotacionid']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('Radicado','Radicado:'); ?>

    <?php echo Form::label('Radicado',$C_expediente->expediente_rad,['class'=>'']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('fechaRadicado','Fecha Radicado:'); ?>

    <?php echo Form::label('fechaRadicado',$C_expediente->expediente_fecha,['class'=>'']); ?>

</div>
<div class="form-group">
    <?php echo Form::label('Procesoid','Proceso:'); ?>

    <?php echo Form::select('Procesoid',$C_proceso,$C_anotacion->procesoid,['class'=>'form-control','placeHolder'=>'Seleccione el Proceso','required','id'=>'Procesoid']); ?>

</div>
  <div class="form-group">

    <?php echo Form::label('Estadoid','Estado:'); ?>

    <div id="DivSelect">
    <?php echo Form::select('Estadoid',$C_estado,$C_anotacion->estadoid,['class'=>'form-control','placeHolder'=>'Seleccione el Estado','required','id'=>'Estadoid']); ?>

    </div>
  </div>

  <div class="form-group">
  	<?php echo Form::label('anotaciones_descripcion','Anotaciones:'); ?>

  	<?php echo Form::textArea('anotacion_anotaciones',$C_anotacion->anotacion_anotaciones,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

  </div>
<div class="form-group">
            <?php echo Form::label('anotaciones_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
            <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx'); ?>

   </div>

   <div class="form-group">
  	<?php echo Form::button('&nbsp;Modificar',['class'=>'btn btn-primary glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar Modificacion','type'=>'submit','id'=>'SaveAnotacion']); ?>

  </div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>