<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
Administración de Product
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
    Product
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
    <br>
    <?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
<div>    
    <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('api.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
    <br>
    <style type="text/css">
    .card text-white bg-primary mb-3 {
        /* Add shadows to create the "card" effect */
        border: 1px; !important;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
    </style>
    
    <?php echo Form::open(array('route' => 'Products.carrito','method'=>'POST')); ?>

        <?php $__currentLoopData = $modelproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $modelproduct): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <img name="images[]" id="images[]" value="<?php echo e($modelproduct->image); ?>" src="<?php echo e($modelproduct->image); ?>" width="180px" height="100px">
            <div class="card-body text-secondary">
                <h5 type="text" name="names[]" id="names[]" value="<?php echo e($modelproduct->name); ?>" class="card-title" ><?php echo e($modelproduct->name); ?></h5>
                <div class="overflow-hidden">
                    <p class="card-text" name="descripcions[]" id="descripcions[]" value="<?php echo e($modelproduct->description); ?>" ><?php echo e($modelproduct->description); ?></p>
                </div> 
                <input type="checkbox" name="productos[]" value="<?php echo e($modelproduct->product_id); ?>,<?php echo e($modelproduct->image); ?>,<?php echo e($modelproduct->name); ?>,<?php echo e($modelproduct->description); ?>" class="custom-control-input">
            </div>
        </div>
        <br>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Registrar</button>
        </div>
    </div>
    <?php echo Form::close(); ?>    
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>