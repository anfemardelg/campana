<?php $__env->startSection('tittle','Creacion de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Creacion de Antecedentes'); ?>
<?php $__env->startSection('content'); ?>

<?php echo Form::open(['action' => 'AntecedentesController@store','enctype'=>'multipart/form-data']); ?>

    <div class="form-group">
        <?php echo Form::label('antecedente_oficio','Oficio:'); ?>

        <?php echo Form::text('antecedente_oficio',null,['class'=>'form-control','placeHolder'=>'Oficio','autocomplete'=>'off','required']); ?>

    </div>
    <div class="form-group">
            <?php echo Form::label('antecedente_descripcion','Anotaciones:'); ?>

            <?php echo Form::textarea('antecedente_descripcion',null,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

    </div>
  <div class="form-group">
  	<?php echo Form::label('antecedente_codigo','Expediente:'); ?>

  	<?php echo Form::text('antecedente_codigo',null,['class'=>'form-control','placeHolder'=>'Expediente','autocomplete'=>'off','required']); ?>

  </div>
   <div class="form-group">
    <?php echo Form::label('antecedente_nombre','Nombre Sancionado:'); ?>

    <?php echo Form::text('antecedente_nombre',null,['class'=>'form-control','placeHolder'=>'Nombre','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_apellido','Apellido Sancionado:'); ?>

    <?php echo Form::text('antecedente_apellido',null,['class'=>'form-control','placeHolder'=>'Apellido','autocomplete'=>'off']); ?>

  </div>


   <div class="form-group">
    <?php echo Form::label('Tipo Documento:','Tipo Documento:'); ?>

    <?php echo Form::select('tipo_doc_id', ['1' => 'Cedula', '2' => 'Nit', '3' => 'Cedula de Extranjeria',  '4' => 'Pasaporte'], null, ['class'=>'form-control','placeholder' => 'Seleccione el tipo...']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_nro_documento','Numero Documento:'); ?>

    <?php echo Form::text('antecedente_nro_documento',null,['class'=>'form-control','placeHolder'=>'Nro Documento','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_telefono','Telefono:'); ?>

    <?php echo Form::text('antecedente_telefono',null,['class'=>'form-control','placeHolder'=>'111-2222','autocomplete'=>'off']); ?>

  </div>

<div class="form-group">
    <?php echo Form::label('antecedente_email','Correo Electronico:'); ?>

    <?php echo Form::email('antecedente_email',null,['class'=>'form-control','placeHolder'=>'aaa@yyyy.com','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_direccion1','Dirección Principal:'); ?>

    <?php echo Form::text('antecedente_direccion1',null,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>

  </div>
  <div class="form-group">
    <?php echo Form::label('antecedente_direccion2','Dirección Opcional:'); ?>

    <?php echo Form::text('antecedente_direccion2',null,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
  	<?php echo Form::label('antecedente_sancion','Sanción:'); ?>

  	<?php echo Form::text('antecedente_sancion',null,['class'=>'form-control','placeHolder'=>'Sanción','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_concepto','Concepto:'); ?>

    <?php echo Form::textarea('antecedente_concepto',null,['class'=>'form-control','placeHolder'=>'Concepto','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_resolucion','Resolución:'); ?>

    <?php echo Form::text('antecedente_resolucion',null,['class'=>'form-control','placeHolder'=>'Resolución','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_fecharesol','Fecha Resolución:'); ?>

    <?php echo Form::text('antecedente_fecharesol',null,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'datepicker','readonly'=>'readonly','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
   <?php echo Form::label('userid','Nombre Abogado:'); ?>

   <?php echo Form::select('userid',$abogado, null, ['class'=>'form-control','placeholder' => 'Seleccione Abogado...'] );; ?>

  </div>
   <div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
            <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx'); ?>

   </div>
   <div class="form-group">
  	<?php echo Form::button('&nbsp;Registrar',['class'=>'btn btn-primary glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit']); ?>

  </div>
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>