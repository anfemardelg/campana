<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Actividades'); ?>
<?php $__env->startSection('title_panel','Lista de Actividades'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#data_Estados').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<a href="<?php echo e(route('estados.create')); ?>" id="Button" class="Button ">Registrar Actividades</a>
<br><br><br><br>

<table id="data_Estados" class="table table-striped display table-responsive"  cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Actividad</th>
            <th>Descripción</th>            
            <th>Etapa Asociada</th>    
            <th>Opcion</th>
        </tr>
    </thead>
    <tbody>
        <?PHP $i=0; ?>
        <?php $__currentLoopData = $C_Estados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_estado): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        
            <tr>
                <td><?php echo e($i+1); ?></td>
                <td><?php echo e($row_estado->estado_nombre); ?></td>
                <td><?php echo e($row_estado->estado_descripcion); ?></td>   
                <td><?php echo e($row_estado->proceso_nombre); ?></td>    
                <td>
                    <?php if (\Entrust::can('proceso-edit')) : ?>
                     <a href="<?php echo e(route('estados.edit', $row_estado->id)); ?>" class="btn btn-warning btn-xs" title="Editar Actividad"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>    
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('proceso-delete')) : ?>
                     <a href="<?php echo e(route('estados.destroy', $row_estado->id)); ?>" onclick="return confirm('Desea Eliminar la Actividad...!!')" class="btn btn-danger btn-xs" title="Eliminar Actividad"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                    <?php endif; // Entrust::can ?>
                </td>
            </tr>
        <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>