<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                <div>
                 <img src="<?php echo e(asset('imagen/LogoSloganMinCIT.png')); ?>"  width="200" style="margin-top: -3%">
                </div>
            </a>
        </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->

            <ul class="nav navbar-nav navbar-left">
              <?php if (\Entrust::can('admin-show')) : ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('users.index')); ?>">Usuarios</a></li>
                    <li><a href="<?php echo e(route('roles.index')); ?>">Roles</a></li>
                  </ul>
                </li>
               <?php endif; // Entrust::can ?>            
              <?php if (\Entrust::can('antecedentes-show')) : ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acto Administrativo<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('antecedentes.index')); ?>">Acto Administrativo</a></li>
                  </ul>
                </li>                        
              <?php endif; // Entrust::can ?>
              <?php if (\Entrust::can('expedientes-show')) : ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Expedientes<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('expedientes.index')); ?>">Expediente</a></li>
                  </ul>
                </li>
              <?php endif; // Entrust::can ?> 
               <?php if (\Entrust::can('proceso-show')) : ?>    
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Etapas y Actividades<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('procesos.index')); ?>">Etapas</a></li>
                    <li><a href="<?php echo e(route('estados.index')); ?>">Actividades</a></li>  
                  </ul>
                </li>           
                <?php endif; // Entrust::can ?> 
                <?php if (\Entrust::can('titulo-show')) : ?>    
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Título Valor<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('titulovalor.index')); ?>">Título Valor</a></li>                    
                  </ul>
                </li>  
                 <!--salarios-->
                  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Salario Minimo<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('salario.index')); ?>">Salario Minimo</a></li>                    
                  </ul>
                </li>
                 <!--end salarios-->         
                <?php endif; // Entrust::can ?>   
                <?php if (\Entrust::can('parametro-show')) : ?>    
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Parametros del Sistema<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('parametro.index')); ?>">Parametros del Sistema</a></li>                    
                  </ul>
                </li>           
                <?php endif; // Entrust::can ?> 
                <?php if (\Entrust::can('reporte-show')) : ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <?php if (\Entrust::can('reporte-titulovalor')) : ?>
                    <li><a href="<?php echo e(route('reporte.viewtitulovalor')); ?>">Reporte Título Valor</a></li> 
                    <?php endif; // Entrust::can ?>                   
                    <?php if (\Entrust::can('reporte-expedientes')) : ?>
                    <li><a href="<?php echo e(route('reporte.viewexpediente')); ?>">Reporte Expediente</a></li> 
                    <?php endif; // Entrust::can ?>
                  </ul>
                </li>  
                <?php endif; // Entrust::can ?>   
                <?php if (\Entrust::can('liquidacion-show')) : ?>    
                 <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Liquidacion<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('liquidador.index')); ?>">Simulador Liquidacion</a></li>
                    <li><a href="<?php echo e(route('tasas.index')); ?>">Actualizar Tasas</a></li>
                  </ul>
                </li>           
                <?php endif; // Entrust::can ?>
                

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                <?php if(Auth::guest()): ?>
                    <li><a href="<?php echo e(url('/login')); ?>">Ingresar</a></li>
                    <!-- <li><a href="<?php echo e(url('/register')); ?>">Registrarse</a></li> -->
                <?php else: ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo e(Auth::user()->username); ?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?php echo e(url('/logout')); ?>"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
