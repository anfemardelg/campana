<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Redenciones'); ?>
<?php $__env->startSection('title_panel','Redenciones'); ?>
<?php $__env->startSection('content'); ?>


<?php echo Form::open(['method'=>'POST', 'action' => 'ReportesController@redenciones']); ?>

<div class="form-group">
	<?php echo Form::label('fechaini', 'Fecha Inicial', ['class'=>'col-md-2 control-label']); ?>

	<div class="col-md-3">
		<?php echo Form::text('fechaini','',['id'=>'fechaini','class'=>'form-control', 'readonly']); ?>

	</div>
</div>
<div class="form-group">
	<?php echo Form::label('fechafin', 'Fecha Final', ['class'=>'col-md-2 control-label']); ?>

	<div class="col-md-3">
		<?php echo Form::text('fechafin','',['id'=>'fechafin','class'=>'form-control', 'readonly']); ?>

	</div>
</div>
<div class="form-group">
	<div class="col-md-2">
		<?php echo Form::submit('Reporte', ['id'=>'reporte','class'=>'btn btn-primary']); ?>

	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Redenciones
		</legend>
		<table id="Redenciones" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Banco</th>
				<th>Planilla</th>
				<th>Avia</th>
				<th>Fecha envio</th>       
				<th>Mes</th>
				<th>Año</th>       
				<th>Fecha de redención</th>
				<th>Cedula</th>       
				<th>Nombres</th>      
				<th>Dirección</th>
				<th>Ciudad</th>
				<th>Depto</th>
                <th>Email</th>
                <th>Celular</th>
				<th>Telefono</th>
				<th>Codigo</th>
				<th>Nombre Premio</th>
				<th>Descripción</th>
				<th>Cod Proveedor</th>
				<th>Nombre Provedor</th>
				<th>Cantidad</th>
				<th>Valor</th>
                <th>Tipo</th> 
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $redenciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $redencion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($redencion->banco); ?></td>
					<td><?php echo e($redencion->planilla); ?></td>
					<td><?php echo e($redencion->avia); ?></td>
					<td><?php echo e($redencion->fecha_envio); ?></td>
					<td><?php echo e($redencion->mes); ?></td>
					<td><?php echo e($redencion->ano); ?></td>
					<td><?php echo e($redencion->fecha_redencion); ?></td>
					<td><?php echo e($redencion->document); ?></td>
					<td><?php echo e($redencion->nombres); ?></td>
					<td><?php echo e($redencion->direccion); ?></td>
					<td><?php echo e($redencion->ciudad); ?></td>
					<td><?php echo e($redencion->depto); ?></td>
                    <td><?php echo e($redencion->email); ?></td>
                    <td><?php echo e($redencion->celular); ?></td>
					<td><?php echo e($redencion->telefono); ?></td>
					<td><?php echo e($redencion->codigo); ?></td>
					<td><?php echo e($redencion->nombre_premio); ?></td>
					<td><?php echo e($redencion->descripcion); ?></td>
					<td><?php echo e($redencion->cod_proveedor); ?></td>
					<td><?php echo e($redencion->nombre_proveedor); ?></td>
					<td><?php echo e($redencion->cantidad); ?></td>
					<td><?php echo e($redencion->valor); ?></td>
                    <td><?php echo e($redencion->tipo); ?></td>
				</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>