<?php $__env->startSection('tittle','Apertura Proceso'); ?>
<?php $__env->startSection('title_panel','Apertura Proceso'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script>
  $( function() {
    

     $( "#fecharesol" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            maxDate:new Date(),
        });
      
  } );
  </script>
<?php echo Form::open(['route'=>['expedientes.store'],'method'=>'POST','enctype'=>'multipart/form-data']); ?>


<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<div class="form-group">
    <?php echo Form::label('antecedenteid','Resolución Acto Administrativo:'); ?>

     <span style="color:red"><strong>*</strong></span>     
    <?php echo Form::select('antecedenteid',$oficios,null,['class'=>'form-control','placeHolder'=>'Seleccione el Antecedente','required']); ?>

</div>


  <div class="form-group">
    <?php echo Form::label('expediente_rad','Radicado:'); ?>

    <span style="color:red"><strong>*</strong></span>     
    <?php echo Form::text('expediente_rad',null,['class'=>'form-control','placeHolder'=>'Radicado','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)']); ?>

  </div>



  <div class="form-group">
    <?php echo Form::label('expediente_folio','Folio:'); ?>

     <span style="color:red"><strong>*</strong></span>       
    <?php echo Form::text('expediente_folio',null,['class'=>'form-control','placeHolder'=>'Folios','autocomplete'=>'off','required']); ?>

  </div>


  <div class="form-group">
  	<?php echo Form::label('expediente_descripcion','Anotaciones:'); ?>

     <span style="color:red"><strong>*</strong></span>       
  	<?php echo Form::textArea('expediente_descripcion',null,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('expediente_fecha','Fecha Radicación:'); ?>

     <span style="color:red"><strong>*</strong></span>       
    <?php echo Form::text('expediente_fecha',null,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required','autocomplete'=>'off','onkeypress'=>'return validarFechas(event)', 'id'=>'fecharesol']); ?>

  </div>

<div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
           <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

   </div>

   <div class="form-group">
  	<?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit']); ?>

  </div>



<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>