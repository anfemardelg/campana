<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Parametros del Sistema'); ?>
<?php $__env->startSection('title_panel','Parametros del Sistema'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/parametro.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#Parametros').DataTable({
            dom: 'Bfrtip',
            buttons: [
                    'excel', 'pdf'
                  ],
            "language": {
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"    
            }
            }
        });
});
</script>
<a onclick="NewParametro()" id="Button" title="Registrar Parametro" class="Button">Registrar Parametro</a>
<br/>
<br/>
<input type="hidden" id="userid" name="userid" value="<?php echo e(Auth::user()->id); ?>" /> 
<div class="table-responsive"> 
    <table id="Parametros" class="table" >
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>Parametro Dominio</th>
    			<th>Parametro Codigo</th>
    			<th>Descripcion</th>
    			<th>Estado</th>
    			<th></th>
    		</tr>
    	</thead>
    	<tbody>
    	<?PHP $i = 0;?>
    		<?php $__currentLoopData = $C_parametro; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parametro): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    			<tr>
    				<td><?php echo e($i+1); ?></td>
    				<td><?php echo e($parametro->parametro_dominio); ?></td>
    				<td><?php echo e($parametro->parametro_codigo); ?></td>
    				<td><?php echo e($parametro->parametro_descripcion); ?></td>
    				<td><?php echo e($parametro->parametro_estado); ?></td>
    				<td>
    					<?php if (\Entrust::can('parametro-edit')) : ?>
                        <a onclick="EditParametro(<?php echo e($parametro->id); ?>)" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
                        <?php endif; // Entrust::can ?>   

                        <?php if (\Entrust::can('parametro-delete')) : ?>
                        <a onclick="DeleteParametro(<?php echo e($parametro->id); ?>)"  class="btn btn-danger btn-xs" title="Eliminar Parametro"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                        <?php endif; // Entrust::can ?>    					
    				</td>
    			</tr>
    			<?PHP $i++;?>
    		<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    	</tbody>
    </table>
</div>   
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>