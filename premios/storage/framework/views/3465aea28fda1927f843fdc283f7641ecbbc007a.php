<?php $__env->startSection('title','Editar  Salarios'); ?>
<?php $__env->startSection('title_panel','Editar  Salarios'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
    fieldset.scheduler-border {
                border: 1px groove #ddd !important;
                padding: 0 1.4em 1.4em 1.4em !important;
                margin: 0 0 1.5em 0 !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;

    }
</style>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script  type="text/javascript">
    $(document).ready(function() {
    $('#estados').DataTable( {
        "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false
    } );
} );
</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('salario.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>


<?php echo Form::open(['route'=>['salario.update',$salario],'method'=>'PUT','enctype'=>'multipart/form-data','id'=>'formsalarioedit']); ?>


 <?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

 
    <div class="form-group">
        <?php echo Form::label('anosalario','Año :'); ?>

        <span style="color:red"><strong>*</strong></span>  
         <?php echo Form::select('salario_ano',$anios_lista,$salario->salario_ano,['class'=>'form-control','placeholder'=>'Seleccione el Año','required', 'disabled']); ?>

    </div>   

    <!--<div class="form-group">       
        <?php echo Form::label('salario_auxmensual','Auxilio Transporte Monto Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_auxmensual',$salario->salario_auxmensual,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

    <<div class="form-group">       
        <?php echo Form::label('salario_auxvarianual','Auxilio Transporte Variación Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
         <?php echo Form::text('salario_auxvarianual',$salario->salario_auxvarianual,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>-->

     <div class="form-group">       
        <?php echo Form::label('salario_montodiario','Salario Monto Diario :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_montodiario',$salario->salario_montodiario,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'9999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

    <div class="form-group">       
        <?php echo Form::label('salario_montomensual','Salario Monto Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_montomensual',$salario->salario_montomensual,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

     <div class="form-group">       
        <?php echo Form::label('salario_varianual','Salario Variación Anual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_varianual',$salario->salario_varianual,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

    <div class="form-group">
       	<?php echo Form::button('&nbsp;Modificar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar Modificacion','type'=>'submit','id'=>'SaveSalario']); ?>

      </div>
    </div>   
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>