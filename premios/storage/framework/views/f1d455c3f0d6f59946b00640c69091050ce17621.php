<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte de usuarios que actualizaron datos'); ?>
<?php $__env->startSection('title_panel','Reporte'); ?>
<?php $__env->startSection('content'); ?>

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte de usuarios actualizados
		</legend>
		<table id="Actualiza" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Cedula</th>
				<th>Usuario</th>
                <th>Email</th>
                <th>Autorización 1</th>
                <th>Autorización 2</th>
                <th>Tipo</th>
                <th>Agregado</th>
                <th>Actualizado</th>
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $actualiza; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usuario): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($usuario->cedula); ?></td>
					<td><?php echo e($usuario->usuario); ?></td>
                    <td><?php echo e($usuario->email); ?></td>
                    <td><?php echo e($usuario->autorizacion1); ?></td>
                    <td><?php echo e($usuario->autorizacion2); ?></td>
                    <td><?php echo e($usuario->tipo); ?></td>
                    <td><?php echo e($usuario->agregado); ?></td>
                    <td><?php echo e($usuario->actualizado); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>