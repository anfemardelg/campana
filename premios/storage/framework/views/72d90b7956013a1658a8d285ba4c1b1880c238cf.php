<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Lista de Salarios'); ?>
<?php $__env->startSection('title_panel','Lista de Salarios'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#data_opciones').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
   
});
</script>
<a href="<?php echo e(route('salario.create')); ?>" id="Button" class="Button ">Registrar Salario</a>
<br><br><br><br>
<div id="dialog-form" title="Creación Salario"></div>
<table id="data_opciones" class="table table-striped display table-responsive"  cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Año</th>
            <th>Auxilio Monto Mensual</th>
            <th>Auxilio Variación Mensual</th>
            <th>Salario Monto Diario</th>
            <th>Salario Monto Mensual</th>
            <th>Variacion Anual</th>
            <th>Opcion</th>
        </tr>
    </thead>
    <tbody>

        <?PHP $i=0; ?>
        <?php $__currentLoopData = $salarios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $salario): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($i+1); ?></td>
                <td><?php echo e($salario->salario_ano); ?></td>
                <td><?php echo e($salario->salario_auxmensual); ?></td>
                <td><?php echo e($salario->salario_auxvarianual); ?>%</td>
                <td><?php echo e($salario->salario_montodiario); ?></td>
                <td><?php echo e($salario->salario_montomensual); ?></td>
                <td><?php echo e($salario->salario_varianual); ?>%</td>
                <td>
                    <!--<?php if (\Entrust::can('proceso-edit')) : ?>-->
                     <a href="<?php echo e(route('salario.edit', $salario->id)); ?>" class="btn btn-warning btn-xs" title="Editar Salario"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a> 
                    <!--<?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('proceso-delete')) : ?>-->
                     
                     <a href="<?php echo e(route('salario.destroy', $salario->id)); ?>"  onclick="return confirm('Desea Eliminar el Salario...!!')"  class="btn btn-danger btn-xs" title="Eliminar Salario"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                    


                    <!--<?php endif; // Entrust::can ?>-->
                </td>
            </tr>
        <?PHP $i++; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>