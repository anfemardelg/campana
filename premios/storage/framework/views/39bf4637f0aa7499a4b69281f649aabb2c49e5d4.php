<script type="text/javascript">
$(document).ready(function(){
    $('#ReporteExpediente').DataTable({
            dom: 'Bfrtip',
            buttons: [
                    'excel', 'pdf'
                  ],
            "language": {
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"    
            }
            }
        });
});
</script>
<div class="table-responsive"> 
    <table id="ReporteExpediente" class="table" >
        <thead>
            <tr>
                <th>N&deg Radicado</th>
                <th>Razón Social ó Nombre</th>
                <th>Tipo Documento</th>
                <th>N&deg Documento</th>                
                <th>Tipo Liquidación</th>
                <th>Cantidad</th>
                <th>Año</th>
                <th>SMMLV</th>
                <th>$ Valor</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $Result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($row->expediente_rad); ?></td>
                <td><?php echo e($row->RazonSocial); ?></td>
                <td><?php echo e($row->parametro_codigo); ?></td>
                <td><?php echo e($row->antecedente_nro_documento); ?></td>
                <td><?php echo e($row->Categoria); ?></td>
                <?php if($row->categoria_valor==1): ?>
                <td><?php echo e($row->antecedente_sancion); ?></td>
                <td><?php echo e($row->salario_ano); ?></td>
                <td><?php echo e(number_format($row->salario_montomensual,2)); ?></td>
                <?PHP $valor = ($row->antecedente_sancion*$row->salario_montomensual);?>
                <td><?php echo e(number_format($valor,2)); ?></td>
                <?php else: ?>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo e(number_format($row->antecedente_sancion,2)); ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
</div>    