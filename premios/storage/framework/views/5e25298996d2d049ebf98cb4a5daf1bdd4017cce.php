<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $__env->yieldContent('tittle','default'); ?>| Cobro Coactivo</title>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('plugins/bootstrap/css/bootstrap.css')); ?>">
        
        <script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.js')); ?>"></script>
       
    </head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <br><br>
                <div class="panel panel-default">
                   <div class="panel-heading">
                        <!--<strong>Listar Documentos</strong>-->
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <input type="hidden" id="userid" name="userid" value="<?php echo e(Auth::user()->id); ?>" />
                                <table  class="table table-striped table-responsive">
                                    <tbody>
                                        <tr>
                                            <td>Proceso:</td>
                                            <td><?php echo e($C_Proceso->proceso_nombre); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Descripción:</td>
                                            <td><?php echo e($C_Proceso->proceso_descripcion); ?></td>
                                        </tr>
                                    </tbody>
                                </table>    
                            <fieldset>
                                <legend>Estados</legend>
                                <table  class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Estado</th>
                                            <th>Descripción</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                             <?php $__currentLoopData = $C_Estados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <tr>
                                                       <td></td>
                                                       <td><?php echo e($rowother[0]->estado_nombre); ?></td>
                                                       <td><?php echo e($rowother[0]->estado_descripcion); ?></td>
                                                       <td></td>
                                                   </tr>
                                               <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </tbody>
                                </table>    
                            </fieldset>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</body>
</html>    
 
