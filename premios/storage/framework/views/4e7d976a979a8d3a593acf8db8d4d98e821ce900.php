<?php $__env->startSection('tittle','Apertura Proceso'); ?>
<?php $__env->startSection('title_panel','Apertura Proceso'); ?>
<?php $__env->startSection('content'); ?>

<?php echo Form::open(['route'=>['expedientes.store'],'method'=>'POST','enctype'=>'multipart/form-data']); ?>

<div class="form-group">
    <?php echo Form::label('antecedenteid','Codigo Antecedente:'); ?>

    <?php echo Form::select('antecedenteid',$oficios,null,['class'=>'form-control','placeHolder'=>'Seleccione el Antecedente','required']); ?>

</div>


  <div class="form-group">
    <?php echo Form::label('expediente_rad','Radicado:'); ?>

    <?php echo Form::text('expediente_rad',null,['class'=>'form-control','placeHolder'=>'Radicado','autocomplete'=>'off','required']); ?>

  </div>



  <div class="form-group">
    <?php echo Form::label('expediente_folio','Folio:'); ?>

    <?php echo Form::text('expediente_folio',null,['class'=>'form-control','placeHolder'=>'Folios','autocomplete'=>'off','required']); ?>

  </div>


  <div class="form-group">
  	<?php echo Form::label('expediente_descripcion','Anotaciones:'); ?>

  	<?php echo Form::textArea('expediente_descripcion',null,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('expediente_fecha','Fecha Radicación:'); ?>

    <?php echo Form::text('expediente_fecha',null,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required','autocomplete'=>'off','readonly'=>'readonly', 'id'=>'datepicker']); ?>

  </div>

<div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
           <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx'); ?>

   </div>

   <div class="form-group">
  	<?php echo Form::button('&nbsp;Registrar',['class'=>'btn btn-primary glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit']); ?>

  </div>



<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>