<?php $__env->startSection('title','Edición de Antecedentes'); ?>
<?php $__env->startSection('title_panel','Edición de Antecedentes'); ?>
<?php $__env->startSection('content'); ?>

<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script>
  $( function() {
    $( "#fechar_ejecucion" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true
        });
      $( "#fecha_recepcion" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true
        });
      
  } );
  </script>
<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a  id="Button"  href="<?php echo e(route('antecedentes.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
 <?php echo Form::open(['route'=>['detalles.update',$antecedente],'method'=>'PUT','enctype'=>'multipart/form-data']); ?> 
<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<div class="form-group">
    <?php echo Form::hidden('antecedente_id',$antecedente->id); ?>

  </div>
  <div class="form-group">
    <?php echo Form::hidden('antecendente_id',$antecedente->antecendente_id); ?>

  </div>
<div class="form-group">
    <?php echo Form::label('Origen:','Origen:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::select('origen', $TipoOrigen, $antecedente->origen , ['class'=>'form-control','placeholder' => 'Seleccione el Origen...']); ?>

    </div>
     <div class="form-group">
    <?php echo Form::label('fecha_recepcion','Fecha de Recepción:'); ?>

      <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::text('fecha_recepcion',$antecedente->fecha_recepcion,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'fecha_recepcion','readonly'=>'readonly','autocomplete'=>'off']); ?>

  </div>
    <div class="form-group">
    <?php echo Form::label('antecedente_resolucion','Resolución:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('antecedente_resolucion',$antecedente->antecedente_resolucion,['class'=>'form-control','placeHolder'=>'Resolución','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_fecharesol','Fecha Resolución:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('antecedente_fecharesol',$antecedente->antecedente_fecharesol,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'datepicker','readonly'=>'readonly','autocomplete'=>'off']); ?>

  </div>
 <div class="form-group">
    <?php echo Form::label('fecha_ejecucion','Fecha Ejecución:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('fechar_ejecucion',$antecedente->fecha_ejecucion,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'fechar_ejecucion','readonly'=>'readonly','autocomplete'=>'off']); ?>

  </div>
    <div class="form-group">
        <?php echo Form::label('antecedente_oficio','Oficio:'); ?>

        <span style="color:red"><strong>*</strong></span>
        <?php echo Form::text('antecedente_oficio',$antecedente->antecedente_oficio,['class'=>'form-control','placeHolder'=>'Oficio','autocomplete'=>'off','required']); ?>

    </div>
<?PHP 
  if($antecedente->tipo_doc_id==4 || $antecedente->tipo_doc_id==5){
    $function = 'return validarNumerosDocumento(event)';
  }else{
    $function = '';
  }
?>
<div class="form-group">
    <?php echo Form::label('Tipo Documento:','Tipo Documento:'); ?>

    <span style="color:red"><strong>*</strong></span>   
    <?php echo Form::select('tipo_doc_id', $TipoDocumento, $antecedente->tipo_doc_id, ['class'=>'form-control','placeholder' => 'Seleccione el tipo...','onchange'=>'TipoDoc(this.value)']); ?>

  </div>

  <div class="form-group  ">
    <?php echo Form::label('antecedente_nro_documento','Número Documento:'); ?>

      <span style="color:red"><strong>*</strong></span>  
      <table style="width:100%">
          <tbody>
              <tr>
                  <td>
                       <input type="text" name="antecedente_nro_documento" value="<?php echo e($antecedente->antecedente_nro_documento); ?>" placeholder="Nro Documento" autocomplete="off" id="antecedente_nro_documento" class="form-control" onblur="CalcularDv()" onkeypress="<?php echo e($function); ?>"  />
                  </td>
                  <td>
                      <div class="col-xs-2">
                       <?php echo Form::text('digitovalidacion',  $antecedente->codigovalidacion,['class'=>'form-control','autocomplete'=>'off','id'=>'digitovalidacion','readonly'=>'readonly','style'=>'text-align:center']); ?>  
                       </div> 
                  </td>
              </tr>
          </tbody>
      </table>
  </div>

<div class="form-group">
    <?php echo Form::label('antecedente_nombre','Nombre ó Razón Social:'); ?>

       <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('antecedente_nombre',$antecedente->antecedente_nombre,['class'=>'form-control','placeHolder'=>'Nombre','autocomplete'=>'off','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_apellido','Apellidos:'); ?>

    <?php echo Form::text('antecedente_apellido',$antecedente->antecedente_apellido,['class'=>'form-control','placeHolder'=>'Apellido','autocomplete'=>'off']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('antecedente_direccion1','Dirección Principal:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('antecedente_direccion1',$antecedente->antecedente_direccion1,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>

  </div>

<div class="form-group">
    <?php echo Form::label('antecedente_telefono','Telefono:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::text('antecedente_telefono',$antecedente->antecedente_telefono,['class'=>'form-control','placeHolder'=>'111-2222','autocomplete'=>'off']); ?>

  </div>

<div class="form-group">
    <?php echo Form::label('antecedente_email','Correo Electronico:'); ?>

    <span style="color:red"><strong>*</strong></span>
    <?php echo Form::email('antecedente_email',$antecedente->antecedente_email,['class'=>'form-control','placeHolder'=>'aaa@yyyy.com','autocomplete'=>'off']); ?>

  </div>

<div class="form-group">
    <div class="table-responsive">
      <table class="table" style="width:100%" id="">
          <tbody id="TablaAdiciones">
          <tr>
              <td><?php echo Form::label('antecedente_direccion2','Dirección Opcional:'); ?></td>
              <td><?php echo Form::label('antecedente_telefono2','Telefono Opcional:'); ?></td>
          </tr>
         <?php $__currentLoopData = $direccion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_dir): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>              
             
          <tr>
              <td>
                <input type="hidden" name="id_dir[]" value="<?php echo e($row_dir->id); ?>" />  
                <?php echo Form::text('antecedente_direccion2[]',$row_dir->direccion,['class'=>'form-control','placeHolder'=>'Dirección','autocomplete'=>'off']); ?>             
               </td>
              <td>
                <?php echo Form::text('antecedente_telefono2[]',$row_dir->telefono,['class'=>'form-control','placeHolder'=>'telefono','autocomplete'=>'off']); ?>             
               </td>              
          </tr>
                      
          <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>      
        </tbody>      
      </table>
        <a id="Button" onclick="AddTr('../')"  title="Click para Adicionar casillas" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>    &nbsp;<a id="Button" onclick="DeleteTr()" title="Click para Eliminar Casillas"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
        <input type="hidden" name="numIndices" id="numIndices" value="0">
    </div>
  </div>

<?PHP 
$check_valor_1 = '';
$check_valor_2 = '';
$ver   = '';
if($antecedente->categoria_valor==1){
    $check_valor_1 = 'checked';
    $ver   = '';
}else if($antecedente->categoria_valor==2){
    $check_valor_2 = 'checked';
    $ver   = 'display:none';
}
if($antecedente->salario_id!=0){

$Valor = number_format(($Monto[0]->salario_montomensual*$antecedente->antecedente_sancion),2);

}else{
  $valor = '$ 0';
}
?>


<div class="form-group">
    <div class="table-responsive">
      <table class="table" style="width:100%">
          <tbody>
              <tr>
                  <td>
                        <?php echo Form::label('antecedente_sancion','Valor:'); ?>      
                        <span style="color:red"><strong>*</strong></span>
                  </td>
                  <td>
                      <?php echo Form::label('categoria','SMML'); ?>

                        <input  type="radio" id="smml" name="categoria_valor" value="1" <?php echo e($check_valor_1); ?>  />
                       <?php echo Form::label('categoria','Pesos'); ?>

                       <input type="radio" id="pesos" name="categoria_valor" value="2" <?php echo e($check_valor_2); ?>  />
                  </td>                 
              </tr>
              <tr>
                  <td colspan="2" >
                      <div class="col-xs-8">
                     <?php echo Form::text('antecedente_sancion',$antecedente->antecedente_sancion,['class'=>'form-control','placeHolder'=>'Numero de Salarios ó Valor ','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)','id'=>'sancion']); ?>   
                    </div> 
                    <div id="rango" style="<?php echo e($ver); ?>" class="col-xs-2">
                          <?php echo Form::select('periodo',$periodo,$antecedente->salario_id, ['class'=>'form-control','placeholder' => 'Seleccione Año...','onchange'=>'CalcularValor(this.value,"../")'] );; ?>

                      
                    </div>
                    <div id="DivRango" style="<?php echo e($ver); ?>" class="col-xs-2">
                      <?php echo Form::text('valor',null ,['class'=>'form-control','id'=>'valor','readonly','style'=>'text-align: right']); ?> 
                    </div>
                  </td>
              </tr>
          </tbody>
      </table>   
      </div>    
  </div>
    <div class="form-group">
            <?php echo Form::label('antecedente_descripcion','Anotaciones:'); ?>

            <span style="color:red"><strong>*</strong></span>
            <?php echo Form::textarea('antecedente_descripcion',$antecedente->antecedente_descripcion,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

    </div>
 
   
  <div class="form-group">
          <?php echo Form::label('Ubiacion','Ubicación Física del Documento:'); ?>

          <?php echo Form::textarea('ubicacionfisica',$antecedente->ubicacionfisica,['class'=>'form-control','placeHolder'=>'Ubicación Física del Documento En El Archivo']); ?>

  </div>
  <div class="form-group">
    <?php echo Form::label('Estado','Estado:'); ?>

      <span style="color:red"><strong>*</strong></span>
    <?php echo Form::select('estado',$Estado,$antecedente->antecedente_estado , ['class'=>'form-control','placeholder' => 'Seleccione Estado...','required'] );; ?>

  </div>
  
 <div class="form-group">
   <?php echo Form::label('abogado','Nombre Abogado:'); ?>

   <span style="color:red"><strong>*</strong></span>     
   <?php echo Form::select('abogado',$abogado,[$antecedente->userid] , ['class'=>'form-control','placeholder' => 'Seleccione Abogado...'] );; ?>

  </div>
 <div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
           <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

   </div>
   <div class="form-group">
  	<?php echo Form::button('&nbsp;Actualizar ',['class'=>'glyphicon glyphicon-floppy-disk','type'=>'submit','title'=>'Click para Actualizar']); ?>

  </div>
<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>