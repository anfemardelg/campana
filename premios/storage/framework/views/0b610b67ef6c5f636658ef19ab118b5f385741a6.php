<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('titulovalor.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<br/>    
<table class="table table-responsive">
    <tbody>
        <tr>
            <td>
                <strong>Radicado</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_resolucion); ?></td>
        </tr>
        <tr>
            <td>
                <strong>Razón Social ó Nombre</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_nombre); ?>&nbsp;<?php echo e($ActoAdmin->antecedente_apellido); ?></td>
        </tr>
        <tr>
            <td>
                <strong>N&deg; Documento</strong>
            </td>
            <td><?php echo e($ActoAdmin->antecedente_nro_documento); ?></td>
        </tr>
    </tbody>
</table>     
<hr/>
<?php echo Form::open(['route'=>['titulovalor.update',$antecedente_id],'method'=>'PUT','enctype'=>'multipart/form-data']); ?>

<?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

<?php echo Form::hidden('antecedente_id',$ActoAdmin->id,['class'=>'form-control','required']); ?>

<div id="dialog-form" title="Listado de Documentos"></div> 
<div class="table-responsive"> 
<table class="table">
    <tbody>   
<?PHP $i=0; ?>         
<?php $__currentLoopData = $TituloValorEdit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $edit_row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr>
        <td>
            <?php echo Form::hidden('ids[]',$edit_row->id,['class'=>'form-control','required']); ?>

            <div class="form-group">
                <?php echo Form::label('titulojudicial:','N° Título Judicial:'); ?>

                <span style="color:red"><strong>*</strong></span>   
                <?php echo Form::text('titulo_judicial[]',$edit_row->titulo_judicial,['class'=>'form-control','placeHolder'=>'Numero de Título Judicial ','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)']); ?>    
            </div>
         </td>
         <td>
            <div class="form-group">
                <?php echo Form::label('Banco:','Banco:'); ?>

                <span style="color:red"><strong>*</strong></span>   
                <?php echo Form::select('Banco[]', $Banco,$edit_row->banco, ['class'=>'form-control','placeholder' => 'Seleccione el Banco...']); ?>

            </div>
        </td>
        <td>
            <div class="form-group">
                <?php echo Form::label('valor:','Valor del Título:'); ?>

                <span style="color:red"><strong>*</strong></span>   
                <?php echo Form::text('valor_titulo[]',$edit_row->valor,['class'=>'form-control','placeHolder'=>'Numero de Título Judicial ','autocomplete'=>'off','required','onkeypress'=>'return validarNumeros(event)']); ?> 
            </div>
        </td>
        
        <td>
            <div class="form-group">
                <?php echo Form::label('Estado','Estado:'); ?>

                <span style="color:red"><strong>*</strong></span>
                <select class="form-control"  name="Estado[]" onchange="Boxfileview(this.value,'<?php echo e($i); ?>')">
                    <option value="">Seleccione el Estado...</option>
                     <?php $__currentLoopData = $Estado; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_estado => $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($row_estado==$edit_row->estado): ?>
                            <?PHP $Selected = 'selected="selected"';?>
                        <?php else: ?>
                            <?PHP $Selected = '';?>
                        <?php endif; ?>
                        <option <?php echo e($Selected); ?> value="<?php echo e($row_estado); ?>"><?php echo e($value); ?></option>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>                   
                </select>                 
            </div>
        </td>        
        <td>
            <div class="form-group" id="TD_Opcion_<?php echo e($i); ?>">
             <?php echo Form::label('opciones:','Opciones:'); ?>  

            <?PHP 
          
            if(count($ListaDocumentos)<1){
                $fileData = false;
            }else{
                $fileData = true;
            }    
            ?>           
            <?php if($fileData): ?>
               
                <?php if($edit_row->ver==true): ?>

                    <a onclick="ListarDocumentosDelete(<?php echo e($edit_row->id); ?>,'titulovalorid','titulovalor')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a  onclick="deleteTitulo(<?php echo e($edit_row->id); ?>)" class="btn btn-danger btn-xs" title="Eliminar Título Valor"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                    <?php else: ?> 
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="hidden" name="index[]" value="<?php echo e($i); ?>">
                                        <input type="file" name="file_primary" multiple>
                                        <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

                                    </td>
                                    <td>
                                        <a  onclick="deleteTitulo(<?php echo e($edit_row->id); ?>)" class="btn btn-danger btn-xs" title="Eliminar Título Valor"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>                                        
                <?php endif; ?>
            <?php else: ?>      
            <table class="table table-responsive">
                <tbody>
                    <tr>
                        <td>
                            <input type="hidden" name="index[]" value="<?php echo e($i); ?>">
                            <input type="file" name="file_primary" multiple>
                            <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

                        </td>
                        <td>
                            <a  onclick="deleteTitulo(<?php echo e($edit_row->id); ?>)" class="btn btn-danger btn-xs" title="Eliminar Título Valor"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                        </td>
                    </tr>
                </tbody>
            </table>    
            <?php endif; ?>                   
            </div>
            <div class="form-group" id="Td_FileOpen_<?php echo e($i); ?>" style="visibility: collapse;">
                <input type="hidden" name="index[]" value="<?php echo e($i); ?>">
                <input type="file" name="file_primary" multiple>
                <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx  .xls  .xlsx'); ?>

            </div>
        </td>
    </tr>
<?PHP $i++; ?>    
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </tbody>
</table>
<div class="form-group">
    <?php echo Form::button('&nbsp;Modificar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Modificar','type'=>'submit']); ?>

</div>
</div>
<?php echo Form::close(); ?>