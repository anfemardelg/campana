<?php $__env->startSection('title','Creación de Salarios Minimos por Año'); ?>
<?php $__env->startSection('title_panel','Salarios Minimos'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<script type="text/javascript">
    function validaPeriodo(){
        var periodo = $('#salario_ano').val();
        var token = $('input:hidden[name=_token]').val();
   
        $.ajax({ //Ajax
                type: 'POST',
                headers:{'X-CSRF-TOKEN':token},
                url: 'ValidaPeriodoExiste',
                async: false,
                dataType: 'json',
                data:({salario_ano:periodo}),
                error:function(objeto, quepaso, otroobj){alert('Error de Conexión , Favor Vuelva a Intentar');},
                success: function(data){
                    if(data.val===false){
                        return false;
                    }else{
                        return true;
                    }
                }//data 
        }); //AJAX
        
    }//function validaPeriodo</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('salario.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['salario.store'],'method'=>'POST','enctype'=>'multipart/form-data','id'=>'formSalarios']); ?>

    <?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']);
        Form::hidden('salario_auxvarianual', '0');
        Form::hidden('salario_auxmensual', '0'); ?>

    <div class="form-group">
        <?php echo Form::label('anosalario','Año :'); ?>

        <span style="color:red"><strong>*</strong></span>  
         <?php echo Form::select('salario_ano',$anios_lista,null,['class'=>'form-control','placeholder'=>'Seleccione el Año','required','id'=>'salario_ano']); ?>

    </div>
   <!-- <div class="form-group">       
        <?php echo Form::label('salario_auxmensual','Auxilio Transporte Monto Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_auxmensual',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>


    </div>
    <div class="form-group">       
        <?php echo Form::label('salario_auxvarianual','Auxilio Transporte Variación Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
         <?php echo Form::text('salario_auxvarianual',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>
-->
    <div class="form-group">       
        <?php echo Form::label('salario_montodiario','Salario Monto Diario :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_montodiario',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'9999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

     <div class="form-group">       
        <?php echo Form::label('salario_montomensual','Salario Monto Mensual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_montomensual',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>

    <div class="form-group">       
        <?php echo Form::label('salario_varianual','Salario Variación Anual :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('salario_varianual',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'999999','onkeypress'=>'return validarNumeros(event)']); ?>

    </div>



<br><br>
    <div class="form-group">
        <?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit','id'=>'SaveSalario','onclick'=>'return validaPeriodo()' ]); ?>

      </div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>