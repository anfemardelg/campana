<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte total de redenciones'); ?>
<?php $__env->startSection('title_panel','Reporte'); ?>
<?php $__env->startSection('content'); ?>


<?php echo Form::open(['method'=>'POST', 'action' => 'ReportesController@total']); ?>

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte total
		</legend>
		<table id="Total" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Orden</th>
				<th>Referencia</th>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Dirección</th>
				<th>Depto</th>
				<th>Ciudad</th>
				<th>Documento</th> 
				<th>Usuario</th>
		                <th>Fecha</th>
                		<th>Tipo</th>
			</tr>
		</thead>            
		<tbody>
			<?php $__currentLoopData = $total; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $redencion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
				<tr>
					<td><?php echo e($redencion->orden); ?></td>
					<td><?php echo e($redencion->referencia); ?></td>
					<td><?php echo e($redencion->producto); ?></td>
					<td><?php echo e($redencion->cantidad); ?></td>
					<td><?php echo e($redencion->direccion); ?></td>
					<td><?php echo e($redencion->depto); ?></td>
					<td><?php echo e($redencion->ciudad); ?></td>
					<td><?php echo e($redencion->documento); ?></td>
					<td><?php echo e($redencion->usuario); ?></td>
			                <td><?php echo e($redencion->fecha); ?></td>
                    			<td><?php echo e($redencion->tipo); ?></td>
				</tr>     
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<script type="application/javascript" src="<?php echo e(asset('js/redemir/Reportes/diario.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>