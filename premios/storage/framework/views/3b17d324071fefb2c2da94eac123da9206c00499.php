<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
Administración de campañas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
 Campañas
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
        
	            <a class="btn btn-success" href="<?php echo e(route('campanas.create')); ?>">Crear Campañas</a>
            
	        </div>
	    </div>
	</div>
    <br>
    <?php if($message = Session::get('success')): ?>
		<div class="alert alert-success">
			<p><?php echo e($message); ?></p>
		</div>
	<?php endif; ?>
    
    <table id="tablacampaña" class="table table-bordered">
		<thead>
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Foto</th>
                <th width="200px">Accion</th>
            </tr>
		</thead>
	    <tbody>
        
        <tr>
            <?php $__currentLoopData = $campanas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $ps_shop): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <td><?php echo $ps_shop->id_shop; ?></td>
            <td><?php echo $ps_shop->name; ?></td>
            <td><?php echo $ps_shop->date_ini; ?></td>
            <td><?php echo $ps_shop->date_fin; ?></td>
            <td>
                <img src="">
            </td>
            <td>
                <a class="btn btn-info" href="<?php echo e(route('campanas.edit', $ps_shop->id_shop)); ?>">Editar</a>
                <a class="btn btn-primary" href="<?php echo e(route('importardata.index')); ?>">Cargar</a>
            </td> 
        <tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>    
	    </tbody>
	</table>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>