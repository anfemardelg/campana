<?php $__env->startSection('title','Creación de Actividades'); ?>
<?php $__env->startSection('title_panel','Creación de Actividades'); ?>
<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/functions.js')); ?>"></script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a  id="Button"  href="<?php echo e(route('estados.index')); ?>"> Regresar</a>
        </div>
    </div>
</div>
<?php echo Form::open(['route'=>['estados.store'],'method'=>'POST','enctype'=>'multipart/form-data','id'=>'formestados']); ?>

    <?php echo Form::hidden('userid',Auth::user()->id,['class'=>'form-control','required', 'id'=>'userid']); ?>

    <div class="form-group">
        <?php echo Form::label('nameestado','Nombre de la Actividad :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::text('estado_nombre',null,['class'=>'form-control','placeholder'=>'Nombre de la Actividad','autocomplete'=>'off','required']); ?>        
    </div>
    <div class="form-group">       
        <?php echo Form::label('Descripestado','Descripción :'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::textArea('estado_descripcion',null,['class'=>'form-control','placeholder'=>'Descripción de la Actividad','required']); ?>

    </div>
     <div class="form-group">
            <table class="table table-striped display table-responsive">
                <tr>
                    <td>
                          <?php echo Form::label('diasproces','Duración de  la  Actividad en Días :'); ?>

                            <span style="color:red"><strong>*</strong></span>  
                    </td>
                    <td>
                          <?php echo Form::label('venceproces','Notifica vencimiento a # Días:'); ?>

                            <span style="color:red"><strong>*</strong></span>  
                    </td>
                </tr>
                <tr>
                    <td>
                         <?php echo Form::number('estado_dias',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>       
                    </td>
                    <td>
                         <?php echo Form::number('estado_dias_vence',0,['class'=>'form-control','autocomplete'=>'off','required','min'=>'0','max'=>'60','onkeypress'=>'return validarNumeros(event)']); ?>      
                    </td>
                </tr>
            </table>      
        </div>
   <div class="form-group">        
        <?php echo Form::label('Proceso','Actividad:'); ?>

        <span style="color:red"><strong>*</strong></span>  
        <?php echo Form::select('Procesoid',$C_Proceso,null,['class'=>'form-control','placeholder'=>'Seleccione la Actividad','required']); ?>

    </div>
<br><br>
    <div class="form-group">
        <?php echo Form::button('&nbsp;Registrar',['class'=>'glyphicon glyphicon-floppy-disk','title'=>'Click para Registrar','type'=>'submit','id'=>'SaveEstado']); ?>

      </div>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>