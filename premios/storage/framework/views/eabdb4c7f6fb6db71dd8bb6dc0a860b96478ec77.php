<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Título Valor'); ?>
<?php $__env->startSection('title_panel','Título Valor'); ?>
<?php $__env->startSection('content'); ?>

<script type="text/javascript" src="<?php echo e(asset('js/titulovalor.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/documentos.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#TituloValor').DataTable({
            dom: 'Bfrtip',
            buttons: [
                    'excel', 'pdf'
                  ],
            "language": {
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"    
            }
            }
        });
});
</script>
<div id="dialog-form" title="Listado de Documentos"></div>    
<div class="table-responsive"> 
    <table id="TituloValor" class="table" >
        <thead>
            <tr>
                <th>#</th>
                <th>Resolución</th>
                <th>Razón Social ó Nombre</th>
                <th>N&deg; Documento</th>
                <th>Opción</th>
            </tr>
        </thead>
        <tbody>
            <?PHP $i=0; ?>
            <?php $__currentLoopData = $actoAdmin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($row->antecedente_estado==18): ?>
            <tr>
                <td><?php echo e($i+1); ?></td>
                <td><?php echo e($row->antecedente_resolucion); ?></td>
                <td><?php echo e($row->antecedente_nombre); ?>&nbsp;<?php echo e($row->antecedente_apellido); ?></td>
                <td><?php echo e($row->antecedente_nro_documento); ?></td>
                <td>
                    <?php if (\Entrust::can('titulo-create')) : ?>
                    <a onclick="addTituloValor(<?php echo e($row->id); ?>)" class="btn btn-success btn-xs" title="Adicionar Título Valor" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                    <?php endif; // Entrust::can ?>
                    <?PHP 
                    $view = true;                                     
                    ?>
                    <?php $__currentLoopData = $ActoAdminDoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row_doc): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($row_doc['padre']==$row->id): ?>
                             <a onclick="ListarDocTitulosValor(<?php echo e($row->id); ?>,'titulovalorid')" class="btn btn-primary btn-xs" title="Visualizar Documentos Anexo"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php $__currentLoopData = $titulovalor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $titulo): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($titulo->antecedente_id==$row->id): ?>
                            <?php if($view): ?>
                            <?PHP $view=false; ?>    
                            <?php if (\Entrust::can('titulo-edit')) : ?>
                            <a onclick="EditTituloValor(<?php echo e($row->id); ?>)" class="btn btn-warning btn-xs" title="Editar"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
                            <?php endif; // Entrust::can ?>   

                            <?php if (\Entrust::can('titulo-delete')) : ?>
                            <a href="<?php echo e(route('titulovalor.destroyall', $row->id)); ?>" onclick="return confirm('Desea Eliminar el ó los Títulos Valor...!!')" class="btn btn-danger btn-xs" title="Eliminar Titulo Valor"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> 
                            <?php endif; // Entrust::can ?> 
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                     
                </td>
            </tr>
            <?php endif; ?>
            <?PHP $i++; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>