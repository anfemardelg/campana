<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                <div>
                 <img src="<?php echo e(asset('imagen/LogoSloganMinCIT.png')); ?>"  width="200" style="margin-top: -3%">&nbsp;&nbsp;<?php echo e(config('app.name', 'Laravel')); ?>

                </div>    
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="<?php echo e(route('antecedentes.index')); ?>">Antecedentes</a></li>
                <!--<li><a href="<?php echo e(route('reparto.index')); ?>">Reparto</a></li>-->
                <li><a href="<?php echo e(route('expedientes.index')); ?>">Expedientes</a></li>
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                 <!--<?php if(Auth::guest()): ?> -->
                   <!-- <li><a href="#">Ingresar</a></li>
                    <li><a href="<?php echo e(url('/register')); ?>">Registrarse</a></li> 
                <?php else: ?> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           Aldo Bohorquez  <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="#" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                <!--<?php endif; ?>-->
            </ul>
        </div>
    </div>
</nav>
