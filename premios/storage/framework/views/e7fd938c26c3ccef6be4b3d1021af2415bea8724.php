<script>
  $( document).ready(function() {
    $( ".datepicker2" ).datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true
        });
  } );
 </script>

 <?php if($cuotas>$nrocuotas[0]->parametro_codigo): ?>
  <input type="hidden" name="couta" id="cuota" value="<?php echo e($nrocuotas[0]->parametro_codigo); ?>">
  <script type="text/javascript" charset="uft-8">
  var n=$('#cuota').val();
    alert('Supera numero de cuotas permitidas!!! \n Numero de cuotas maximas: '+n);
  </script>
  <?php die;  ?>
 <?php endif; ?>

<?php echo Form::open(array('route' => 'storecuotasliq','method'=>'POST')); ?>

<table id="cuota" class="table table-striped table-responsive">
  <thead>
    <tr>
      <th>#</th>	
      <th>VALOR CUOTA</th>
      <th>FECHA PAGO</th>  
    </tr>
  </thead>
  <tbody>
   <?php
for($i=1;$i<=$cuotas;$i++){
	?>
	<tr>
		<td><?php echo e($i); ?>

    
    </td>
		<td>$<?php echo e(number_format($totalresultado)); ?>

		  <?php echo Form::hidden('id_total[]', $totalresultado, array('class' => 'typeahead form-control','id' => 'id_total')); ?>

      <?php echo Form::hidden('encid[]', $encid, array('class' => 'typeahead form-control','id' => 'encid')); ?>

     </td>
		<td>
			<div class="col-xs-6">
			  <?php echo Form::text('fecha_pago[]', null, array('placeholder' => 'Fecha de pago','class' => 'form-control datepicker2','autocomplete'=>'off','required')); ?>

			</div>
		</td>
	</tr>
	<?php
	}//for
	?>
  </tbody>
</table>  
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
	<button type="submit">Guardar</button>
</div>
<?php echo Form::close(); ?>



