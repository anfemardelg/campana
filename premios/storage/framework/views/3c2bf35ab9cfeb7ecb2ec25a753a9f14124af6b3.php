<?php $__env->startSection('content'); ?>

<?php $__env->startSection('title_panel'); ?>
    Crear campaña
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title'); ?>
    Crear campaña
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="<?php echo e(route('campanas.index')); ?>"> Regresar</a>
	        </div>
	    </div>
	</div>
    <?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
	<?php echo Form::open(array('route' => 'campanas.store','method'=>'POST','enctype'=>'multipart/form-data')); ?>

    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::text('nombre', null, array('placeholder' => 'Nombre Campaña','class' => 'form-control')); ?>

            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inicio Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <!--<input type="date" name="bday">-->
                <?php echo Form::date('inicio', null, array('placeholder' => 'Fecha Inicio Campaña','class' => 'form-control')); ?>

            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fin Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::date('final', null, array('placeholder' => 'Fecha Fin Campaña','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imagen Tienda:</strong>
                <span style="color:red"><strong>*</strong></span>
                <label for="exampleInputFile"></label>
                <?php echo Form::file('imagentienda', null, array('placeholder' => 'Imagen','class' => 'form-control')); ?>

                <p class="help-block">Tamaño imagen px .</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imagen Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <label for="exampleInputFile"></label>
                <?php echo Form::file('imagen', null, array('placeholder' => 'Imagen','class' => 'form-control')); ?>

                <p class="help-block">Tamaño imagen px .</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Productos Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <a class="btn btn-primary" href="<?php echo e(route('api.index')); ?>"> Ir a Productos</a>
                <br>
                <br>
                <table id="tablacampaña" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($modelos)): ?>
                        <tr>   
                            <?php $__currentLoopData = $modelos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $modelo): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <td><?php echo e($modelo->product_id); ?></td>

                        <tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </tbody>
                    <?php endif; ?>    
                </table>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-success">Registrar</button>
        </div>
        
        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>URL Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <?php echo Form::text('username', null, array('placeholder' => 'Usuario','class' => 'form-control')); ?>

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Copiar</button>
        </div>-->
    </div>    
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>