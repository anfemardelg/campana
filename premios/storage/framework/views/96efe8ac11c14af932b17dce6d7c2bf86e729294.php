<?php $__env->startSection('tittle','Edición Expedientes'); ?>
<?php $__env->startSection('title_panel','Edición de Expedientes'); ?>
<?php $__env->startSection('content'); ?>

<?php echo Form::open(['route'=>['expedientes.update',$expediente],'method'=>'PUT','enctype'=>'multipart/form-data']); ?>

<div class="form-group">
    <?php echo Form::label('detalle_id','Codigo Antecedente:'); ?>

    <?php echo Form::select('detalle_id',$detalles,$expediente->antecedenteid,['class'=>'form-control','placeHolder'=>'Seleccione el Antecedente','required','disabled']); ?>

</div>


  <div class="form-group">
    <?php echo Form::label('expediente_rad','Radicado:'); ?>

    <?php echo Form::text('expediente_rad',$expediente->expediente_rad,['class'=>'form-control','placeHolder'=>'Radicado','required','autocomplete'=>'off']); ?>

  </div>



  <div class="form-group">
    <?php echo Form::label('expediente_folio','Folio:'); ?>

    <?php echo Form::text('expediente_folio',$expediente->expediente_folio,['class'=>'form-control','placeHolder'=>'Folios','required','autocomplete'=>'off']); ?>

  </div>


  <div class="form-group">
    <?php echo Form::label('expediente_descripcion','Anotaciones:'); ?>

    <?php echo Form::textArea('expediente_descripcion',$expediente->expediente_descripcion,['class'=>'form-control','placeHolder'=>'Anotaciones','required']); ?>

  </div>

  <div class="form-group">
    <?php echo Form::label('expediente_fecha','Fecha Radicación:'); ?>

    <?php echo Form::text('expediente_fecha',$expediente->expediente_fecha,['class'=>'form-control','placeHolder'=>'mm/dd/yyyy','required', 'id'=>'datepicker','autocomplete'=>'off','readonly'=>'readonly']); ?>

  </div>

   <div class="form-group">
            <?php echo Form::label('antecedente_files','Adjuntar Documento:'); ?>

            <input type="file" name="file_primary[]" multiple>
           <?php echo Form::label('archivospermitidos','archivos permitidos  .pdf  .doc  .docx'); ?>

   </div>

   <div class="form-group">
    <?php echo Form::button('&nbsp;Actualizar',['class'=>'btn btn-primary glyphicon glyphicon-floppy-disk','title'=>'Click para Actualizar','type'=>'submit']); ?>

  </div>



<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>