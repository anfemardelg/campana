<?php echo e(csrf_field()); ?>

<?php $__env->startSection('title','Reporte Carga Usuarios'); ?>
<?php $__env->startSection('title_panel','Reporte de usuarios cargados'); ?>
<?php $__env->startSection('content'); ?>


<?php echo Form::open(['method'=>'POST', 'action' => 'ImportController@consulta']); ?>


    <div class="form-group">
        <fieldset>
            <legend>Usuarios Cargados</legend>
            <img src="<?php echo e(asset('imagen/icono/1494555369_excel.png')); ?>" id="DescargarData" style="cursor:pointer" width="30" title="Descargar Datos" >
            <input type="hidden" value="<?php echo e($idRepositorio); ?>" name="Repositorio" id="Repositorio" />
            <br/>
            <br/>
            <table id="Nuevos" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>E-mail</th>                    
                </tr>
            </thead>            
            <tbody>
                <?php 
                $n=1;
                 ?>
                <?php $__currentLoopData = $Nuevos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Detalle_new): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
                    <tr>
                        <td><?php echo e($n++); ?></td>
                        <td><?php echo e($Detalle_new->documento); ?></td>
                        <td><?php echo e($Detalle_new->nombre); ?></td>
                        <td><?php echo e($Detalle_new->apellido); ?></td>
                        <td><?php echo e($Detalle_new->email); ?></td>
                    </tr>     
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>    
            </table>  
        </fieldset>    
    </div>
    
    <div class="form-group">
        <fieldset>
            <legend>Usuarios Existen</legend>
            <table id="Existen" class="table table-striped table-bordered ConsultaCarga" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>E-mail</th>                    
                </tr>
            </thead>            
            <tbody>
                <?php 
                $n=1;
                 ?>
                <?php $__currentLoopData = $Existe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Detalle): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>               
                    <tr>
                        <td><?php echo e($n++); ?></td>
                        <td><?php echo e($Detalle->documento); ?></td>
                        <td><?php echo e($Detalle->nombre); ?></td>
                        <td><?php echo e($Detalle->apellido); ?></td>
                        <td><?php echo e($Detalle->email); ?></td>
                    </tr>     
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>    
            </table>
        </fieldset>    
    </div>

<?php echo Form::close(); ?>


<script type="application/javascript" src="<?php echo e(asset('js/redemir/importardata/consulta.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.template.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>