<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title','Default')</title>

    <!-- Styles {{ asset('css/sticky-footer.css') }}-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/stickyfooter.css')}}">
    <!--<link rel="stylesheet" href="{{asset('librerias/bootstrap-3.3.7-dist/css/bootstrap.css')}}">      -->
    <link rel='stylesheet' href="{{asset('librerias/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}" >
    <link rel='stylesheet' href="{{asset('librerias/DataTables-1.10.15/media/css/jquery.dataTables.min.css')}}" >
    <link rel='stylesheet' href="{{asset('librerias/DataTables-1.10.15/media/css/buttons.dataTables.min.css')}}" >
    <!--Bootstrap-->
      <link rel='stylesheet' href="{{asset('librerias/DataTables-1.10.15/media/css/buttons.bootstrap.min.css')}}" >
      <link rel='stylesheet' href="{{asset('librerias/jqueryui-datepicker/jquery-ui.css')}}" >
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
  
</head>
    
  <body>
      <div id="app">
          @include('admin.template.partials.nav')
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-md-10 col-md-offset-1">
                         <div class="panel panel-default">
                             <div class="panel-heading" style="font-size:18px"><strong>@yield('title_panel','Titulo del panel')</strong></div>
                             <div class="panel-body ">
                              @include('flash::message')
                               @yield('content')
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
      </div> 
      <br/><br/>
      <br/><br/>
        <footer class="footer">
            <div class="container">
                <div>
                    <a href="https://www.puntoscredencial.com.co/login?back=my-account">
                        <img src="{{asset('imagen/avia-m.png')}}"  width="200" style="margin-top: -2%" >
                    </a>    
                    <span class="text-muted">© Copyright {{ date('Y') }}</span>
                </div>  
            </div>
        </footer>
       <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="application/javascript" src="{{asset('librerias/jquery/js/jquery-1.12.4.js')}}"></script>   
        <script type="application/javascript" src="{{asset('librerias/jqueryui-datepicker/jquery-ui.js')}}"></script>  
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/jquery.dataTables.min.js')}}"></script>
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/dataTables.bootstrap.min.js')}}"></script>
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/buttons.bootstrap.min.js')}}"></script>
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/dataTables.buttons.min.js')}}"></script> 
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/buttons.html5.min.js')}}"></script>  
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/jszip.min.js')}}"></script> 
        <script type="application/javascript" src="{{asset('librerias/DataTables-1.10.15/media/js/buttons.colVis.min.js')}}"></script>  
      
    @yield('script')
      
  </body>
   
</html>
