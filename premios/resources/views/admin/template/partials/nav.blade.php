<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/home') }}">
                <div>
                 <img src="{{asset('imagen/puntos.png')}}" class="img-responsive" width="200" style="margin-top: -6%">
                </div>
            </a>
        </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
     
            <ul class="nav navbar-nav navbar-left">
               @permission(('admin-show'))
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @permission(('admin-user-show'))
                    <li><a href="{{ route('users.index') }}">Usuarios</a></li>
                    @endpermission 
                    @permission(('admin-rol-show'))  
                    <li><a href="{{ route('roles.index') }}">Roles</a></li>
                    @endpermission
                  </ul>
                </li>
               @endpermission  
               @permission(('importar-show'))    
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Campañas<span class="caret"></span></a>      
                  <ul class="dropdown-menu">                    
                    <li><a href="{{ route('campanas.index') }}">Administrar Campañas</a></li>
                    <!--<li><a href="{{ route('bloqueo.index') }}">Bloquear</a></li>-->
                    <!--<li><a href="{{ route('desbloqueo.index') }}">Desbloquear</a></li>-->
                    <!--<li><a href="{{ route('importardata.consulta') }}">Repositorio</a></li>-->
                  </ul>    
                </li>
                @endpermission
				@permission(('reporte-show'))
				<!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes<span class="caret"></span></a>
                  <ul class="dropdown-menu">                    
                    <li><a href="{{ route('reportes.index') }}">Diario</a></li>
                    <li><a href="{{ route('reportes.total') }}">Total</a></li>
					<li><a href="{{ route('reportes.actualiza') }}">Actualizados</a></li>
                    <li><a href="{{ route('reportes.bloqueo') }}">Bloqueados</a></li>
                    <li><a href="{{ route('reportes.desbloqueo') }}">Desbloqueados</a></li>
                    <li><a href="{{ route('reportes.payu') }}">Pagos PAYU</a></li>
                  </ul>
                </li>-->
				@endpermission
                @permission(('reporte-avia'))
				<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Redenciones<span class="caret"></span></a>
                  <ul class="dropdown-menu">                    
                    <li><a href="{{ route('reportes.redenciones') }}">Consultar</a></li>                    
                  </ul>
                </li>
				@endpermission
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Ingresar</a></li>
                    <!-- <li><a href="{{ url('/register') }}">Registrarse</a></li> -->
                @else
               
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->usr_name }} {{ Auth::user()->usr_lname }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>