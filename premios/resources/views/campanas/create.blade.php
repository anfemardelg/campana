@extends('admin.template.main')

@section('content')

@section('title_panel')
    Crear campaña
@endsection

@section('title')
    Crear campaña
@endsection

@section('content')
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('campanas.index') }}"> Regresar</a>
	        </div>
	    </div>
	</div>
    @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!! Form::open(array('route' => 'campanas.store','method'=>'POST','enctype'=>'multipart/form-data')) !!}
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::text('nombre', null, array('placeholder' => 'Nombre Campaña','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Inicio Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <!--<input type="date" name="bday">-->
                {!! Form::date('inicio', null, array('placeholder' => 'Fecha Inicio Campaña','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fin Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::date('final', null, array('placeholder' => 'Fecha Fin Campaña','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imagen Tienda:</strong>
                <span style="color:red"><strong>*</strong></span>
                <label for="exampleInputFile"></label>
                {!! Form::file('imagentienda', null, array('placeholder' => 'Imagen','class' => 'form-control')) !!}
                <p class="help-block">Tamaño imagen px .</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imagen Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <label for="exampleInputFile"></label>
                {!! Form::file('imagen', null, array('placeholder' => 'Imagen','class' => 'form-control')) !!}
                <p class="help-block">Tamaño imagen px .</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Productos Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                <a class="btn btn-primary" href="{{ route('api.index') }}"> Ir a Productos</a>
                <a class="btn btn-primary" href="{{ route('Products.todos') }}"> Todos</a>
                <br>
                <br>
                <table id="tablacampaña" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Valor Producto</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(isset($modelos))
                        <tr>   
                            @foreach($modelos as $key => $modelo)
                            <td>{{ $modelo->product_id }}</td>
                                <input type="hidden" name="idProduct[]" id="idProduct[]" value="{{ $modelo->product_id }}">
                            <td>{{ $modelo->name }}</td>
                                <input type="hidden" name="nameProduct[]" id="nameProduct[]" value="{{ $modelo->name }}">
                            <td>{{ $modelo->description }}</td>
                                <input type="hidden" name="descriptionProduct[]" id="descriptionProduct[]" value="{{ $modelo->description }}">
                            <td >${{ number_format($modelo->price) }}</td>
                                <input type="hidden" name="priceProduct[]" id="priceProduct[]" value="{{ number_format($modelo->price) }}">
                        <tr>

                        @endforeach
                    </tbody>
                    @endif    
                </table>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-success">Registrar</button>
        </div>
        
        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>URL Campaña:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::text('username', null, array('placeholder' => 'Usuario','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Copiar</button>
        </div>-->
    </div>    
    {!! Form::close() !!}
@endsection