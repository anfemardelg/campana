@extends('admin.template.main')
@section('content')

@section('title_panel')
Administración de campañas
@endsection

@section('title')
 Campañas
@endsection

@section('content')
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
        
	            <a class="btn btn-success" href="{{ route('campanas.create') }}">Crear Campañas</a>
            
	        </div>
	    </div>
	</div>
    <br>
    @if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
    
    <table id="tablacampaña" class="table table-bordered">
		<thead>
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Foto</th>
                <th width="200px">Accion</th>
            </tr>
		</thead>
	    <tbody>
        
        <tr>
            @foreach($campanas as $key => $ps_shop)
            <td>{!! $ps_shop->id_shop !!}</td>
            <td>{!! $ps_shop->name !!}</td>
            <td>{!! $ps_shop->date_ini !!}</td>
            <td>{!! $ps_shop->date_fin !!}</td>
            <td>
                <img src="">
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('campanas.edit', $ps_shop->id_shop) }}">Editar</a>
                <a class="btn btn-primary" href="{{ route('importardata.index') }}?id_shop= {{ $ps_shop->id_shop }}">Cargar</a>
            </td> 
        <tr>
        @endforeach    
	    </tbody>
	</table>
@endsection

