@extends('admin.template.main')

@section('content')

@section('title_panel')
 Editar usuario
@endsection

@section('title')
 Editar usuario
@endsection

@section('content')
	<div class="row">
	    <div class="col-lg-12 margin-tb">

	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('users.index') }}"> Regresar</a>
	        </div>
	    </div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
	<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Usuario:</strong>
		                {!! Form::text('username', null, array('placeholder' => 'usuario','class' => 'form-control','disabled')) !!}
		            </div>
		    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Nombres:</strong>
		                <span style="color:red"><strong>*</strong></span>
		                {!! Form::text('usr_name', null, array('placeholder' => 'Nombres','class' => 'form-control')) !!}
		            </div>
		    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
		            <div class="form-group">
		                <strong>Apellidos:</strong>
		                <span style="color:red"><strong>*</strong></span>
		                {!! Form::text('usr_lname', null, array('placeholder' => 'Apellidos','class' => 'form-control')) !!}
		            </div>
		    </div>

				<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
										<strong>N° Identificacion:</strong>
										<span style="color:red"><strong>*</strong></span>
										{!! Form::text('usr_personalid', null, array('placeholder' => 'Nro identificacion','class' => 'form-control')) !!}
								</div>
				</div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              {!! Form::label('usr_status','Estado:')!!}
              {!! Form::select('usr_status',['1'=>'Activo', '0'=>'Inactivo']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Contraseña:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Confirmar Contraseña:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::password('confirm-password', array('placeholder' => 'Confirmar Password','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Roles:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Modificar</button>
        </div>
	</div>
	{!! Form::close() !!}
@endsection
