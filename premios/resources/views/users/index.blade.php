
@extends('admin.template.main')
@section('content')

@section('title_panel')
 Administracion de usuarios
@endsection

@section('title')
 Usuarios
@endsection

@section('content')

<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>

	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
            @permission(('user-create'))
	            <a class="btn btn-success" href="{{ route('users.create') }}"> Crear Usuarios</a>
            @endpermission
	        </div>
	    </div>
	</div>
<br/>
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	<table id="tablauser" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Usuario</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Estado</th>
			<th width="200px">Roles</th>
			<th width="200px">Accion</th>
		</tr>
		</thead>
	<tbody>
	@foreach ($data as $key => $user)
	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $user->username }}</td>
		<td>{{ $user->usr_name }}</td>
		<td>{{ $user->usr_lname }}</td>
		<td> 

			@permission(('user-edit'))
			
			{!! Form::open(['method' => 'UPDATE','route' => ['users.user_status_updt', $user->id],'style'=>'display:inline']) !!}
            
			@if($user->usr_status==0)
				{!! Form::submit('Inactivo', ['class' => 'btn btn-danger','id'=>'Estado']) !!}
			@else
				{!! Form::submit('Activo', ['class' => 'btn btn-success','id'=>'Estado']) !!}
			@endif	
			{!! Form::close() !!}
			@endpermission
		</td>
		<td>
			@if(!empty($user->roles))
				@foreach($user->roles as $v)
					<label class="label label-success">{{ $v->display_name }}</label>
				@endforeach
			@endif
		</td>
		<td>
		
     @permission(('user-edit'))
			<a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Editar</a>
			{!! Form::open(['method' => 'UPDATE','route' => ['users.user_status_updt', $user->id],'style'=>'display:inline']) !!}
        	{!! Form::close() !!}
    @endpermission 

			

		</td>
	</tr>
	@endforeach
	</tbody>
	</table>

@endsection
