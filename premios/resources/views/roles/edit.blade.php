
@extends('admin.template.main')
@section('content')

@section('title_panel')
 Editar Rol
@endsection

@section('title')
 Editar rol
@endsection

@section('content')

<style type="text/css">
    fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
    
</style>

	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('roles.index') }}"> Regresar</a>
	        </div>
	    </div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
				<strong>Error!!</strong> Verifique informacion en los campos.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::text('display_name', null, array('placeholder' => 'Rol','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripción:</strong>
                <span style="color:red"><strong>*</strong></span>
                {!! Form::textarea('description', null, array('placeholder' => 'Descripcion','class' => 'form-control','style'=>'height:100px')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <legend >PERMISOS &nbsp<span style="color:red"><strong>*</strong></span> </legend>
                <br/>
                
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">ADMINISTRACIÓN</legend>
                    <div class="control-group">
                        @foreach($permission as $value)

                            @if ($value->orden == 1) 
                            	<label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                            	{{ $value->display_name }}</label>
                            	<br/>
                            @endif
                        @endforeach
                    </div>
                </fieldset>
                <br/>
                <!--<fieldset class="scheduler-border">
                    <legend class="scheduler-border">OPERACIÓN</legend>
                    <div class="control-group">
                        @foreach($permission as $value)

                            @if ($value->orden == 2) 
                                <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                {{ $value->display_name }}</label>
                                <br/>
                            @endif
                        @endforeach
                    </div>
                </fieldset>
                <br/>-->
                 

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Aceptar</button>
        </div>
	</div>

	{!! Form::close() !!}
@endsection
