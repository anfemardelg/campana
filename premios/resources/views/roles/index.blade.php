
@extends('admin.template.main')
@section('content')

@section('title_panel')
 Crear Roles
@endsection

@section('title')
 Roles
@endsection

@section('content')

<script type="text/javascript">
$(document).ready(function(){
    $('#tablaroles').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$('#eliminar').click(function(){
			if(confirm('Seguro desea eliminar el rol...?')){
				return true;
			}else{
				return false;
			}
		});
	});
</script>
	<div class="row">
	    <div class="col-lg-12 margin-tb">

	        <div class="pull-right">
	        	@permission(('role-create'))
	            <a class="btn btn-success" href="{{ route('roles.create') }}"> Crear Rol</a>
	            @endpermission
	        </div>
	    </div>
	</div>
	<br>
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	<table  id="tablaroles" class="table table-bordered">
		<thead>
		<tr>
			<th>No</th>
			<th>Nombre</th>
			<th>Descripcion</th>
			<th width="200px">Accion</th>
		</tr>
		</thead>
	<tbody>	
	@foreach ($roles as $key => $role)
	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $role->display_name }}</td>
		<td>{{ $role->description }}</td>
		<td>
			<!-- <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Mostrar</a> -->
			@permission(('role-edit'))
			<a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Editar</a>
			@endpermission
			@permission(('role-delete'))

			{!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger','id'=>'eliminar']) !!}
        	{!! Form::close() !!}
       	@endpermission
		</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	
@endsection
