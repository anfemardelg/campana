@extends('admin.template.main')
@section('content')

@section('title_panel')
Administración de Api
@endsection

@section('title')
    Api
@endsection

@section('content')
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
    <br>
    @if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
    <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('campanas.create') }}"> Regresar</a>
	        </div>
	    </div>
	</div>
    <br>

    
    
    <table id="tabla-api" class="table table-bordered">
		<thead>
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Foto</th>
                <th width="200px">Accion</th>
            </tr>
		</thead>
	    <tbody>
        
        <tr>
            @foreach($modelmarcas as $key => $modelmarca)
            <td>{{ $modelmarca->brand_id }}</td>
            <td>{{ $modelmarca->nombre }}</td>
            <td>
                <img src="{{ $modelmarca->logo }}" width="100px" height="60px">
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('Products.product') }}?valor={{ $modelmarca->brand_id }}">Productos</a>
                
            </td>
        <tr>
        @endforeach    
	    </tbody>
	</table>
@endsection



