@extends('admin.template.main')
@section('content')

@section('title_panel')
Administración de Product
@endsection

@section('title')
    Product
@endsection

@section('content')
<script type="text/javascript">
$(document).ready(function(){
    $('#tablauser').DataTable({
          "language": {
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente"    
            }
          }
        });
});
</script>
    <br>
    @if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
<div>    
    <div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('api.index') }}"> Regresar</a>
	        </div>
	    </div>
	</div>
    <br>
    <style type="text/css">
    .card text-white bg-primary mb-3 {
        /* Add shadows to create the "card" effect */
        border: 1px; !important;
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
    </style>
    
    {!! Form::open(array('route' => 'Products.carrito','method'=>'POST')) !!}
        @foreach($modelproducts as $key=> $modelproduct)
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
            <img name="images[]" id="images[]" value="{{ $modelproduct->image }}" src="{{ $modelproduct->image }}" width="180px" height="100px">
            <div class="card-body text-secondary">
                <h5 type="text" name="names[]" id="names[]" value="{{ $modelproduct->name }}" class="card-title" >{{ $modelproduct->name }}</h5>
                <div class="overflow-hidden">
                    <p class="card-text" name="descripcions[]" id="descripcions[]" value="{{ $modelproduct->description }}" >{{ $modelproduct->description }}</p>
                </div>
                <h5 type="text" name="prices[]" id="prices[]" value="{{ $modelproduct->price }}" class="card-title" >${{ number_format($modelproduct->price) }}</h5> 
                <input type="checkbox" name="productos[]" value="{{ $modelproduct->product_id }};;{{ $modelproduct->image }};;{{ $modelproduct->name }};;{{ $modelproduct->description }};;{{ $modelproduct->price }}" class="custom-control-input">
            </div>
        </div>
        <br>
        @endforeach
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Registrar</button>
        </div>
    </div>
    {!! Form::close() !!}    
</div>
@endsection

