
@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Importar Data')
@section('title_panel','Importar Data')
@section('content')

{!!  Form::open(['action' => 'ImportController@import','enctype'=>'multipart/form-data'])!!}
    <div class="form-group">
        <label for="exampleInputFile"></label>
        <input type="file" name="FileData" id="FileData" />
        <p class="help-block">Archivos Permitidos  xls , xlsx , txt .</p>
        <input type="text" name="id_shop" id="id_shop" value="<?php echo $_GET['id_shop']; ?>" />
    </div>
    <br><br>
    <input class="btn btn-success" type="submit" onClick="disabled='disabled'" name="EnviarData" id="EnviarData" value="Cargar Data" >                
{!! Form::close() !!}

@endsection