@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte Carga Usuarios')
@section('title_panel','Reporte de usuarios cargados')
@section('content')


{!! Form::open(['method'=>'POST', 'action' => 'ImportController@consulta']) !!}
<div class="row">
    <div class="form-group">
        <fieldset>
            <legend>Usuarios Cargados</legend>
            <img src="{{asset('imagen/icono/1494555369_excel.png')}}" id="DescargarData" style="cursor:pointer" width="30" title="Descargar Datos" >
            <input type="hidden" value="{{$idRepositorio}}" name="Repositorio" id="Repositorio" />
            <br/>
            <br/>
			
            <table id="Nuevos" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>E-mail</th> 
					<th>Dirección</th>
                    <th>Teléfono</th> 
                    <th>Celular</th> 
					<th>Ciudad</th>  
					<th>Departamento</th>
                    <th>Visa</th>
                    <th>Master</th>
                </tr>
            </thead>            
            <tbody>
                @php
                $n=1;
                @endphp
                @foreach($Nuevos as $Detalle_new)               
                    <tr>
                        <td>{{$n++}}</td>
                        <td>{{$Detalle_new->documento}}</td>
                        <td>{{$Detalle_new->nombre}}</td>
                        <td>{{$Detalle_new->apellido}}</td>
                        <td>{{$Detalle_new->email}}</td>                        
						<td>{{$Detalle_new->direccion}}</td>
                        <td>{{$Detalle_new->telefono}}</td>
                        <td>{{$Detalle_new->celular}}</td>
						<td>{{$Detalle_new->ciudad}}</td>
						<td>{{$Detalle_new->depto}}</td>
                        <td>{{$Detalle_new->visa}}</td>
                        <td>{{$Detalle_new->master}}</td>
                    </tr>     
                @endforeach
            </tbody>    
            </table>  
        </fieldset>    
    </div>
    
    <div class="form-group">
        <fieldset>
            <legend>Usuarios Existen</legend>
            <table id="Existen" class="table table-striped table-bordered ConsultaCarga" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>E-mail</th>
					<th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Celular</th>
					<th>Ciudad</th>
					<th>Departamento</th>
                </tr>
            </thead>            
            <tbody>
                @php
                $n=1;
                @endphp
                @foreach($Existe as $Detalle)               
                    <tr>
                        <td>{{$n++}}</td>
                        <td>{{$Detalle->documento}}</td>
                        <td>{{$Detalle->nombre}}</td>
                        <td>{{$Detalle->apellido}}</td>
                        <td>{{$Detalle->email}}</td>                        
						<td>{{$Detalle->direccion}}</td>
                        <td>{{$Detalle->telefono}}</td>
                        <td>{{$Detalle->celular}}</td>
						<td>{{$Detalle->ciudad}}</td>
						<td>{{$Detalle->depto}}</td>
                    </tr>     
                @endforeach
            </tbody>    
            </table>
        </fieldset>    
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('script')
<script type="application/javascript" src="{{asset('js/redemir/importardata/consulta.js')}}"></script>
@endsection
