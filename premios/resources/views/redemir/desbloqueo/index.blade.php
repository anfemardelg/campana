
@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Desbloquear')
@section('title_panel','Desbloquear usuarios')
@section('content')

{!!  Form::open(['action' => 'DesbloqueoController@import','enctype'=>'multipart/form-data'])!!}

    <div class="form-group">
        <label for="exampleInputFile"></label>
        <input type="file" name="FileData" id="FileData" />
        <p class="help-block">Archivos Permitidos  xls , xlsx , txt .</p>
    </div>
    <br><br>
    <input class="btn btn-success" type="submit" name="EnviarData" id="EnviarData" value="Cargar Data">                
{!! Form::close() !!}

@endsection