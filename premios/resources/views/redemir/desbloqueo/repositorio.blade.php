@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Repositorio Carga Usuarios')
@section('title_panel','Repositorio De Usuarios Cargados')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <fieldset>
            <legend>Repositorio De Usuarios Cargados</legend>
            
            <br/>
            <br/>

            <table id="Repositorio" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha de Carga</th>
                        <th>Nombre Archivo</th>
                        <th>Opción</th>                  
                    </tr>
                </thead>            
                <tbody>
                    @php
                    $n=1;
                    @endphp
                    @foreach($Datos as $Row)               
                    <tr>
                        <td>{{$n++}}</td>
                        <td style="text-align:center">{{Carbon\Carbon::parse($Row->fechacarga)->format('Y-m-d')}}</td>
                        <td>{{$Row->archivo}}</td>
                        <td>
                            <div style="margin-left: 40%;">
                                <img src="{{asset('imagen/icono/1494555369_excel.png')}}" id="DescargarData" onclick="DownloadRepositorio('{{$Row->idrepositorio}}')" style="cursor:pointer" width="20" title="Descargar Datos" >
                            </div>    
                        </td>
                    </tr>     
                @endforeach
                </tbody>    
            </table>  
            </fieldset>
        </div>
    </div>
</div>   

@endsection
@section('script')

<script type="application/javascript" src="{{asset('js/redemir/importardata/repositorio.js')}}"></script>

@endsection
