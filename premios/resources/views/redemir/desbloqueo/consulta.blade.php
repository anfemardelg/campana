@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte desbloqueo usuarios')
@section('title_panel','Reporte de usuarios desbloqueados')
@section('content')


{!! Form::open(['method'=>'POST', 'action' => 'DesbloqueoController@consulta']) !!}
<div class="row">
    <div class="form-group">
        <fieldset>
            <legend>Usuarios Desbloqueados</legend>
            <img src="{{asset('imagen/icono/1494555369_excel.png')}}" id="DescargarDesbloqueo" style="cursor:pointer" width="30" title="Descargar Datos" >
            <input type="hidden" value="{{$idRepositorio}}" name="Repositorio" id="Repositorio" />
            <br/>
            <br/>
			
            <table id="Desbloqueos" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th> 
                </tr>
            </thead>            
            <tbody>
                @php
                $n=1;
                @endphp
                @foreach($Desbloqueos as $desbloqueo)               
                    <tr>
                        <td>{{$n++}}</td>
                        <td>{{$desbloqueo->documento}}</td>
                    </tr>     
                @endforeach
            </tbody>    
            </table>  
        </fieldset>    
    </div>
    
    <div class="form-group">
        <fieldset>
            <legend>Usuarios No Existen</legend>
            <table id="nexisten" class="table table-striped table-bordered ConsultaBloqueo" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Documento</th>
                </tr>
            </thead>            
            <tbody>
                @php
                $n=1;
                @endphp
                @foreach($Nexiste as $Detalle)            
                    <tr>
                        <td>{{$n++}}</td>
                        <td>{{$Detalle->documento}}</td>
                    </tr>     
                @endforeach
            </tbody>    
            </table>
        </fieldset>    
    </div>
</div>
{!! Form::close() !!}
@endsection
<!--@section('script')
<script type="application/javascript" src="{{asset('js/redemir/importardata/consulta.js')}}"></script>
@endsection-->
