@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte de usuarios que actualizaron datos')
@section('title_panel','Reporte')
@section('content')

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte de usuarios actualizados
		</legend>
		<table id="Actualiza" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Cedula</th>
				<th>Usuario</th>
                <th>Email</th>
                <th>Autorización 1</th>
                <th>Autorización 2</th>
                <th>Tipo</th>
                <th>Agregado</th>
                <th>Actualizado</th>
			</tr>
		</thead>            
		<tbody>
			@foreach($actualiza as $usuario)               
				<tr>
					<td>{{$usuario->cedula}}</td>
					<td>{{$usuario->usuario}}</td>
                    <td>{{$usuario->email}}</td>
                    <td>{{$usuario->autorizacion1}}</td>
                    <td>{{$usuario->autorizacion2}}</td>
                    <td>{{$usuario->tipo}}</td>
                    <td>{{$usuario->agregado}}</td>
                    <td>{{$usuario->actualizado}}</td>
				</tr>     
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

@endsection
@section('script')
<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection
