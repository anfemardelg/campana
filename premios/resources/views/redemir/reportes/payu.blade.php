@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte de pagos')
@section('title_panel','Reporte')
@section('content')

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte de pagos
		</legend>
		<table id="Payu" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Orden</th>
				<th>Usuario</th>
                <th>Payu</th>
                <th>Valor</th>
                <th>Moneda</th>
                <th>Medio de pago</th>
                <th>Entidad</th>
                <th>Estado</th>
                <th>Fecha</th>
			</tr>
		</thead>            
		<tbody>
			@foreach($payu as $pago)               
				<tr>
					<td>{{$pago->orden}}</td>
					<td>{{$pago->usuario}}</td>
                    <td>{{$pago->payu}}</td>
                    <td>{{$pago->valor}}</td>
                    <td>{{$pago->moneda}}</td>
                    <td>{{$pago->medio_de_pago}}</td>
                    <td>{{$pago->entidad}}</td>
                    <td>{{$pago->estado}}</td>
                    <td>{{$pago->fecha}}</td>
				</tr>     
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

@endsection
@section('script')
<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection