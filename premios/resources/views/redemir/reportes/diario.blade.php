@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte de redenciones')
@section('title_panel','Reporte')
@section('content')


{!! Form::open(['method'=>'POST', 'action' => 'ReportesController@diario']) !!}
<div class="form-group">
	{!! Form::label('fechaini', 'Fecha Inicial', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-3">
		{!! Form::text('fechaini','',['id'=>'fechaini','class'=>'form-control', 'readonly'])!!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('fechafin', 'Fecha Final', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-3">
		{!! Form::text('fechafin','',['id'=>'fechafin','class'=>'form-control', 'readonly'])!!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-2">
		{!! Form::submit('Reporte', ['id'=>'reporte','class'=>'btn btn-primary'])!!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte diario {{$existe}}
		</legend>
		<table id="Diario" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Orden</th>
				<th>Referencia</th>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Dirección</th>
				<th>Depto</th>
				<th>Ciudad</th>                    
				<th>Usuario</th>
                <th>Fecha</th>
                <th>Tipo</th>
			</tr>
		</thead>            
		<tbody>
			@foreach($diario as $redencion)               
				<tr>
					<td>{{$redencion->orden}}</td>
					<td>{{$redencion->referencia}}</td>
					<td>{{$redencion->producto}}</td>
					<td>{{$redencion->cantidad}}</td>
					<td>{{$redencion->direccion}}</td>
					<td>{{$redencion->depto}}</td>
					<td>{{$redencion->ciudad}}</td>
					<td>{{$redencion->usuario}}</td>
                    <td>{{$redencion->fecha}}</td>
                    <td>{{$redencion->tipo}}</td>
				</tr>     
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

{!! Form::close() !!}

@endsection
@section('script')

<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection