@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte de usuarios desbloqueados')
@section('title_panel','Reporte')
@section('content')

<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte de usuarios desbloqueados
		</legend>
		<table id="Bloqueo" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Cedula</th>
				<th>Usuario</th>
                <th>Email</th>                
                <th>Agregado</th>
                <th>Actualizado</th>
                <th>Tipo</th>
			</tr>
		</thead>            
		<tbody>
			@foreach($desbloqueo as $usuario)               
				<tr>
					<td>{{$usuario->cedula}}</td>
					<td>{{$usuario->usuario}}</td>
                    <td>{{$usuario->email}}</td>                    
                    <td>{{$usuario->agregado}}</td>
                    <td>{{$usuario->actualizado}}</td>
                    <td>{{$usuario->tipo}}</td>
				</tr>     
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

@endsection
@section('script')
<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection