@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Reporte total de redenciones')
@section('title_panel','Reporte')
@section('content')


{!! Form::open(['method'=>'POST', 'action' => 'ReportesController@total']) !!}
<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Reporte total
		</legend>
		<table id="Total" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Orden</th>
				<th>Referencia</th>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Dirección</th>
				<th>Depto</th>
				<th>Ciudad</th>
				<th>Documento</th> 
				<th>Usuario</th>
		                <th>Fecha</th>
                		<th>Tipo</th>
			</tr>
		</thead>            
		<tbody>
			@foreach($total as $redencion)               
				<tr>
					<td>{{$redencion->orden}}</td>
					<td>{{$redencion->referencia}}</td>
					<td>{{$redencion->producto}}</td>
					<td>{{$redencion->cantidad}}</td>
					<td>{{$redencion->direccion}}</td>
					<td>{{$redencion->depto}}</td>
					<td>{{$redencion->ciudad}}</td>
					<td>{{$redencion->documento}}</td>
					<td>{{$redencion->usuario}}</td>
			                <td>{{$redencion->fecha}}</td>
                    			<td>{{$redencion->tipo}}</td>
				</tr>     
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

{!! Form::close() !!}

@endsection
@section('script')

<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection
