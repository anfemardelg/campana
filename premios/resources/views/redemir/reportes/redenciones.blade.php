@extends('admin.template.main')
{{ csrf_field() }}
@section('title','Redenciones')
@section('title_panel','Redenciones')
@section('content')


{!! Form::open(['method'=>'POST', 'action' => 'ReportesController@redenciones']) !!}
<div class="form-group">
	{!! Form::label('fechaini', 'Fecha Inicial', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-3">
		{!! Form::text('fechaini','',['id'=>'fechaini','class'=>'form-control', 'readonly'])!!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('fechafin', 'Fecha Final', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-3">
		{!! Form::text('fechafin','',['id'=>'fechafin','class'=>'form-control', 'readonly'])!!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-2">
		{!! Form::submit('Reporte', ['id'=>'reporte','class'=>'btn btn-primary'])!!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
	<br/>
	<br/>
	<fieldset>
		<legend>
			Redenciones
		</legend>
		<table id="Redenciones" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Banco</th>
				<th>Planilla</th>
				<th>Avia</th>
				<th>Fecha envio</th>       
				<th>Mes</th>
				<th>Año</th>       
				<th>Fecha de redención</th>
				<th>Cedula</th>       
				<th>Nombres</th>      
				<th>Dirección</th>
				<th>Ciudad</th>
				<th>Depto</th>
                <th>Email</th>
                <th>Celular</th>
				<th>Telefono</th>
				<th>Codigo</th>
				<th>Nombre Premio</th>
				<th>Descripción</th>
				<th>Cod Proveedor</th>
				<th>Nombre Provedor</th>
				<th>Cantidad</th>
				<th>Valor</th>
                <th>Tipo</th> 
			</tr>
		</thead>            
		<tbody>
			@foreach($redenciones as $redencion)               
				<tr>
					<td>{{$redencion->banco}}</td>
					<td>{{$redencion->planilla}}</td>
					<td>{{$redencion->avia}}</td>
					<td>{{$redencion->fecha_envio}}</td>
					<td>{{$redencion->mes}}</td>
					<td>{{$redencion->ano}}</td>
					<td>{{$redencion->fecha_redencion}}</td>
					<td>{{$redencion->document}}</td>
					<td>{{$redencion->nombres}}</td>
					<td>{{$redencion->direccion}}</td>
					<td>{{$redencion->ciudad}}</td>
					<td>{{$redencion->depto}}</td>
                    <td>{{$redencion->email}}</td>
                    <td>{{$redencion->celular}}</td>
					<td>{{$redencion->telefono}}</td>
					<td>{{$redencion->codigo}}</td>
					<td>{{$redencion->nombre_premio}}</td>
					<td>{{$redencion->descripcion}}</td>
					<td>{{$redencion->cod_proveedor}}</td>
					<td>{{$redencion->nombre_proveedor}}</td>
					<td>{{$redencion->cantidad}}</td>
					<td>{{$redencion->valor}}</td>
                    <td>{{$redencion->tipo}}</td>
				</tr>
			@endforeach
		</tbody>    
		</table>  
	</fieldset>
	</div>
</div>

{!! Form::close() !!}

@endsection
@section('script')

<script type="application/javascript" src="{{asset('js/redemir/Reportes/diario.js')}}"></script>
@endsection
