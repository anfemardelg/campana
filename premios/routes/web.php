<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('welcome2',['as'=>'welcome2','uses'=>'HomeController@index2']);

Auth::routes();

Route::group(['middleware' => ['auth']], function() {

  Route::get('/home', 'HomeController@index');
  
  
  
  Route::resource('campanas','BellController');

    Route::get('campanas',['as'=>'campanas.index','uses'=>'BellController@index','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
    Route::get('campanas/create',['as'=>'campanas.create','uses'=>'BellController@create','middleware' => ['permission:role-create']]);
    Route::post('campanas/create',['as'=>'campanas.store','uses'=>'BellController@store','middleware' => ['permission:role-create']]);
    Route::get('campanas/{id}',['as'=>'campanas.show','uses'=>'BellController@show']);
    Route::get('campanas/{id}/edit',['as'=>'campanas.edit','uses'=>'BellController@edit','middleware' => ['permission:role-edit']]);
    Route::patch('campanas/{id}',['as'=>'campanas.update','uses'=>'BellController@update','middleware' => ['permission:role-edit']]);
    Route::delete('campanas/{id}',['as'=>'campanas.destroy','uses'=>'BellController@destroy','middleware' => ['permission:role-delete']]);

  Route::resource('api','ApiController');
  
    Route::get('Products',['as'=>'Products.index','uses'=>'ApiController@index']);
    Route::get('Products/product',['as'=>'Products.product','uses'=>'ApiController@product','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
    Route::get('Products/todos',['as'=>'Products.todos','uses'=>'ApiController@Todosproduct','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
    
    Route::post('Products/carrito',['as'=>'Products.carrito','uses'=>'ApiController@carrito']);

  Route::resource('users','UserController');

	Route::get('roles',['as'=>'roles.index','uses'=>'RoleController@index','middleware' => ['permission:role-list|role-create|role-edit|role-delete']]);
	Route::get('roles/create',['as'=>'roles.create','uses'=>'RoleController@create','middleware' => ['permission:role-create']]);
	Route::post('roles/create',['as'=>'roles.store','uses'=>'RoleController@store','middleware' => ['permission:role-create']]);
	Route::get('roles/{id}',['as'=>'roles.show','uses'=>'RoleController@show']);
	Route::get('roles/{id}/edit',['as'=>'roles.edit','uses'=>'RoleController@edit','middleware' => ['permission:role-edit']]);
	Route::patch('roles/{id}',['as'=>'roles.update','uses'=>'RoleController@update','middleware' => ['permission:role-edit']]);
	Route::delete('roles/{id}',['as'=>'roles.destroy','uses'=>'RoleController@destroy','middleware' => ['permission:role-delete']]);

    Route::get('users',['as'=>'users.index','uses'=>'UserController@index','middleware' => ['permission:user-list|user-create|user-edit|user-delete']]);

    Route::get('users/create',['as'=>'users.create','uses'=>'UserController@create','middleware' => ['permission:user-create']]);
    Route::post('users/create',['as'=>'users.store','uses'=>'UserController@store','middleware' => ['permission:user-create']]);
    Route::get('users/{id}',['as'=>'users.show','uses'=>'UserController@show']);
    Route::get('users/{id}/edit',['as'=>'users.edit','uses'=>'UserController@edit','middleware' => ['permission:user-edit']]);
    Route::patch('users/{id}',['as'=>'users.update','uses'=>'UserController@update','middleware' => ['permission:user-edit']]);
    Route::delete('users/{id}',['as'=>'users.destroy','uses'=>'UserController@destroy','middleware' => ['permission:user-delete']]);
    Route::post('users/{id}',['as'=>'users.user_status_updt','uses'=>'UserController@user_status_updt','middleware' => ['permission:user-edit']]);

	

	Route::group(['prefix'=>'redemir'], function(){
    //Route::resource('antecedentes', 'AntecedentesController');
        Route::post('import',['uses' => 'ImportController@import']);
        Route::get('importardata',['uses'=>'ImportController@index','as'=>'importardata.index','middleware' => ['permission:importar-up|importar-list']]);
        Route::get('importardata/consulta',['uses'=>'ImportController@consulta','as'=>'importardata.consulta','middleware' => ['permission:importar-list']]);
        Route::get('importardata/descargaData/{id}',['uses'=>'ImportController@exportarDocumento','as'=>'importardata.descargaData']);
        Route::post('bloqueo',['uses' => 'BloqueoController@import']);
        Route::get('bloqueodata',['uses'=>'BloqueoController@index','as'=>'bloqueo.index','middleware' => ['permission:bloqueo-up|bloqueo-list']]);
        Route::get('bloqueo/consulta',['uses'=>'BloqueoController@consulta','as'=>'bloqueo.consulta','middleware' => ['permission:bloqueo-list']]);
        Route::post('desbloqueo',['uses' => 'DesbloqueoController@import']);
        Route::get('desbloqueodata',['uses'=>'DesbloqueoController@index','as'=>'desbloqueo.index','middleware' => ['permission:desbloqueo-up|desbloqueo-list']]);
        Route::get('desbloqueo/consulta',['uses'=>'DesbloqueoController@consulta','as'=>'desbloqueo.consulta','middleware' => ['permission:desbloqueo-list']]);
		Route::get('reportes',['as'=>'reportes.index','uses'=>'ReportesController@index']);
		Route::post('reportes/diario',['as'=>'reportes.diario','uses'=>'ReportesController@diario','middleware' => ['permission:reporte-show']]);
		Route::get('reportes/total',['as'=>'reportes.total','uses'=>'ReportesController@total','middleware' => ['permission:reporte-show']]);
		Route::get('reportes/actualiza',['as'=>'reportes.actualiza','uses'=>'ReportesController@actualiza','middleware' => ['permission:reporte-show']]);
        Route::get('reportes/bloqueo',['as'=>'reportes.bloqueo','uses'=>'ReportesController@bloqueo','middleware' => ['permission:reporte-show']]);
        Route::get('reportes/desbloqueo',['as'=>'reportes.desbloqueo','uses'=>'ReportesController@desbloqueo','middleware' => ['permission:reporte-show']]);
        Route::get('reportes/payu',['as'=>'reportes.payu','uses'=>'ReportesController@payu','middleware' => ['permission:reporte-show']]);
		Route::get('reportes/redenciones',['as'=>'reportes.redenciones','uses'=>'ReportesController@redenciones','middleware' => ['permission:reporte-avia']]);
        Route::post('reportes/redenciones',['as'=>'reportes.redenciones','uses'=>'ReportesController@redenciones','middleware' => ['permission:reporte-avia']]);
   });//Grupo


 });

/************************************/
