<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_shop_url extends Model
{
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ps_shop_url';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
            'id_shop_url',
            'id_shop',
            'domain',
            'domain_ssl',
            'physical_uri',
            'virtual_uri',
            'main',
            'active'
            
            
    ];
}