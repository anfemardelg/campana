<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_attribute_group_shop extends Model
{
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ps_attribute_group_shop';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
            'id_attribute_group',
            'id_shop'
    ];
}