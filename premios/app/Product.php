<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{    

     protected $fillable = [        
          'product_id',
          'name',
          'description',
          'condiciones',
          'image',
          'pvp',
          'brand_id',
          'brand_name',
          'catl1',
          'catl2',
          'catl3',  
          'silver'      
  ];

}