<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_cms extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_cms';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cms',
        'id_cms_category',
        'position',
        'active',
        'indexation'
    ];        
}
