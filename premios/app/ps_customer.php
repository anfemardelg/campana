<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_customer extends Model
{
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ps_customer';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
            'id_shop_group',
            'id_shop',
            'id_gender',
            'id_default_group',
            'id_lang',
            'id_risk',
            'company',
            'siret',
            'ape',
            'firstname',
            'lastname',
            'email',
            'document',
            'passwd',
            'last_passwd_gen',
            'birthday',
            'newsletter',
            'ip_registration_newsletter',
            'newsletter_date_add',
            'optin',
            'website',
            'outstanding_allow_amount',
            'show_public_prices',
            'max_payment_days',
            'secure_key',
            'note',
            'active',
            'is_guest',
            'deleted',
            'date_add',
            'date_upd',
            'autoriza1',
            'autoriza2',
            'actualiza'            
    ];
}