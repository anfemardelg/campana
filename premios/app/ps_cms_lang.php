<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_cms_lang extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_cms_lang';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cms',
        'id_lang',
        'id_shop',
        'meta_title',
        'head_seo_title',
        'meta_description',
        'meta_keywords',
        'content',
        'link_rewrite'

    ];        
}
