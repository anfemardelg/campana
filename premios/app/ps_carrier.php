<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_carrier extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_carrier';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_carrier',
        'id_reference',
        'id_tax_rules_group',
        'name',
        'url',
        'active',
        'deleted',
        'shipping_handling',
        'range_behavior',
        'is_module',
        'is_free',
        'shipping_external',
        'need_range',
        'external_module_name',
        'shipping_method',
        'position',
        'max_whidth',
        'max_height',
        'max_depth',
        'max_weight',
        'grade'
    ];        
}
