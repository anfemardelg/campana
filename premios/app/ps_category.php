<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_category extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_category';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_category',
        'id_parent',
        'id_shop_default',
        'level_depth',
        'nleft',
        'nringht',
        'active',
        'date_add',
        'date_upd',
        'position',
        'is_root_category'

    ];        
}
