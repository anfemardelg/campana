<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_product_lang extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_product_lang';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_product',
        'product_id',
        'id_shop',
        'id_lang',
        'description',
        'description_short',
        'link_rewrite',
        'meta_description',
        'meta_keywords',
        'meta_title',
        'name',
        'available_now',
        'available_later',
        'delivery_in_stock',
        'delivery_out_stock'
                
    ];        
}