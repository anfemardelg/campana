<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_group extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_group';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_group',
        'reduction',
        'prince_display_method',
        'show_prices',
        'date_add',
        'date_upd'
        
    ];        
}
