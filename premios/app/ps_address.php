<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_address extends Model
{
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ps_address';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
            'id_address',
			'id_country',
			'id_state',
			'id_customer',
			'id_manufacturer',
			'id_supplier',
			'id_warehouse',
			'alias',
			'company',
			'lastname',
			'firstname',
			'address1',
            'phone',
            'phone_mobile',
			'postcode',
			'city',
			'vat_number',
			'dni',
			'date_add',
			'date_upd',
			'active',
			'deleted'        
    ];
    
}
