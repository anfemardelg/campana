<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use SoapClient;

class ReportesController extends Controller
{	
	public function index(){
		$existe=date('Y-m-d');
		$diario = DB::connection('mysqlfidelidad')->select('
            SELECT a.id_order as orden, a.reference as referencia, c.`name` as producto, b.product_quantity as cantidad, d.address1 as direccion, f.`name` as depto, d.city as ciudad, concat(e.firstname," ", e.lastname) as usuario, a.date_add as fecha,
            CASE WHEN e.tipo = 1 then "Gratis" else "Pago" END as tipo
            FROM ps_orders a
            INNER JOIN ps_order_detail b
            ON a.id_order = b.id_order
            INNER JOIN ps_product_lang c
            ON b.product_id = c.id_product
            INNER JOIN ps_address d
            ON a.id_address_delivery = d.id_address
            INNER JOIN ps_customer e
            ON a.id_customer = e.id_customer
            INNER JOIN ps_state f
            ON d.id_state = f.id_state
            WHERE date(a.date_add) = curdate()');
		return view('redemir.reportes.diario', compact('diario','existe'));
	}
	
	public function diario(Request $request){
		$fechaini = $request->fechaini;
		$fechafin = $request->fechafin;
		if ($fechaini == '' && $fechafin == ''){
			$fechadata =' date(a.date_add) = curdate()';
			$existe=date('Y-m-d');
		}else{
			$fechadata =' date(a.date_add) >="'.$fechaini.'" AND date(a.date_add)<="'.$fechafin.'"';
			$existe=$fechaini.' a '.$fechafin;
		}		
		$diario = DB::connection('mysqlfidelidad')->select('
            SELECT a.id_order as orden, a.reference as referencia, c.`name` as producto, b.product_quantity as cantidad, d.address1 as direccion, f.`name` as depto, d.city as ciudad, concat(e.firstname," ", e.lastname) as usuario, a.date_add as fecha,
            CASE WHEN e.tipo = 1 then "Gratis" else "Pago" END as tipo
            FROM ps_orders a
            INNER JOIN ps_order_detail b
            ON a.id_order = b.id_order
            INNER JOIN ps_product_lang c
            ON b.product_id = c.id_product
            INNER JOIN ps_address d
            ON a.id_address_delivery = d.id_address
            INNER JOIN ps_customer e
            ON a.id_customer = e.id_customer
            INNER JOIN ps_state f
            ON d.id_state = f.id_state
            WHERE '.$fechadata);
		return view('redemir.reportes.diario', compact('diario','existe'));
	}
	
	public function total(){		
		$total = DB::connection('mysqlfidelidad')->select('
            SELECT a.id_order as orden, a.reference as referencia, c.`name` as producto, b.product_quantity as cantidad, d.address1 as direccion, f.`name` as depto, d.city as ciudad, e.document as documento, concat(e.firstname," ", e.lastname) as usuario, a.date_add as fecha,
            CASE WHEN e.tipo = 1 then "Gratis" else "Pago" END as tipo
            FROM ps_orders a
            INNER JOIN ps_order_detail b
            ON a.id_order = b.id_order
            INNER JOIN ps_product_lang c
            ON b.product_id = c.id_product
            INNER JOIN ps_address d
            ON a.id_address_delivery = d.id_address
            INNER JOIN ps_customer e
            ON a.id_customer = e.id_customer
            INNER JOIN ps_state f
            ON d.id_state = f.id_state
		');
		return view('redemir.reportes.total', compact('total'));
	}
    
    public function actualiza(){		
		$actualiza = DB::connection('mysqlfidelidad')->select('
            SELECT document as cedula, concat(firstname," ",lastname) as usuario, email,
            CASE WHEN autoriza1 = 1 then "Si" else "No" END as autorizacion1, 
            CASE WHEN autoriza2 = 1 then "Si" else "No" END as autorizacion2,
            CASE WHEN tipo = 1 then "Gratis" else "Pago" END as tipo,
            date_add as agregado, date_upd as actualizado 
            from ps_customer
            where actualiza = 1
		');
		return view('redemir.reportes.actualiza', compact('actualiza'));
    }
    
    public function bloqueo(){		
		$bloqueo = DB::connection('mysqlfidelidad')->select('
            SELECT document as cedula, concat(firstname," ",lastname) as usuario, email, date_add as agregado, date_upd as actualizado,
            CASE WHEN tipo = 1 then "Gratis" else "Pago" END as tipo
            FROM ps_customer
            WHERE bloqueo = 1
		');
		return view('redemir.reportes.bloqueo', compact('bloqueo'));
    }
    
    public function desbloqueo(){		
		$desbloqueo = DB::connection('mysqlfidelidad')->select('
            SELECT document as cedula, concat(firstname," ",lastname) as usuario, email, date_add as agregado, date_upd as actualizado,
            CASE WHEN tipo = 1 then "Gratis" else "Pago" END as tipo
            FROM ps_customer
            WHERE bloqueo = 0
		');
		return view('redemir.reportes.desbloqueo', compact('desbloqueo'));
    }
    
    public function payu(){		
		$payu = DB::connection('mysqlfidelidad')->select('
            SELECT a.id_order as orden, concat(c.firstname," ",c.lastname) as usuario, a.id_transaction as payu, 
            a.value_transaction as valor, a.currency as moneda, a.payment as medio_de_pago, a.entity as entidad, 
            a.state_transaction as estado, a.date_transaction as fecha
            FROM ps_customer_buypoints a
            INNER JOIN ps_orders b
            ON a.id_order = b.id_order
            INNER JOIN ps_customer c
            ON a.id_customer = c.id_customer
        ');
		return view('redemir.reportes.payu', compact('payu'));
    }
    
    public function redenciones(Request $request){
        $fechaini = $request->fechaini;
        $fechafin = $request->fechafin;
        if ($fechaini == '' && $fechafin == ''){
            $fechadata ='';
        }else{
            $fechadata ='WHERE date(d.fecha) >="'.$fechaini.'" AND date(d.fecha)<="'.$fechafin.'"';
        }
        $redenciones = DB::connection('mysqlfidelidad')->select('
            SELECT banco, planilla, avia, fecha_envio, mes, ano, fecha_redencion, document, nombres, direccion, 
            ciudad, depto, email, celular, telefono, codigo, nombre_premio, descripcion, nombre_proveedor, cod_proveedor, 
            CASE
                WHEN d.cantidad>1 THEN 1
                ELSE 1
            END as cantidad,
            CASE 
                WHEN d.cantidad>1 THEN d.valor/d.cantidad
                ELSE d.valor
            END as valor, 
            tipo, row_number AS n
            FROM
                (
                    SELECT
                        @curRow := @curRow + 1 AS row_number
                    FROM
                        reporte
                    JOIN (SELECT @curRow := 0) r
                    WHERE
                        @curRow < (
                            SELECT
                                max(cantidad)
                            FROM
                                reporte
                        )
                ) AS vwtable2
            LEFT JOIN reporte d ON vwtable2.row_number <= d.cantidad '.$fechadata.'
            ORDER BY d.banco, vwtable2.row_number
            ');
        return view('redemir.reportes.redenciones', compact('redenciones'));
    }
}
