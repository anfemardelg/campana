<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Laracasts\Flash\Flash;
use App\Product;
use App\Marcas;
use Storage;

session_start();

class ApiController extends Controller
{
    //require del modelo producto: para llenarlo con lo que obtiene del consumo
    //Solicitud todos los productos
    public function index(Request $request){ 
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://wsrest.activarpromo.com/api/getbrands.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Host: wsrest.activarpromo.com",
            "cache-control: no-cache",
            "token: 8h703sT-4Ct_31*~",
            "user: avm"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $results = json_decode($response, true);
        $results = $results["response"]["message"];
        $cuantos=count($results);
        for($i = 0;$i< $cuantos;$i++)
        {
            $modelmarca = new Marcas();

            $modelmarca->brand_id=$results[$i]["brand_id"];
            $modelmarca->nombre=$results[$i]["nombre"];
            $modelmarca->logo=$results[$i]["logo"];
                    
            
            $modelmarcas[$i] = $modelmarca;
        }
        return view('Products.index',compact('modelmarcas'));        
    }

    public function Todosproduct(Request $request){ 
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://wsrest.activarpromo.com/api/getproducts.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n\t\"brand_id\":\"\"\r\n}\r\n",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Host: wsrest.activarpromo.com",
            "cache-control: no-cache",
            "token: 8h703sT-4Ct_31*~",
            "user: avm"
        ),
        ));

        $responses = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results = json_decode($responses, true);
        $results = $results["response"]["message"];
        /*if(!isset($results[0])){            
            Flash::error("Esta marca no tiene productos vigentes!");
            return redirect()->route('Products.index');    
        }*/

        $cuantos=count($results);
        for($i = 0;$i< $cuantos;$i++)
        {
            $modelproduct = new Product();

            $modelproduct->product_id=$results[$i]["product_id"];
            $modelproduct->name=$results[$i]["name"];
            $modelproduct->description=$results[$i]["description"];
            $modelproduct->image=$results[$i]["image"];
            $modelproduct->condiciones=$results[$i]["condiciones"];
            $modelproduct->price=$results[$i]["pvp"];
            
            $modelproducts[$i] = $modelproduct;
        } 
        return view('Products.product',compact('modelproducts'));
    }


    public function product(Request $request){ 
        $esta = $_GET['valor'];
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://wsrest.activarpromo.com/api/getproducts.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n\t\"brand_id\":\"$esta\"\r\n}\r\n",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Host: wsrest.activarpromo.com",
            "cache-control: no-cache",
            "token: 8h703sT-4Ct_31*~",
            "user: avm"
        ),
        ));

        $responses = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results = json_decode($responses, true);
        $results = $results["response"]["message"];
        if(!isset($results[0])){            
            Flash::error("Esta marca no tiene productos vigentes!");
            return redirect()->route('Products.index');    
        }

        $cuantos=count($results);
        for($i = 0;$i< $cuantos;$i++)
        {
            $modelproduct = new Product();

            $modelproduct->product_id=$results[$i]["product_id"];
            $modelproduct->name=$results[$i]["name"];
            $modelproduct->description=$results[$i]["description"];
            $modelproduct->image=$results[$i]["image"];
            $modelproduct->condiciones=$results[$i]["condiciones"];
            $modelproduct->price=$results[$i]["pvp"];
            
            $modelproducts[$i] = $modelproduct;
        } 
        return view('Products.product',compact('modelproducts'));
    }

    public function show()
    {
      return view('campanas.edit');
    }
    
    public function carrito(Request $request){
        $producto =$_POST['productos'];
        $cuantos=count($producto);


        for($i = 0;$i< $cuantos;$i++){
            $seleccionados=explode(";;",$producto[$i]);
            $modelo= new Product();
        
            $modelo->product_id= $seleccionados[0];
            $modelo->image= $seleccionados[1];
            $modelo->name= $seleccionados[2];
            $modelo->description= $seleccionados[3];
            $modelo->price= $seleccionados[4];


            $modelos[$i] = $modelo;
        }

        return view('campanas.create',compact('modelos'));
    }

    /*public function exportExcel()
    {

    }*/

    /*public function departamentos(Request $request){ 

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://wsrest.activarpromo.com/api/getbrands.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Host: wsrest.activarpromo.com",
            "cache-control: no-cache",
            "token: 8h703sT-4Ct_31*~",
            "user: avm"
        ),
        ));

        $marcas = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } 
        return $marcas;
    }*/
    

}