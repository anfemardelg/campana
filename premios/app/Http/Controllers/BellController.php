<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Laracasts\Flash\Flash;
use App\ps_shop;
use App\ps_attribute_group;
use App\ps_attribute_group_shop;
use App\ps_attribute;
use App\ps_currency_shop;
use App\ps_attribute_shop;
use App\ps_shop_url;
use App\ps_customer;
use App\ps_customer_group;
use App\ps_address;
use App\ps_product;
use App\ps_carrier;
use App\ps_carrier_shop;
use App\ps_product_lang;
use App\ps_product_shop;
use App\ps_module;
use App\ps_module_shop;
use App\repositorio;
use App\ps_carrier_tax_rules_group_shop; 
use App\ps_category;
use App\ps_category_shop;
use App\ps_cms;
use App\ps_cms_shop;
use App\ps_cms_category_shop;
use App\ps_country;
use App\ps_country_shop;
use App\repositoriodetalle;
use App\ps_czhomeslider_slides;
use App\ps_czhomeslider;
use App\ps_czleftbanner;
use App\ps_feature;
use App\ps_feature_shop;
use App\ps_group;
use App\ps_group_shop;
use App\ps_homeslider_slides;
use App\ps_homeslider;
use App\ps_lang_shop;
use App\ps_info_shop;
use App\ps_info_lang;
use Storage;


class BellController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){ 
        
        $campanas = ps_shop::orderBy('id_shop','DESC')->get();

        return view('campanas.index',compact('campanas'))
        ->with('i', ($request->input('page', 1) - 1) * 5);
                      
    }//public function index

    public function attribut(){ 

        $campanas_Attribute = ps_attribute::orderBy('id_attribute','DESC')->get();  

        return $campanas_Attribute;
        //print_r($campanas_Attribute);die;
    }
    
    public function homeslider_slides(){ 

        $homeslider_slides = ps_homeslider_slides::orderBy('id_homeslider_slides','DESC')->get();  

        return $homeslider_slides;
        //print_r($campanas_Attribute);die;
    }

    public function carrier(){ 

        $carrier = ps_carrier::orderBy('id_carrier','DESC')->get();  

        return $carrier;
    }

    public function country(){ 

        $country = ps_country::orderBy('id_country','DESC')->get();  

        return $country;
    }
    public function group(){ 

        $group = ps_group::orderBy('id_group','DESC')->get();  

        return $group;
    }
    
    public function feature(){ 

        $feature = ps_feature::orderBy('id_feature','DESC')->get();  

        return $feature;
    }

    public function czhomeslider_slides(){ 

        $czhomeslider_slides = ps_czhomeslider_slides::orderBy('id_czhomeslider_slides','DESC')->get();  

        return $czhomeslider_slides;
    }

    public function cms(){ 

        $cms = ps_cms::orderBy('id_cms','DESC')->get();  

        return $cms;
    }
    
    public function category(){ 

        $category = ps_category::orderBy('id_category','DESC')->get();  

        return $category;
    }

    public function module(){ 

        $module = ps_module::orderBy('id_module','DESC')->get();  

        return $module;
    }

    public function attributgroup(){ 

        $campanas_Attribute_group = ps_attribute_group::orderBy('id_attribute_group','DESC')->get();  

        return $campanas_Attribute_group;
    }

    public function create(){ 
        return view('campanas.create');
    }//public function create
    
    public function show()
    {
      return view('campanas.edit');
    }
    
    public function edit($id)
    {
      $campanas = ps_shop::find($id);
      return view('campanas.edit',compact('campanas'));
    }//public function edit
    
    public function store(Request $request){
        
        $campanas = new ps_shop();
        
            $campanas->id_shop_group      = '1';
            $campanas->name               = $request->input('nombre');
            $campanas->id_category        = '2';
            $campanas->theme_name         = 'Kitchen';
            $campanas->date_ini           = $request->input('inicio');
            $campanas->date_fin           = $request->input('final');;
            $campanas->active             = '1';
            $campanas->deleted            = '0';

            $campanas->save();

        $campanas_Url = new ps_shop_url();

            $Shop_id = $campanas->id;

            $campanas_Url->id_shop          = $Shop_id;
            $campanas_Url->domain           ='localhost:8080';
            $campanas_Url->domain_ssl       ='localhost:8080';
            $campanas_Url->physical_uri     ='/campanas/';
            $campanas_Url->virtual_uri      =$request->input('nombre')."/";
            $campanas_Url->main             ='1';
            $campanas_Url->active           ='1';
            
            $campanas_Url->save();

        $contadorId = count($_POST['idProduct']);

        for($i=0;$i<$contadorId;$i++){

            $products = new ps_product;

                $products->product_id                   = $_POST['idProduct'][$i];
                $products->id_supplier                  = "0";
                $products->id_manufacturer              = "0";
                $products->id_category_default          = "4";
                $products->id_shop_default              = "1";
                $products->id_tax_rules_group           = "1";
                $products->on_sale                      = "0";
                $products->online_only                  = "0";
                $products->ean13                        = "0";
                $products->isbn                         = "0";
                $products->upc                          = "0";
                $products->ecotax                       = "0,000000";
                $products->quantity                     = "0";
                $products->minimal_quantity             = "1";
                $products->low_stock_threshold          = "0";
                $products->low_stock_alert              = "0";
                $products->price                        = $_POST['priceProduct'][$i];
                $products->wholesale_price              = "0,000000";
                $products->unity                        = "0";
                $products->unit_price_ratio             = "0,000000";
                $products->additional_shipping_cost     = "0,00";
                $products->reference                    = "demo_1";
                $products->supplier_reference           = "0";
                $products->location                     = "0";
                $products->width                        = "0,000000";
                $products->height                       = "0,000000";
                $products->depth                        = "0,000000";
                $products->weight                       = "0,000000";
                $products->out_of_stock                 = "2";
                $products->additional_delivery_times    = "1";
                $products->quantity_discount            = "0";
                $products->customizable                 = "0";
                $products->uploadable_files             = "0";
                $products->text_fields                  = "0";
                $products->active                       = "1";
                $products->redirect_type                = "301-category";
                $products->id_type_redirected           = "0";
                $products->available_for_order          = "1";
                $products->available_date               = "0000-00-00";
                $products->show_condition               = "0";
                $products->condition                    = "new";
                $products->show_price                   = "1";
                $products->indexed                      = "1";
                $products->visibility                   = "both";
                $products->cache_is_pack                = "0";
                $products->cache_has_attachments        = "0";
                $products->is_virtual                   = "0";
                $products->cache_default_attribute      = "0";
                $products->date_add                     = $request->input('inicio');
                $products->date_upd                     = $request->input('inicio');
                $products->advanced_stock_management    = "0";
                $products->pack_stock_type              = "3";
                $products->state                        = "1";
                
                $products->save();


            $products_lang = new ps_product_lang;
                
                $ProductId = $products->id;

                $products_lang->id_product          = $ProductId;
                $products_lang->product_id          = $_POST['idProduct'][$i];
                $products_lang->id_shop             = $Shop_id;
                $products_lang->id_lang             = "1";
                $products_lang->description         = $_POST['descriptionProduct'][$i];
                $products_lang->description_short   = $_POST['descriptionProduct'][$i];
                $products_lang->link_rewrite        = "0";
                $products_lang->meta_description    = "0";
                $products_lang->meta_keywords       = "0";
                $products_lang->meta_title          = $_POST['nameProduct'][$i];
                $products_lang->name                = $_POST['nameProduct'][$i];
                $products_lang->available_now       = "0";
                $products_lang->available_later     = "0";
                $products_lang->delivery_in_stock   = "0";
                $products_lang->delivery_out_stock  = "0";

                $products_lang->save();
                
                
            $products_shop = new ps_product_shop;

                $products_shop->id_product                  = $ProductId;
                $products_shop->product_id                  = $_POST['idProduct'][$i];
                $products_shop->id_shop                     = $Shop_id;
                $products_shop->id_category_default         = "1";
                $products_shop->id_tax_rules_group          = "1";
                $products_shop->on_sale                     = "0";
                $products_shop->online_only                 = "0";
                $products_shop->ecotax                      = "0,000000";
                $products_shop->minimal_quantity            = "1";
                $products_shop->low_stock_threshold         = "";
                $products_shop->low_stock_alert             = "0";
                $products_shop->price                       = $_POST['priceProduct'][$i];
                $products_shop->wholesale_price             = "";
                $products_shop->unity                       = "";
                $products_shop->unit_price_ratio            = "";
                $products_shop->additional_shipping_cost    = "";
                $products_shop->customizable                = "";
                $products_shop->uploadable_files            = "";
                $products_shop->text_fields                 = "";
                $products_shop->active                      = "1";
                $products_shop->redirect_type               = "";
                $products_shop->id_type_redirected          = "0";
                $products_shop->available_for_order         = "1";
                $products_shop->available_date              = "";
                $products_shop->show_condition              = "";
                $products_shop->condition                   = "";
                $products_shop->show_price                  = "";
                $products_shop->indexed                     = "1";
                $products_shop->visibility                  = "";
                $products_shop->cache_default_attribute     = "";
                $products_shop->advanced_stock_management   = "";
                $products_shop->date_add                    = $request->input('inicio');
                $products_shop->date_upd                    = $request->input('inicio');
                $products_shop->pack_stock_type             = "";
                        
                $products_shop->save();
                
                unset($products_shop);
                unset($products_lang);
                unset($products);
        }

        $campanas_Attribute_group = $this->attributgroup();
        $contador = count($campanas_Attribute_group);
        
        for($i=0; $i<$contador;$i++){
            
            $Attribute_group_shop = new ps_attribute_group_shop;
            
                $asignarAG = $campanas_Attribute_group[$i]->id_attribute_group;

                $Attribute_group_shop->id_attribute_group  = $asignarAG;
                $Attribute_group_shop->id_shop             = $Shop_id;

        $Attribute_group_shop->save();  
        unset($Attribute_group_shop);
        }

        $module = $this->module();
        $contador = count($module);
        
        for($i=0; $i<$contador;$i++){
            
            $module_shop = new ps_module_shop;
            
                $asignarM = $module[$i]->id_module;

                $module_shop->id_module         = $asignarM;
                $module_shop->id_shop           = $Shop_id;
                $module_shop->enable_device     = '7';
                

            $module_shop->save();  
            unset($module_shop);
        }

        $currency = new ps_currency_shop();
        
            $currency->id_currency        = '1';
            $currency->id_shop            = $Shop_id;
            $currency->conversion_rate    = '1,000000';

            $currency->save();

        $carrier = $this->carrier();
        $contador = count($carrier);

        
        for($i=0; $i<$contador;$i++){
            
            $carrier_shop = new ps_carrier_shop;
            
                $asignarC = $carrier[$i]->id_carrier;

                $carrier_shop->id_carrier      = $asignarC;
                $carrier_shop->id_shop          = $Shop_id;

                $carrier_group_shop = new ps_carrier_tax_rules_group_shop;

                    $carrier_group_shop->id_carrier            = $asignarC;
                    $carrier_group_shop->id_tax_rules_group     = '1';
                    $carrier_group_shop->id_shop                = $Shop_id;               

        $carrier_shop->save();
        $carrier_group_shop->save();  
        unset($carrier_shop);
        unset($carrier_group_shop);
        }


        $category = $this->category();
        $contador = count($category);
        
        for($i=0; $i<$contador;$i++){
            
            $category_shop = new ps_category_shop;
            
                $asignarCa = $category[$i]->id_category;

                $category_shop->id_category      = $asignarCa;
                $category_shop->id_shop          = $Shop_id;
                $category_shop->position         = '0';

        $category_shop->save();  
        unset($category_shop);

        }
        
        $cms = $this->cms();
        $contador = count($cms);
        
        for($i=0; $i<$contador;$i++){
            
            $cms_shop = new ps_cms_shop;
            
                $asignarCm = $cms[$i]->id_cms;

                $cms_shop->id_cms          = $asignarCm;
                $cms_shop->id_shop          = $Shop_id;

        $cms_shop->save();  
        unset($cms_shop);

        }


        $cms_category = new ps_cms_category_shop;

            $cms_category->id_cms_category       = '1';
            $cms_category->id_shop              = $Shop_id;

        $cms_category->save();


        $country = $this->country();
        $contador = count($country);
        
        for($i=0; $i<$contador;$i++){
            
            $country_shop = new ps_country_shop;
            
                $asignarCo = $country[$i]->id_country;

                $country_shop->id_country          = $asignarCo;
                $country_shop->id_shop              = $Shop_id;

        $country_shop->save();  
        unset($country_shop);

        }

        $czhomeslider_slides = $this->czhomeslider_slides();
        $contador = count($czhomeslider_slides);
        
        for($i=0; $i<$contador;$i++){
            
            $czhomeslider = new ps_czhomeslider;
            
                $asignarCz = $czhomeslider_slides[$i]->id_czhomeslider_slides;

                $czhomeslider->id_czhomeslider_slides      = $asignarCz;
                $czhomeslider->id_shop                      = $Shop_id;

        $czhomeslider->save();  
        unset($czhomeslider);

        }
        
        $czleftbanner = new ps_czleftbanner;

            $czleftbanner->id_czleftbanner_slides      = '1';
            $czleftbanner->id_shop                     = $Shop_id;

        $czleftbanner->save();

        
        $feature = $this->feature();
        $contador = count($feature);
        
        for($i=0; $i<$contador;$i++){
            
            $feature_shop = new ps_feature_shop;
            
                $asignarF = $feature[$i]->id_feature;

                $feature_shop->id_feature      = $asignarF;
                $feature_shop->id_shop          = $Shop_id;

        $feature_shop->save();  
        unset($feature_shop);

        }
        
        $group = $this->group();
        $contador = count($group);
        
        for($i=0; $i<$contador;$i++){
            
            $group_shop = new ps_group_shop;
            
                $asignarG = $group[$i]->id_group;

                $group_shop->id_group      = $asignarG;
                $group_shop->id_shop        = $Shop_id;

        $group_shop->save();  
        unset($group_shop);

        }
        
        $homeslider_slides = $this->homeslider_slides();
        $contador = count($homeslider_slides);
        
        for($i=0; $i<$contador;$i++){
            
            $homeslider = new ps_homeslider;
            
                $asignarH = $homeslider_slides[$i]->id_homeslider_slides;

                $homeslider->id_homeslider_slides      = $asignarH;
                $homeslider->id_shop                    = $Shop_id;

        $homeslider->save();  
        unset($homeslider);

        }

        $lang_shop = new ps_lang_shop;

            $lang_shop->id_lang            = '1';
            $lang_shop->id_shop            = $Shop_id;

        $lang_shop->save();

        $info_shop = new ps_info_shop;

            $info_shop->id_info             = '1';
            $info_shop->id_shop             = $Shop_id;

        $info_shop->save();
        
        $info_lang = new ps_info_lang;

            $info_lang->id_info        = '1';
            $info_lang->id_shop        = $Shop_id;
            $info_lang->id_lang        = '1';
            $info_lang->text           = '';

        $info_lang->save();

        $campanas =request()->all();
        $campanas =request()->except('_token');
        return redirect()->route('campanas.index')
                    ->with('success','Campaña creada satisfactoriamente');
    }//public function agregar
    
}
