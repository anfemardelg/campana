<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Laracasts\Flash\Flash;
use App\ps_customer;
use App\repositorio;
use App\repositoriodetalle;
use Storage;

class BloqueoController extends Controller
{
    //
    public $DataExiste = array();
    public $DataNew    = array();
    public $ID_new     = 0;
    public $name;
    public $Key_Secret = 'Ahf121d!454/f7&%GHF87\GiS**09DSe';
    public $id_Repor;
    
    public function index(){         
    	return view('redemir.bloqueo.index');
    }//public function index
    
    public function import(Request $request){        
        if($request->file('FileData')){    
                $nama_File = $request->file('FileData')->getClientOriginalName();
                $Opcion = explode('.',$nama_File);
                $file_route = date('Ymd_h_i_s').'_'.$request->file('FileData')->getClientOriginalName();// Nombre de la Imagen
                Storage::disk('bloqueos')->put($file_route, file_get_contents( $request->file('FileData')->getRealPath()));// almaceno en el disco
                //$DataExiste = array();
                $this->name = $file_route;
				$validaData = false;
                switch ($Opcion[1]) {
                    case 'xlsx': 
                        Excel::load('public/bloqueos/'.$file_route,function($reader){
                            foreach ($reader->get() as $book) {					
								if ($book->documento){
									$validaData = $this->validaData($book);  
									if($validaData['val']){
										$this->insertData($book,$validaData['id']);
									}else{
										$this->DataExiste[] = $book;
									}
								}
                            }
                        });
                        break;
                    case 'xls':
                        Excel::load('public/bloqueos/'.$file_route,function($reader){
                            foreach ($reader->get() as $book) {								
								if ($book->documento){
									$validaData = $this->validaData($book);
									if($validaData){
										$this->insertData($book);
									}else{
										$this->DataExiste[] = $book;
									}//if
								}
                            }
                        });
                        break;	
                    case 'txt':
                        Excel::load('public/bloqueos/'.$file_route,function($reader){
                            foreach ($reader->get() as $book) {								
								if ($book->documento){
                                  $validaData = $this->validaData($book);
									if($validaData === true){
										$this->insertData($book);                                        
                                    }else{
										$this->DataExiste[] = $book;
									}//if
								}
                            }//foreach
                        });
                        break;
                }//switch
                $Nexiste = $this->DataExiste;
                $Bloqueos = $this->DataNew;
                $idRepositorio = $this->ID_new;
                return view('redemir.bloqueo.consulta',compact('Nexiste','Bloqueos','idRepositorio'));
        }else{
            Flash::error("Por favor cargar un archivo de forma correcta");
            return redirect()->route('bloqueo.index');
        }
    }//public function import
    
    public function insertData($Data, $id){        
                
        $C_repositorio      = new repositorio();
        $C_repDetalle       = new repositoriodetalle();        
        
        DB::connection('mysqlfidelidad')
            ->table('ps_customer')
            ->where('id_customer', $id)
            ->update(['date_upd' => date('Y-m-d H:i:s'), 'bloqueo' => '1']);
        		
        $this->DataNew[] = $Data;        
        if($this->ID_new==0){            
            $C_repositorio->fechacarga  = date('Y-m-d H:i:s');
            $C_repositorio->archivo     = $this->name;
            $C_repositorio->save();                       
            $this->ID_new = $C_repositorio->id;
        }        
        
        $C_repDetalle->idrepositorio = $this->ID_new;
        $C_repDetalle->id_customer   = $id;        
        $C_repDetalle->save();
        return true;
    }//public function insertData
    public function validaData($Data){
		$valor['val'] = false;
        $Nexiste = DB::connection('mysqlfidelidad')       
            ->table('ps_customer')
            ->where('document',$Data->documento)    
            ->get();
        
        foreach($Nexiste as $row){
            if ($row->id_customer){
              	$valor['val'] = true;			
                $valor['id'] = $row->id_customer;			
            }
			
        }

        return $valor;
    }//public function validaData
        
    public function encriptar_AES($string,$key){
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM );
        mcrypt_generic_init($td, $key, $iv);
        $encrypted_data_bin = mcrypt_generic($td, $string);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $encrypted = bin2hex($iv).bin2hex($encrypted_data_bin);
        return $encrypted;
    }// public function encriptar_AES 
    
    public function desencriptar_AES($encrypted_data_hex, $key){
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv_size_hex = mcrypt_enc_get_iv_size($td)*2;
        $iv = pack("H*", substr($encrypted_data_hex, 0, $iv_size_hex));
        $encrypted_data_bin = pack("H*", substr($encrypted_data_hex, $iv_size_hex));
        mcrypt_generic_init($td, $key, $iv);
        $decrypted = mdecrypt_generic($td, $encrypted_data_bin);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $decrypted;
    }//public function desencriptar_AES
}