<?php

namespace App\Http\Controllers;

use App\Mail\EnviarEmail as EnviarEmail;
use App\Mail\EnviarEmailMasivo as EnviarEmailMasivo;

use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller {
	//
	public function EnviarMailNotificaciion($email, $mesajeText, $email_cc) {

		$Data['email']    = $email;
		$Data['name_to']  = $mesajeText;
		$Data['email_cc'] = $email_cc;

		Mail::to($Data['email'], $Data['name_to'])
		->send(new EnviarEmail($Data));
	}//public function EnviarMailNotificaciion
	public function EnviarMailNotificaciionMasiva($email, $mesajeText, $email_cc) {

		$Data['email']    = $email;
		$Data['name_to']  = $mesajeText;
		$Data['email_cc'] = $email_cc;

		Mail::to($Data['email'], $Data['name_to'])
		->send(new EnviarEmailMasivo($Data));
	}//public function EnviarMailNotificaciion
	public function NotificarVencerSistema() {

		$abogado      = $this->AllAbogado();
		$fecha_Actual = date('Y-m-d');

		foreach ($abogado as $row_abg) {

			$abogado_id = $row_abg->id;

			$Data_abogado = User::find($abogado_id);

			$msj_text  = '';
			$msj_email = false;

			$venciendo_abg = DB::select('SELECT aa.id,aa.antecedente_resolucion,aa.fecha_recepcion,aa.antecedente_fecharesol,aa.fecha_ejecucion,CONCAT(aa.antecedente_nombre," ",aa.antecedente_apellido) AS razonsocial,ee.id,ee.antecedenteid,ee.expediente_rad,ee.expediente_folio,ee.expediente_fecha,a.id AS anotacion_id,a.procesoid,a.estadoid,a.expedienteid,p.proceso_nombre,e.estado_nombre,p.proceso_dias,p.proceso_dias_vence,e.estado_dias,e.estado_dias_vence,DATE(a.fecha) AS fecha_inicial, DATE_ADD(DATE(a.fecha),INTERVAL p.proceso_dias DAY) AS fecha_fin_etapa,DATE_SUB(ADDDATE(DATE(a.fecha),INTERVAL p.proceso_dias DAY ),INTERVAL p.proceso_dias_vence DAY) AS fecha_not_etapa,DATE_ADD(DATE(a.fecha),INTERVAL e.estado_dias DAY) AS fecha_fin_Actividad,DATE_SUB(ADDDATE(DATE(a.fecha),INTERVAL e.estado_dias DAY ),INTERVAL e.estado_dias_vence DAY) AS fecha_not_Actividad FROM anotacion AS a INNER JOIN proceso AS p ON p.id=a.procesoid INNER JOIN estado AS e ON e.id=a.estadoid INNER JOIN expediente AS ee ON ee.id = a.expedienteid INNER JOIN antecedente AS aa ON aa.id = ee.antecedenteid WHERE a.deleted_at IS NULL AND p.deleted_at IS NULL AND e.deleted_at IS NULL AND (p.proceso_dias <>0 OR e.estado_dias<>0) AND ((DATE_ADD(DATE(a.fecha),INTERVAL p.proceso_dias DAY) >= CURDATE()) OR (DATE_ADD(DATE(a.fecha),INTERVAL e.estado_dias DAY)>=CURDATE()))  AND aa.userid =? AND aa.antecedente_estado=? GROUP BY a.id', array($abogado_id,18));

			//dd($venciendo_abg);

			$Info_msj = array();

			foreach ($venciendo_abg as $row) {

				if ($row->estado_dias_vence >= 1) {
					if ($row->fecha_not_Actividad == $fecha_Actual) {
						$Info_msj['expedienteid'][]    = $row->expedienteid;
						$Info_msj['anotacion_id'][]    = $row->anotacion_id;
						$Info_msj['Fecha_Etapa'][]     = $row->fecha_fin_etapa;
						$Info_msj['Fecha_Actividad'][] = $row->fecha_fin_Actividad;
						//$Info_msj['abogado']         = $abogado_id;
					} else if (($fecha_Actual >= $row->fecha_not_Actividad) && ($fecha_Actual <= $row->fecha_fin_Actividad)) {
						$Info_msj['expedienteid'][]    = $row->expedienteid;
						$Info_msj['anotacion_id'][]    = $row->anotacion_id;
						$Info_msj['Fecha_Etapa'][]     = $row->fecha_fin_etapa;
						$Info_msj['Fecha_Actividad'][] = $row->fecha_fin_Actividad;
						//$Info_msj['abogado']          = $abogado_id;
					}
				} else if ($row->proceso_dias_vence >= 1) {
					if ($row->fecha_not_etapa == $fecha_Actual) {
						$Info_msj['expedienteid'][]    = $row->expedienteid;
						$Info_msj['anotacion_id'][]    = $row->anotacion_id;
						$Info_msj['Fecha_Etapa'][]     = $row->fecha_fin_etapa;
						$Info_msj['Fecha_Actividad'][] = $row->fecha_fin_Actividad;
						//$Info_msj['abogado']          = $abogado_id;
					} else if (($fecha_Actual >= $row->fecha_not_etapa) && ($fecha_Actual <= $row->fecha_fin_etapa)) {
						$Info_msj['expedienteid'][]    = $row->expedienteid;
						$Info_msj['anotacion_id'][]    = $row->anotacion_id;
						$Info_msj['Fecha_Etapa'][]     = $row->fecha_fin_etapa;
						$Info_msj['Fecha_Actividad'][] = $row->fecha_fin_Actividad;
						//$Info_msj['abogado']         = $abogado_id;
					}
				}

			}//foreach datatext

			if (count($Info_msj) >= 1) {

				$num_msj = count($Info_msj['expedienteid']);

				for ($x = 0; $x < $num_msj; $x++) {
					/***************************************/

					$Datos_msj = DB::select('SELECT   a.antecedente_resolucion,CONCAT(a.antecedente_nombre," ",a.antecedente_apellido) AS RazonSocial,pp.parametro_codigo,a.antecedente_nro_documento,e.expediente_rad,p.proceso_nombre,s.estado_nombre,a.useridcreate,DATE_ADD(DATE(n.fecha),INTERVAL p.proceso_dias DAY) AS fecha_fin_etapa,DATE_ADD(DATE(n.fecha),INTERVAL s.estado_dias DAY) AS fecha_fin_Actividad FROM expediente e INNER JOIN antecedente a ON a.id = e.antecedenteid INNER JOIN anotacion n ON n.expedienteid=e.id  INNER JOIN proceso p ON p.id=n.procesoid INNER JOIN estado s ON s.id=n.estadoid INNER JOIN parametro pp ON pp.id=a.tipo_doc_id WHERE  e.id =? AND n.id=? AND e.deleted_at IS NULL AND a.deleted_at IS NULL AND n.deleted_at IS NULL AND p.deleted_at IS NULL AND s.deleted_at IS NULL AND pp.deleted_at IS NULL  AND a.antecedente_estado=? ', array($Info_msj['expedienteid'][$x], $Info_msj['anotacion_id'][$x],18));

					$Info_msj['Datos'][] = $Datos_msj;
					$Data_user           = User::find($Datos_msj[0]->useridcreate);
					$Email_cc            = $Data_user->email;

					/***************************************/
				}//for

				$Info_data = (object) $Info_msj;// creacion de objeto

				$Email_ab = $Data_abogado->email;

				$this->EnviarMailNotificaciionMasiva($Email_ab, $Info_data, $Email_cc);

			}
			/**************************************************************************/
		}//foreach abogado

	}// public function NotificarVencerSistema

	public function AllAbogado() {
		$C_AllAbogado = DB::table('role_user AS r')
		->join('users AS u', 'u.id', '=', 'r.user_id')
		->where('r.role_id', '3')
		->where('u.usr_status', '1')
		->select('u.id')
		->get();

		return $C_AllAbogado;
	}//public function AllAbogado
}
