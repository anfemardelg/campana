<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Laracasts\Flash\Flash;
use App\ps_customer;
use App\ps_customer_group;
use App\ps_address;
use App\repositorio;
use App\repositoriodetalle;
use Storage;

class ImportController extends Controller
{
    //
    public $DataExiste = array();
    public $DataNew    = array();
    public $ID_new     = 0;
    public $name;
    public $Key_Secret = 'Ahf121d!454/f7&%GHF87\GiS**09DSe';
    public $id_Repor;
    
    public function index(){ 
        
        //$id_shop = $_GET['id_shop'];
                return view('redemir.importardata.index');
    }//public function index
    public function import(Request $request){
        
        if($request->file('FileData')){ 
        
                $nama_File = $request->file('FileData')->getClientOriginalName();
                $Opcion = explode('.',$nama_File);

                $file_route = date('Ymd_h_i_s').'_'.$request->file('FileData')->getClientOriginalName();// Nombre de la Imagen

                Storage::disk('DataImportar')->put($file_route, file_get_contents( $request->file('FileData')->getRealPath()));// almaceno en el disco

                //$DataExiste = array();

                $this->name = $file_route;
				$validaData = false;
                switch ($Opcion[1]) {

                    case 'xlsx': 
                        Excel::load('public/DataImportar/'.$file_route,function($reader){

                            foreach ($reader->get() as $book) {
							
								if ($book->documento){

                                    $validaData = $this->validaData($book); 
                                    
									if($validaData){

										$this->insertData($book);

									}else{

										$this->DataExiste[] = $book;

									}
								}
                            }
                        });
                        break;
                    case 'xls':
                        Excel::load('public/DataImportar/'.$file_route,function($reader){

                            foreach ($reader->get() as $book) {
								
								if ($book->documento){
									$validaData = $this->validaData($book);  

									if($validaData){

										$this->insertData($book);

									}else{

										$this->DataExiste[] = $book;

									}//if
								}
                            }
                        });
                        break;	
                    case 'txt':

                        Excel::load('public/DataImportar/'.$file_route,function($reader){

                            foreach ($reader->get() as $book) {
								
								if ($book->documento){
									$validaData = $this->validaData($book);  

									if($validaData === true){

										$this->insertData($book);

									}else{

										$this->DataExiste[] = $book;

									}//if
								}
                            }//foreach                    

                        });

                        break;
                }//switch
				
                //$this->DataNew[0]->clave
                $Existe = $this->DataExiste;
                $Nuevos = $this->DataNew;
                $idRepositorio = $this->ID_new;

                return view('redemir.importardata.consulta',compact('Existe','Nuevos','idRepositorio'));
        }else{
            Flash::error("Por favor cargar un archivo de forma correcta");
            return redirect()->route('importardata.index');
        }
    }//public function import
    public function insertData($Data){
        
        $C_customer         = new ps_customer(); 
        $C_customer_grupo   = new ps_customer_group(); 
		$C_address			= new ps_address();
        $C_repositorio      = new repositorio();
        $C_repDetalle       = new repositoriodetalle();
        
        //$Clave = $this->generarpassword();  
        $secure = md5(uniqid(rand(), true));
        //$Data->clave = $Clave;        
        //dd($Data);
        $id_shop = $_POST['id_shop'];

        $C_customer->id_shop_group      = '1';
        $C_customer->id_shop            = $id_shop;
        $C_customer->id_gender          = '1';
        $C_customer->id_default_group   = '3';
        $C_customer->id_lang            = '4';
        $C_customer->id_risk            = '0';
        $C_customer->company            = '';
        $C_customer->siret              = '';
        $C_customer->ape                = '';
        $C_customer->firstname          = $Data->nombre;
        $C_customer->lastname           = $Data->apellido;
        $C_customer->email              = $Data->email;
        $C_customer->document           = $Data->documento;
        $C_customer->passwd             = $this->EncriptarPassword($Data->tarjeta);  
        $C_customer->last_passwd_gen    = date('Y-m-d H:i:s');
        $C_customer->birthday           = date('Y-m-d');
        $C_customer->newsletter         = '0';
        $C_customer->ip_registration_newsletter ='';
        $C_customer->newsletter_date_add =date('Y-m-d H:i:s');
        $C_customer->optin              = '0';
        $C_customer->website            = '';
        $C_customer->outstanding_allow_amount = '0.000000';
        $C_customer->show_public_prices = '0';
        $C_customer->max_payment_days   = '0';
        $C_customer->secure_key         = $secure;
        $C_customer->note               = '';
        $C_customer->active             = '1';
        $C_customer->is_guest           = '0';
        $C_customer->deleted            = '0';
        $C_customer->date_add           = date('Y-m-d H:i:s');
        $C_customer->date_upd           = date('Y-m-d H:i:s');
        $C_customer->autoriza1          = '0';
        $C_customer->autoriza2          = '0';
        $C_customer->actualiza          = '0';

        $C_customer->save();
        
        $Last_id = $C_customer->id;  
                
        $C_customer_grupo->id_customer = $Last_id;
        $C_customer_grupo->id_group     = '3';
        
        $C_customer_grupo->save();
        
        $IdDepto = $this->BuscarDept($Data->depto);
        
		$C_address->id_country = '69';
        $C_address->id_state = $IdDepto;
		$C_address->id_customer = $Last_id;
		$C_address->id_manufacturer = '0';
		$C_address->id_supplier = '0';
		$C_address->id_warehouse = '0';
		$C_address->alias = 'Principal';
		$C_address->company = '';
		$C_address->lastname = $Data->apellido;
		$C_address->firstname = $Data->nombre;
		$C_address->address1 = $Data->direccion;
        $C_address->phone = $Data->telefono;
        $C_address->phone_mobile = $Data->celular;
		$C_address->postcode = '000000';
		$C_address->city = $Data->ciudad;
		$C_address->vat_number = '';
		$C_address->dni = '';
		$C_address->date_add = date('Y-m-d H:i:s');;
		$C_address->date_upd = date('Y-m-d H:i:s');;
		$C_address->active = '1';
		$C_address->deleted = '0';
		
        $C_address->save();
		
        $this->DataNew[] = $Data;
        
        if($this->ID_new==0){
            
            $C_repositorio->fechacarga  = date('Y-m-d H:i:s');
            $C_repositorio->archivo     = $this->name;

            $C_repositorio->save();    
            
            
            $this->ID_new = $C_repositorio->id;
        }
        
        
        
        $passwdTwo = $this->encriptar_AES($Data->tarjeta,$this->Key_Secret);
        
        $C_repDetalle->idrepositorio = $this->ID_new;
        $C_repDetalle->id_customer   = $Last_id;
        $C_repDetalle->clave         = $passwdTwo;
        
        $C_repDetalle->save();
        
        return true;
        
        /*   
        
        $ClaveDes = $this->desencriptar_AES($passwdTwo,$Key_Secret);*/
    }//public function insertData
    public function validaData($Data){
		$valor = true;
        $Existe = DB::connection('mysqlfidelidad')       
            ->table('ps_customer')
            ->where('document',$Data->documento)    
            ->get();
        
        foreach($Existe as $row){
            if ($row->id_customer)
				$valor = false;			
        }

        return $valor;
    }//public function validaData
    public function exportarDocumento(Request $request){
       
        $products = array();
        $this->id_Repor = $request->id;
        $titulo = 'Usuarios Cargados_'.date('Y-m-d');
        
        Excel::create($titulo, function($excel) {
 
            $excel->sheet('Usuarios Cargados', function($sheet) {
                
                $repositorioData = DB::select('select d.id_customer, d.clave from repositorio r inner join repositoriodetalle d on d.idrepositorio=r.idrepositorio where r.idrepositorio=?',array($this->id_Repor));
                
                $i=1;
                $products[0][] = 'Documento';
                $products[0][] = 'Nombre';
                $products[0][] = 'Apellido';
                $products[0][] = 'Email';
                $products[0][] = 'Clave';
                
                foreach($repositorioData as $row){
                    
                    $customer = DB::connection('mysqlfidelidad')->select('select firstname,lastname,email,document from ps_customer where id_customer=?',array($row->id_customer));
                   
                    foreach($customer as $dato){
                        $products[$i][]   = $dato->document;
                        $products[$i][]   = $dato->firstname;
                        $products[$i][]   = $dato->lastname;
                        $products[$i][]   = $dato->email;   
                        $products[$i][]   = $this->desencriptar_AES($row->clave,$this->Key_Secret);                          
                       
                    }//foreach
                    $i++;
                }//foreach
                
                $collection = Collection::make($products); 
                
                $sheet->fromArray($collection);
            
            });
        })->export('xls');
        
        
    }//public function exportarDocumento
    public function consulta(Request $request){
        $Datos = repositorio::all();
        
        return view('redemir.importardata.repositorio',compact('Datos'));
    }//public function consulta
    
    
    public function generarpassword(){
        
        $opc_letras = TRUE; //  FALSE para quitar las letras
        $opc_numeros = TRUE; // FALSE para quitar los números
        $opc_letrasMayus = TRUE; // FALSE para quitar las letras mayúsculas
        $opc_especiales = FALSE; // FALSE para quitar los caracteres especiales
        $longitud = 6;
        $password = "";

        $letras ="abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales ="|@#~$%()=^*+[]{}-_";
        $listado = "";

        if ($opc_letras == TRUE) {
            $listado .= $letras; 
            }
        if ($opc_numeros == TRUE) {
            $listado .= $numeros; }
        if($opc_letrasMayus == TRUE) {
            $listado .= $letrasMayus;    
        }
        if($opc_especiales == TRUE) {
            $listado .= $especiales; 
        }

        str_shuffle($listado);
        for( $i=1; $i<=$longitud; $i++) {
        $password[$i] = $listado[rand(0,strlen($listado))];
        str_shuffle($listado);
        }

        $longitudnumeros=strlen($numeros);

        $n=rand(0,$longitudnumeros-1);
        $password[$i]=substr($numeros,$n,1);

        $longitudMayus=strlen($letrasMayus);

        $v=rand(0,$longitudMayus-1);
        $password[$i+1]=substr($letrasMayus,$v,1);

        $newPassword = '';

        foreach ($password as $dato_password) {
            $newPassword = $newPassword.$dato_password;
        }        
       
        return $newPassword;    
    }//public function generarpassword
    
    public function encriptar_AES($string,$key){
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM );
        mcrypt_generic_init($td, $key, $iv);
        $encrypted_data_bin = mcrypt_generic($td, $string);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $encrypted = bin2hex($iv).bin2hex($encrypted_data_bin);
        return $encrypted;
    }// public function encriptar_AES 
    
    public function desencriptar_AES($encrypted_data_hex, $key){
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv_size_hex = mcrypt_enc_get_iv_size($td)*2;
        $iv = pack("H*", substr($encrypted_data_hex, 0, $iv_size_hex));
        $encrypted_data_bin = pack("H*", substr($encrypted_data_hex, $iv_size_hex));
        mcrypt_generic_init($td, $key, $iv);
        $decrypted = mdecrypt_generic($td, $encrypted_data_bin);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $decrypted;
    }//public function desencriptar_AES
    
    public function EncriptarPassword($Password){
        $key_segurity = 'I3mLxuxZo1zSFjdCAcWhubRk9Gu3ZbTxtTdnrPSxX7zJYRu2x0IuZZDW';
        $PasswEncript = md5(sha1(md5($key_segurity.$Password).sha1($key_segurity.$Password)));
        
        return $PasswEncript;
    }
    
     public function BuscarDept($texto){
        $like = $texto.'%';
        $Depto = DB::connection('mysqlfidelidad')->select('SELECT id_state FROM ps_state where id_country=69 and name like ? ',array($like));

        if(count($Depto)>=1){
            $id = $Depto[0]->id_state;
        }else{
            $id = '326';
        }

        return $id;
    }
}