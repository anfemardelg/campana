<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_czhomeslider_slides extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_czhomeslider_slides';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_czhomeslider_slides',
        'position',
        'active'
        
    ];        
}
