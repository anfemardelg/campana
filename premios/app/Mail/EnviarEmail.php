<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnviarEmail extends Mailable
{
    use Queueable, SerializesModels;
   /**
   *@var
   */
    public $name;
    public $email;
    public $email_cc;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Data)
    {
        $this->name = $Data['name_to'] ;
        $this->email = $Data['email'] ;
        $this->email_cc = $Data['email_cc'] ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('coactivo.email.emailenvio')
            ->cc($this->email_cc)
            ->subject('Notificación del Sistema');
    }
}
