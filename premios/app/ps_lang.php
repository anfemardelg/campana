<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_lang extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_lang';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_lang',
        'name',
        'active',
        'iso_code',
        'language_code',
        'locale',
        'date_format_lite',
        'date_format_full',
        'is_rtl'

        
    ];        
}