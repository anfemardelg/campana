<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_meta_lang extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_meta_lang';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_meta',
        'id_shop',
        'id_lang',
        'title',
        'description',
        'keywords',
        'url_rewrite'
    ];        
}