<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_product_shop extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_product_shop';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_product',
        'product_id',
        'id_shop',
        'id_category_default',
        'id_tax_rules_group',
        'on_sale',
        'online_only',
        'ecotax',
        'minimal_quantity',
        'low_stock_threshold',
        'low_stock_alert',
        'price',
        'wholesale_price',
        'unity',
        'init_price_ratio',
        'additional_shipping_cost',
        'customizable',
        'uploadable_files',
        'text_fields',
        'active',
        'redirect_type',
        'id_type_redirected',
        'available_for_order',
        'avaible_date',
        'show_condition',
        'condition',
        'show_price',
        'indexed',
        'visibility',
        'cache_default_attribute',
        'advanced_stock_management',
        'date_add',
        'date_upd',
        'pack_stock_type'
    ];        
}