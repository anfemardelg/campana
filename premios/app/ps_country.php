<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_country extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_country';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_country',
        'id_zone',
        'id_currency',
        'iso_code',
        'call_prefix',
        'active',
        'contains_states',
        'need_identification_number',
        'need_zip_code',
        'zip_code_format',
        'display_tax_label'
    ];        
}
