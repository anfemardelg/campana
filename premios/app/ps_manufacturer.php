<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ps_manufacturer extends Model
{
    //
    //
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysqlfidelidad';
    
    protected $table = 'ps_manufacturer';
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_manufacturer',
        'name',
        'date_add',
        'date_upd',
        'active'
        
    ];        
}