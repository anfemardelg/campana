<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marcas extends Model
{    

     protected $fillable = [        
          'brand_id',
          'nombre',
          'logo',
          'type'       
  ];

}