<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasas', function (Blueprint $table) {
            $table->increments('id');
            $table->float('porcentaje_anual');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');
            $table->timestamps();
            $table->softDeletes();
        });

         Schema::create('anios_bisiestos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('anios_bisiestos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('tasas');
         Schema::dropIfExists('anios_bisiestos');
    }
}
