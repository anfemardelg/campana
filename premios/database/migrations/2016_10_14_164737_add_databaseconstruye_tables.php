<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatabaseconstruyeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('proceso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rol_id')->unsigned();  
            $table->string('proceso_nombre');
            $table->text('proceso_descripcion');
            $table->integer('proceso_dias')->default(0);
            $table->integer('proceso_dias_vence')->default(0);
            $table->integer('proceso_parent_id')->default(0);
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');
            $table->foreign('rol_id')->references('id')->on('roles');         
        });
        
          Schema::create('rutaproceso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proceso_padre');
            $table->integer('proceso_hijo');            
            $table->timestamps();           
         });

        Schema::create('estado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado_nombre');
            $table->text('estado_descripcion');
            $table->integer('estado_dias')->default(0);
            $table->integer('estado_dias_vence')->default(0);
            $table->integer('procesoid')->unsigned();
            $table->integer('estado_parent_id')->default(0);
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');
            $table->foreign('procesoid')->references('id')->on('proceso');       
        });

        Schema::create('documento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('extension');
            $table->text('campo_id');
            $table->integer('valor_id');
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');   
        });

         Schema::create('antecedente', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('userid')->unsigned();
                    $table->integer('origen');
                    $table->date('fecha_recepcion');
                    $table->string('antecedente_oficio');
                    $table->text('antecedente_descripcion');
                    $table->string('antecedente_nombre');
                    $table->string('antecedente_apellido');
                    $table->string('antecedente_direccion1');            
                    $table->string('antecedente_telefono');
                    $table->string('antecedente_email');
                    $table->integer('tipo_doc_id');
                    $table->string('antecedente_nro_documento');
                    $table->integer('codigovalidacion')->nullable(); 
                    $table->text('antecedente_sancion');
                    $table->tinyInteger('categoria_valor');
                    $table->text('antecedente_concepto');
                    $table->string('antecedente_resolucion');
                    $table->date('antecedente_fecharesol');
                    $table->date('fecha_ejecucion');
                    $table->integer('antecedente_estado');
                    $table->integer('salario_id')->nullable();
                    $table->enum('antecedente_reparto',['S','N'])->default('S');    
                    $table->text('ubicacionfisica')->nullable();        
                    $table->softDeletes();         
                    $table->timestamps();
                    $table->integer('useridcreate');
                    $table->integer('useridupdate');
                    $table->string('ip_create');
                    $table->string('ip_update');    
                    $table->foreign('userid')->references('id')->on('users'); 
        });
        
        
        /**************************************************/
             Schema::create('antecedentedirecciones', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('antecedente_id')->unsigned();
                    $table->string('direccion')->nullable();
                    $table->string('telefono')->nullable();
                    $table->softDeletes();         
                    $table->timestamps();
                    $table->integer('useridcreate');
                    $table->integer('useridupdate');
                    $table->string('ip_create');
                    $table->string('ip_update');    
                    $table->foreign('antecedente_id')->references('id')->on('antecedente'); 
             });
        
        /************************************************/
        Schema::create('expediente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('expediente_rad')->unique();
            $table->string('expediente_folio');
            $table->text('expediente_descripcion');
            $table->date('expediente_fecha');
            $table->integer('antecedenteid')->unsigned();
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');       
            $table->index('useridcreate');
            $table->index('antecedenteid');
            $table->foreign('antecedenteid')->references('id')->on('antecedente'); 
        });
        /************************************************************/
        Schema::create('anotacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expedienteid')->unsigned();
            $table->integer('userid')->unsigned();
            $table->integer('procesoid')->unsigned();
            $table->integer('estadoid')->unsigned();
            $table->integer('consecutivo')->nullable();
            $table->integer('year_consecutivo');
            $table->date('fecha');
            $table->text('anotacion_anotaciones');
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');
            $table->foreign('expedienteid')->references('id')->on('expediente');
            $table->foreign('userid')->references('id')->on('users'); 
            $table->foreign('procesoid')->references('id')->on('proceso'); 
            $table->foreign('estadoid')->references('id')->on('estado'); 
        });
        Schema::create('titulovalor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('antecedente_id')->unsigned();
            $table->string('titulo_judicial');            
            $table->integer('banco');            
            $table->double('valor',15,2);  
            $table->integer('estado');          
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');                
            $table->foreign('antecedente_id')->references('id')->on('antecedente'); 
         });
        Schema::create('reservaconsecutivo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procesoid');
            $table->integer('consecutivo_reserva');
            $table->string('year');                               
            $table->softDeletes();         
            $table->timestamps();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');  
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento');
        Schema::dropIfExists('anotacion');
        Schema::dropIfExists('expediente');
        Schema::dropIfExists('antecedente');
        Schema::dropIfExists('estado');
        Schema::dropIfExists('proceso');
        Schema::dropIfExists('rutaproceso');
        Schema::dropIfExists('antecedentedirecciones');
        Schema::dropIfExists('titulovalor');
    }
}