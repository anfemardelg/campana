<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidacion_enc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('antecedenteid')->nullable();
            $table->string('empresa');
            $table->string('tipo_impuesto')->nullable();
            $table->integer('valor_base');
            $table->string('vencimiento_legal',10);
            $table->string('fecha_pago',10);
            $table->integer('periodo_gravable')->nullable();
            $table->integer('tipo_liquidacion');
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('liquidacion', function (Blueprint $table) {
            $table->increments('id');
            $table->float('porcentaje_anual');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->double('tasa_diaria',7,6);
            $table->integer('dias_mora');
            $table->integer('interes');
            $table->integer('nuevo_saldo');
            $table->integer('dias_acumulados');
            $table->integer('liquidacionencid');
            $table->timestamps();
            $table->softDeletes();
      
        });

         Schema::create('liquidacion_cuotas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liquidacionencid');
            $table->integer('valor_cuota');
            $table->string('fecha_pago',10);
            $table->timestamps();
            $table->softDeletes();
      
        });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidacion_enc');
        Schema::dropIfExists('liquidacion');
        Schema::dropIfExists('liquidacion_cuotas');
    }
}
