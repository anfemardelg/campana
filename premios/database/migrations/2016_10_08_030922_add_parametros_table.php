<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parametro_codigo');
            $table->string('parametro_dominio');
            $table->text('parametro_descripcion');
            $table->enum('parametro_estado',['activo','inactivo'])->default('activo');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('useridcreate');
            $table->integer('useridupdate');
            $table->string('ip_create');
            $table->string('ip_update');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametro');
    }
}
