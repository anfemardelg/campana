<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('salario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salario_ano');
            $table->float('salario_auxmensual',12, 2);
            $table->float('salario_auxvarianual',5, 2);
            $table->float('salario_montodiario',12, 2);
            $table->float('salario_montomensual',12, 2);
            $table->float('salario_varianual',5, 2);
            $table->softDeletes();         
            $table->timestamps();
            $table->string('ip_create');
            $table->string('ip_update');
            $table->integer('useridcreate');
            $table->integer('useridupdate');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salario');
    }
}
